<?php

namespace App\Exceptions;

use Exception, Request, Helper;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson() OR Request::segment(1) == "api") {
            $format = $request->route('format');
            
            $response = [
                'msg' => "Unable to process request.",
                'status' => FALSE,
                'status_code' => "UNAUTHORIZED",
                'exception' => $exception->getMessage(),
            ];

            $response_code = 500;

            if($exception instanceof RequestException) {
                $response['msg'] = "cURL error. Please check your connetion.";
                $response['status_code'] = "CURL_ERROR";
                $response_code = 409;
                switch ($format) {
                    case 'json':
                        return response()->json($response);
                    break;
                    case 'xml':
                        return response()->xml($response);
                    break;
                }
            } else {
                if($exception instanceof \Illuminate\Validation\ValidationException) goto callback;

                if($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException)
                    return response()->json($response, 401);

                return response()->json($response, $response_code);
            }


            callback:
        }

        return parent::render($request, $exception);
    }
}