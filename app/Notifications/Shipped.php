<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Shipped extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

   public function toDatabase($notifiable)
    {
       $url = route('portal.deliver.index');
        return [
            "subject" => "Order Status",
            "messages" => "Youre Order has been Shipped!",
            "url" => $url
        ];
    }
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
