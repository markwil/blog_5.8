<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RecievedNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }
public function toDatabase($notifiable)
    {
         $url = route('portal.stock.index');
        return [
            "subject" => "Order Status",
            "messages" => "Order has been recieved by  the customer!",
            "url" => $url
        ];
    }
    
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
