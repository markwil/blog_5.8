<?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/
Route::group(['as' => "api.",
		 'namespace' => "Api",
		],function() {

	// Route::post('index.{format}',['as' => "index",'uses' => "MainController@index"]);
Route::post('index.{format}',['as' => "index",'uses' => "RegisterController@index"]);

	
	Route::group(['as' => "auth.", 'prefix' => "auth", 'namespace' => "Auth"], function () {
		Route::post('login.{format?}', ['as' => "login", 'uses' => "LoginController@authenticate"]);

		Route::post('logout.{format?}', ['as' => "logout", 'uses' => "LoginController@logout", 'middleware' => ["jwt.auth"]]);
		Route::post('refresh-token.{format?}', ['as' => "refresh_token", 'uses' => "RefreshTokenController@refresh"]);
	});

	Route::group(['middleware' => ['jwt.auth']],function(){
		Route::post('store.{format}',['uses' => "RegisterController@store"]);
		Route::post('destroy.{format}',['uses' => "RegisterController@destroy"]);
		Route::post('user-list.{format}',['as'=>"user_list",'uses' => "RegisterController@user_list"]);
		Route::post('search.{format}',['as'=>"search",'uses' => "RegisterController@search"]);
		 
	});
});