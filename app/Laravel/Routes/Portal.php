 <?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/
Route::group(['as' => "portal.",
		 'namespace' => "Portal",
		 'middleware' => ["web"]
		],function() {

Route::group(['middleware' => "portal.guest",'prefix'=>"register",'as'=>"register."],function(){

			route::get('/',['as'=>"index",'uses'=>"RegisterController@index"]);
			route::get('create',['as'=>"create",'uses'=>"RegisterController@create"]);
			route::post('create',['uses'=>"RegisterController@store"]);
		});
	Route::group(['middleware' => "portal.guest"], function(){

		// Route::get('register/{_token?}',['as' => "register",'uses' => "AuthController@register"]);
		// Route::post('register/{_token?}',['uses' => "AuthController@store"]);

		Route::get('login',['as' => "login",'uses' => "AuthController@login"]);
		Route::get('admin-login',['as' => "admin_login",'uses' => "AuthController@admin_login"]);

		Route::post('login',['uses' => "AuthController@authenticate"]);
		Route::post('admin-login',['uses' => "AuthController@authenticate"]);


		Route::post('login',['uses' => "AuthController@authenticate"]);
	});

	Route::group(['middleware' => "portal.auth"],function(){
		Route::get('/',['as' => "index",'uses' => "MainController@index"]);
		// Route::get('cashier',['as' => "cashier",'uses' => "MainController@index"]);

		Route::get('logout',['as' => "logout",'uses' => "AuthController@logout"]);

		Route::group(['as' => "profile."],function(){
			Route::get('edit-password',['as' => "edit_password",'uses' => "ProfileController@edit_password"]);
			Route::post('edit-password',['uses' => "ProfileController@update_password"]);
		});

		Route::group(['prefix' => "partner",'as' => "partner."],function()
{			Route::get('/',['as' => "index",'uses' => "PartnerController@index"]);
			Route::get('create',['as' => "create",'uses' => "PartnerController@create"]);
			Route::post('create',['uses' => "PartnerController@store"]);

			Route::get('edit/{partner_id?}',['as' => "edit",'uses' => "PartnerController@edit"]);
			Route::post('edit/{partner_id?}',['uses' => "PartnerController@update"]);

			Route::get('reset-password/{partner_id?}',['as' => "reset_password",'uses' => "PartnerController@reset_password"]);
			Route::post('reset-password/{partner_id?}',['uses' => "PartnerController@update_reset_password"]);

			Route::get('import',['as' => "import",'uses' => "PartnerController@import"]);
			Route::post('import',['uses' => "PartnerController@import_account"]);


			Route::any('delete/{partner_id?}',['as' => "delete",'uses' => "PartnerController@delete"]);

		});

		Route::group(['prefix' => "administrator",'as' => "administrator."],function(){
			Route::get('/',['as' => "index",'uses' => "AdministratorController@index"]);
			Route::get('create',['as' => "create",'uses' => "AdministratorController@create"]);
			Route::post('create',['uses' => "AdministratorController@store"]);

			Route::get('edit/{administrator_id?}',['as' => "edit",'uses' => "AdministratorController@edit"]);
			Route::post('edit/{administrator_id?}',['uses' => "AdministratorController@update"]);

			Route::get('reset-password/{administrator_id?}',['as' => "reset_password",'uses' => "AdministratorController@reset_password"]);
			Route::post('reset-password/{administrator_id?}',['uses' => "AdministratorController@update_reset_password"]);

			Route::any('delete/{administrator_id?}',['as' => "delete",'uses' => "AdministratorController@delete"]);

		});

		 
		Route::group(['prefix' => "reporting",'as' => "reporting."],function(){
			Route::get('/',['as' => "index",'uses' => "ReportingController@index"]);
			Route::get('import',['as' => "import",'uses' => "ReportingController@import"]);
			Route::post('import',['uses' => "ReportingController@import_data"]);

			Route::get('reset',['as' => "reset",'uses' => "ReportingController@reset"]);
			Route::post('reset',['uses' => "ReportingController@reset_data"]);
		});

		Route::group(['middleware'=>['portal.auth'],'prefix' => "product",'as' => "product."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductController@index"]);
			Route::get('create',['as'=>"create",'uses' => "ProductController@create"]);
			Route::post('create',['uses' => "ProductController@store"]);
			Route::get('show',['as'=> "show",'uses' => "ProductController@display"]);
			Route::any('destroy/{id?}',['as'=> "destroy",'uses' => "ProductController@destroy"]);
			Route::get('edit/{id?}',['as'=> "edit",'uses' => "ProductController@edit"]);
			Route::post('edit/{id?}',['uses' => "ProductController@update"]);
			Route::get('remove-product/{id?}',['as'=>"remove-product",'uses' => "ProductController@remove_product"]);
			Route::get('remove-ingredient/{id?}',['as'=>"remove-ingredient",'uses' => "ProductController@remove_ingredient"]);
			Route::get('edit-qty/{id?}/{qty?}',['as'=>"edit-qty",'uses' => "ProductController@edit_qty"]);
		});

// ////////////////////////////////// Branch /////////////////////////////////////////
		Route::group(['middleware'=>['portal.auth'],'prefix' => "purchase-order-branch",'as' => "purchase_order_branch."],function(){
			Route::get('/',['as' => "index",'uses' => "PurchaseOrderBranchController@index"]);
			Route::post('/',['uses' => "PurchaseOrderBranchController@add_to_cart"]);
			Route::post('search',['as'=>"search",'uses' => "PurchaseOrderBranchController@search"]);
		});

			Route::group(['middleware'=>'portal.auth','prefix' => "order-list-branch",'as' => "order_list_branch."],function(){
			Route::get('/',['as' => "index",'uses' => "OrderListBranchController@index"]);
			Route::get('create',['as'=>"create",'uses' => "OrderListBranchController@create"]);
			Route::post('create',['as'=>"store",'uses' => "OrderListBranchController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "OrderListBranchController@edit"]);
			Route::post('edit/{id?}',['uses' => "OrderListBranchController@update"]);
			Route::get('show/{id?}',['as'=>"show",'uses' => "OrderListBranchController@show"]);
			Route::post('stock-in',['as'=>"stock-in",'uses' => "OrderListBranchController@stock_in"]);	
			Route::get('detail/{id?}',['as'=>"detail",'uses' => "OrderListBranchController@order_details"]);	
			Route::get('recieve/{id?}',['as'=>"recieve",'uses' => "OrderListBranchController@recieve"]);	
			Route::post('recieve/{id?}',['uses' => "OrderListBranchController@store"]);	
			Route::get('cancel/{id?}',['as'=>'cancel', 'uses' => "OrderListBranchController@cancel_order"]);	
			Route::get('delete/{id?}',['as'=>'delete', 'uses' => "OrderListBranchController@remove_order"]);
			Route::get('checked-out/{id?}',['as'=>"checkedout",'uses' => "OrderListBranchController@checked_out"]);	
		});

			Route::group(['middleware'=>'portal.auth','prefix' => "terminal",'as' => "terminal."],function(){
			Route::get('/',['as' => "index",'uses' => "TerminalController@index"]);
			Route::get('create',['as'=>"create",'uses' => "TerminalController@create"]);
			Route::post('create',['as'=>"store",'uses' => "TerminalController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "TerminalController@edit"]);
			Route::post('edit/{id?}',['uses' => "TerminalController@update"]);
			
		});
			Route::group(['middleware'=>'portal.auth','prefix' => "product-ingredient",'as' => "product-ingredient."],function(){
			Route::get('/{id?}',['as' => "index",'uses' => "ProductIngredientController@index"]);
			Route::get('create',['as'=>"create",'uses' => "ProductIngredientController@create"]);
			Route::post('create',['as'=>"store",'uses' => "ProductIngredientController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "ProductIngredientController@edit"]);
			Route::post('edit/{id?}',['uses' => "ProductIngredientController@update"]);
			Route::post('update/{id?}',['as'=>'update','uses' => "ProductIngredientController@update"]);
			
		});

			Route::group(['middleware'=>'portal.auth','prefix' => "raw-material",'as' => "raw-material."],function(){
			Route::any('/{id?}',['as' => "index",'uses' => "RawMaterialController@index"]);
			Route::get('create',['as'=>"create",'uses' => "RawMaterialController@create"]);
			Route::post('create',['as'=>"store",'uses' => "RawMaterialController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "RawMaterialController@edit"]);
			Route::post('edit/{id?}',['uses' => "RawMaterialController@update"]);
		
			
		});


// //////////////////////////////////////Branch //////////////////////////////////////////////////////////////

		Route::group(['middleware'=>['portal.auth'],'prefix' => "product-category",'as' => "product_category."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductCategoryController@index"]);
			Route::get('create',['as'=>"create",'uses' => "ProductCategoryController@create"]);
			Route::post('create',['uses' => "ProductCategoryController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "ProductCategoryController@edit"]);

			Route::post('edit/{id?}',['uses' => "ProductCategoryController@update"]);

			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "ProductCategoryController@destroy"]);

		});

		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "stock",'as' => "stock."],function(){
			Route::get('/',['as' => "index",'uses' => "StockInController@index"]);
			Route::post('/',['uses' => "StockInController@update_marked_up"]);
			
			Route::get('create',['as'=>"create",'uses' => "StockInController@create"]);
			Route::post('create',['uses' => "StockInController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "StockInController@edit"]);
			Route::post('edit/{id?}',['uses' => "StockInController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "StockInController@destroy"]);

		});

		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "supplier",'as' => "supplier."],function(){
			Route::get('/',['as' => "index",'uses' => "SupplierController@index"]);
			Route::get('create',['as'=>"create",'uses' => "SupplierController@create"]);
			Route::post('create',['uses' => "SupplierController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "SupplierController@edit"]);
			Route::post('edit/{id?}',['uses' => "SupplierController@update"]);
			Route::get('update-status/{id?}/{status?}',['as'=>"update-status",'uses' => "SupplierController@update_status"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "SupplierController@destroy"]);

		});
		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "supplier-product",'as' => "supplier-product."],function(){
			Route::get('/{id?}',['as' => "index",'uses' => "SupplierProductController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "SupplierProductController@create"]);
			Route::post('create/{id?}',['uses' => "SupplierProductController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "SupplierProductController@edit"]);
			Route::post('edit/{id?}',['uses' => "SupplierProductController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "SupplierProductController@destroy"]);

		});
		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "stock-out",'as' => "stock-out."],function(){

			Route::get('/',['as' => "index",'uses' => "StockOutController@index"]);
			Route::get('create',['as'=>"create",'uses' => "StockOutController@create"]);
			Route::post('create',['uses' => "StockOutController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "StockOutController@edit"]);
			Route::post('edit/{id?}',['uses' => "StockOutController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "StockOutController@destroy"]);

		});
		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "branch",'as' => "branch."],function(){

			Route::get('/',['as' => "index",'uses' => "BranchController@index"]);
			Route::get('create',['as'=>"create",'uses' => "BranchController@create"]);
			Route::post('create',['uses' => "BranchController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "BranchController@edit"]);
			Route::post('edit/{id?}',['uses' => "BranchController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "BranchController@destroy"]);

		});
		Route::group(['middleware'=>['portal.auth'],'prefix' => "deliver",'as' => "deliver."],function(){

			Route::get('/',['as' => "index",'uses' => "DeliverController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "DeliverController@create"]);
			Route::post('create/{id?}',['uses' => "DeliverController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "DeliverController@edit"]);
			Route::post('edit/{id?}',['uses' => "DeliverController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "DeliverController@destroy"]);
			Route::get('details/{id?}',['as'=>'details','uses' => "DeliverController@details"]);
			Route::get('complete-transaction/{id?}/{bid?}',['as'=>'complete_transaction','uses' => "DeliverController@complete_transaction"]);
			Route::get('print/{id?}',['as'=>'print','uses' => "DeliverController@print_purchase_oder"]);

		});
		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "store",'as' => "store."],function(){

			Route::any('/{id?}',['as' => "index",'uses' => "StoreController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "StoreController@create"]);
			Route::post('create/{id?}',['uses' => "StoreController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "StoreController@edit"]);
			Route::post('edit/{id?}',['uses' => "StoreController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "StoreController@destroy"]);

		});

		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "stock-request",'as' => "stock-request."],function(){
			Route::any('/{id?}',['as' => "index",'uses' => "StockRequestController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "StockRequestController@create"]);
			Route::post('create/{id?}',['uses' => "StockRequestController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "StockRequestController@edit"]);
			Route::post('edit/{id?}',['uses' => "StockRequestController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "StockRequestController@destroy"]);
			Route::get('details/{id?}',['as'=>'details','uses' => "StockRequestController@details"]);
			Route::post('details/{id?}',['uses' => "StockRequestController@update"]);
			Route::get('approve/{id?}',['as'=>"approve",'uses' => "StockRequestController@approve_order"]);
			Route::get('print/{id?}',['as'=>"print",'uses' => "StockRequestController@print_purchase_oder"]);
		
		});

		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "deliver",'as' => "deliver."],function(){
			Route::any('/{id?}',['as' => "index",'uses' => "DeliverController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "DeliverController@create"]);
			Route::post('create/{id?}',['uses' => "DeliverController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "DeliverController@edit"]);
			Route::post('edit/{id?}',['uses' => "DeliverController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "DeliverController@destroy"]);
			Route::get('details/{id?}',['as'=>'details','uses' => "DeliverController@details"]);
			Route::post('details/{id?}',['uses' => "DeliverController@update"]);
			// Route::any('destroy/{id?}',['as'=>'destroy','uses' => "DeliverController@destroy"]);
		});
		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "pending-request",'as' => "pending-request."],function(){
			Route::any('/{id?}',['as' => "index",'uses' => "PendingRequestController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "PendingRequestController@create"]);
			Route::post('create/{id?}',['uses' => "PendingRequestController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "PendingRequestController@edit"]);
			Route::post('edit/{id?}',['uses' => "PendingRequestController@update"]);

					});

		Route::group(['middleware'=>'portal.auth','prefix' => "cashier",'as' => "cashier."],function(){
			Route::get('/',['as' => "index",'uses' => "CashierController@index"]);
			Route::get('create',['as'=>"create",'uses' => "CashierController@create"]);
			Route::post('create',['uses' => "CashierController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "CashierController@edit"]);

			Route::post('edit/{id?}',['uses' => "CashierController@update"]);

			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "CashierController@destroy"]);

		});

		Route::group(['middleware'=>'portal.auth','prefix' => "branch-stock",'as' => "branch-stock."],function(){
			Route::get('/{branch_id?}',['as' => "index",'uses' => "BranchStockController@index"]);
			Route::get('create',['as'=>"create",'uses' => "BranchStockController@create"]);
			Route::post('create',['uses' => "BranchStockController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "BranchStockController@edit"]);
			Route::post('edit/{id?}',['uses' => "BranchStockController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "StoreBranchController@destroy"]);
		});

		Route::group(['middleware'=>'portal.auth','prefix' => "raw-material-request",'as' => "raw-material-request."],function(){
			Route::get('/',['as' => "index",'uses' => "RawMaterialRequestController@index"]);
			Route::get('create',['as'=>"create",'uses' => "RawMaterialRequestController@create"]);
			Route::post('create',['uses' => "RawMaterialRequestController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "RawMaterialRequestController@edit"]);
			Route::post('edit/{id?}',['uses' => "RawMaterialRequestController@update"]);
			Route::get('destroy/{id?}',['as'=>'destroy','uses' => "RawMaterialRequestController@destroy"]);

			Route::post('addtocart',['as'=>'addtocart','uses' => "RawMaterialRequestController@addtocart"]);

			Route::get('completed/{id?}',['as'=>'completed','uses' => "RawMaterialRequestController@transaction_complete"]);
		});


		Route::get('search',['uses'=>'RawMaterialRequestController@search']);

		// Route::group(['middleware'=>'portal.auth','prefix' => "add-to-cart",'as' => "add-to-cart."],function(){
		// 	Route::get('/',['as' => "index",'uses' => "PurchaseOrderSupplierController@index"]);
		// 	Route::get('create/{id?}',['as'=>"create",'uses' => "PurchaseOrderSupplierController@create"]);
		// 	Route::post('create/{id?}',['uses' => "PurchaseOrderSupplierController@store"]);
		// 	Route::get('edit/{id?}',['as'=>'edit','uses' => "PurchaseOrderSupplierController@edit"]);
		// 	Route::post('edit/{id?}',['uses' => "PurchaseOrderSupplierController@update"]);
		// });

		Route::group(['middleware'=>'portal.auth','prefix' => "purchase-order-supplier",'as' => "purchase-order-supplier."],function(){
			Route::get('/',['as' => "index",'uses' => "PurchaseOrderSupplierController@index"]);
			Route::get('create/{id?}',['as'=>"create",'uses' => "PurchaseOrderSupplierController@create"]);
			Route::post('create/{id?}',['as'=>"store",'uses' => "PurchaseOrderSupplierController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "PurchaseOrderSupplierController@edit"]);
			Route::post('edit/{id?}',['uses' => "PurchaseOrderSupplierController@update"]);
			Route::post('add-to-cart/{id?}',['as'=>"add-to-cart",'uses' => "PurchaseOrderSupplierController@add_to_cart"]);
			Route::post('search',['as'=>"search",'uses' => "PurchaseOrderSupplierController@search"]);

		});

		Route::group(['middleware'=>'portal.auth','prefix' => "order-list",'as' => "order_list."],function(){
			Route::get('/',['as' => "index",'uses' => "OrderListController@index"]);
			
			Route::get('create',['as'=>"create",'uses' => "OrderListController@create"]);
			Route::post('create',['as'=>"store",'uses' => "OrderListController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "OrderListController@edit"]);
			Route::post('edit/{id?}',['uses' => "OrderListController@update"]);
			Route::get('show/{id?}',['as'=>"show",'uses' => "OrderListController@show"]);
			Route::post('stock-in',['as'=>"stock-in",'uses' => "OrderListController@stock_in"]);	
			Route::get('print/{id?}',['as'=>"print",'uses' => "OrderListController@print_purchase_oder"]);	
			Route::get('recive-warehouse/{id?}',['as'=>"recieve",'uses' => "OrderListController@recieve"]);	
			Route::get('remove-product/{id}/{cart_id?}',['as'=>"remove-product",'uses' => "OrderListController@remove_product"]);

		});
		Route::group(['middleware'=>'portal.auth','prefix' => "order-list-details",'as' => "order_list_details."],function(){
			Route::get('/{id?}/{supplier?}',['as' => "index",'uses' => "OrderListController@order_details"]);
			// Route::get('create',['as'=>"create",'uses' => "OrderListController@create"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "OrderListDetailController@edit"]);
			Route::post('update',['as'=>'update','uses' => "OrderListDetailController@update"]);
			Route::get('recieve/{id?}',['as'=>'recieve','uses' => "OrderListDetailController@recieve"]);
			Route::post('recieve/{id}',['as'=>"purchase",'uses' => "OrderListDetailController@store"]);
			Route::get('print/{id?}/{sid?}',['as'=>'print','uses' => "OrderListDetailController@print_purchase_oder"]);

			
		});

		


		Route::group(['middleware'=>'portal.auth','prefix' => "product-category",'as' => "product_category."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductCategoryController@index"]);
			Route::get('create',['as'=>"create",'uses' => "ProductCategoryController@create"]);
			Route::post('create',['uses' => "ProductCategoryController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "ProductCategoryController@edit"]);
			Route::post('edit/{id}',['as'=>'update','uses' => "ProductCategoryController@update"]);
			Route::post('destroy/{id}',['as'=>'delete','uses' => "ProductCategoryController@destroy"]);
			
			
		});
		Route::group(['middleware'=>'portal.auth','prefix' => "quick-report",'as' => "quick_report."],function(){
			Route::get('/',['as' => "index",'uses' => "QuickReportController@index"]);
			Route::get('stock-out-report/{id?}',['as'=>'stock_out_report','uses' => "QuickReportController@stock_out_report"]);
			Route::get('create',['as'=>"create",'uses' => "QuickReportController@create"]);
			Route::post('create',['uses' => "QuickReportController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "QuickReportController@edit"]);
			Route::post('update',['as'=>'update','uses' => "QuickReportController@update"]);	
			Route::get('export-pdf-in',['as'=>'export_pdf_in','uses' => "QuickReportController@export_pdf"]);	
			Route::get('export-pdf-out',['as'=>'export_pdf_out','uses' => "QuickReportController@export_pdf"]);	
		});

			Route::group(['middleware'=>['portal.auth','store_admin'],'prefix' => "transactions",'as' => "transactions."],function(){
			Route::get('/',['as' => "index",'uses' => "TransactionController@index"]);

			Route::post('/',['uses' => "TransactionController@export_all"]);


			Route::get('stock-out-report/{id?}',['as'=>'stock_out_report','uses' => "TransactionController@stock_out_report"]);
			Route::get('create',['as'=>"create",'uses' => "TransactionController@create"]);
			Route::post('create',['uses' => "TransactionController@store"]);
			Route::get('edit/{id?}',['as'=>'edit','uses' => "TransactionController@edit"]);
			Route::post('update',['as'=>'update','uses' => "TransactionController@update"]);	
			Route::get('filter/{date?}',['as'=>'filter','uses' => "TransactionController@filter_date"]);


			Route::get('today-sales',['as'=>'today','uses' => "TransactionController@sales_today"]);	
			Route::post('today-sales',['uses' => "TransactionController@export_sales_today"]);	
			Route::get('week-sales',['as'=>'week','uses' => "TransactionController@week_sales"]);	
			Route::post('week-sales',['uses' => "TransactionController@export_week_sales"]);	
			Route::get('monthly-sales',['as'=>'month','uses' => "TransactionController@monthly_sales"]);	
			Route::post('monthly-sales',['uses' => "TransactionController@export_monthly_sales"]);	
			Route::get('summary-report',['as'=>'summary-report','uses' => "TransactionController@summary_report"]);	
	
		});

			Route::group(['middleware'=>['portal.auth','store_admin'],'prefix' => "approved-order",'as' => "approved-order."],function(){
			Route::get('/',['as' => "index",'uses' => "ApprovedOrderController@index"]);
			Route::get('details/{id?}',['as' => "details",'uses' => "ApprovedOrderController@details"]);
			Route::get('cancel/{id?}',['as' => "cancel",'uses' => "ApprovedOrderController@cancel_order"]);
			Route::get('destroy/{id?}',['as' => "delete",'uses' => "ApprovedOrderController@destory"]);
			// Route::get('recieve-order',['as' => "recieve-order",'uses' => "DeliverController@complete_transaction"]);
			
		});
			Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "expenses",'as' => "expenses."],function(){
			Route::get('/',['as' => "index",'uses' => "ExpensesController@index"]);
			Route::get('create',['as' => "create",'uses' => "ExpensesController@create"]);
			Route::post('create',['as' => "store",'uses' => "ExpensesController@store"]);
			Route::get('edit/{id?}',['as' => "edit",'uses' => "ExpensesController@edit"]);
			Route::post('edit/{id?}',['uses' => "ExpensesController@update"]);
			Route::get('delete/{id?}',['as' => "delete",'uses' => "ExpensesController@destroy"]);
			// Route::get('recieve-order',['as' => "recieve-order",'uses' => "DeliverController@complete_transaction"]);
		});
			Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "expense-title",'as' => "expense_title."],function(){
			Route::get('/',['as' => "index",'uses' => "ExpenseTitleController@index"]);
			Route::post('/',['uses' => "ExpenseTitleController@store"]);
			Route::get('delete/{id?}',['as' => "delete",'uses' => "ExpenseTitleController@destroy"]);
			Route::post('update',['as'=>"update",'uses' => "ExpenseTitleController@update"]);
			
	
		});
			Route::group(['middleware'=>['portal.auth','store_admin'],'prefix' => "short-over-report",'as' => "short_over_report."],function(){
			Route::get('/',['as' => "index",'uses' => "ShortOverReportController@index"]);
			Route::get('branch/{id?}',['as' => "branch",'uses' => "ShortOverReportController@branch"]);
		});
		Route::group(['middleware'=>['portal.auth','admin'],'prefix' => "settings",'as' => "settings."],function(){
			Route::get('company-logo',['as' => "company_logo",'uses' => "ShortOverReportController@company_logo"]);
			});
		Route::group(['middleware'=>['portal.auth'],'prefix' => "logs",'as' => "logs."],function(){
			Route::get('/',['as' => "index",'uses' => "LogsController@index"]);
			
			Route::get('auth-logs',['as' => "auth_logs",'uses' => "LogsController@auth_logs"]);
			});


	});
});
