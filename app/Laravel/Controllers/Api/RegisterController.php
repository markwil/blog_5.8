<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Controller;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\ProductTransformer;
use App\Laravel\Transformers\TransactionTransformer;
use App\Laravel\Transformers\UserTransformer;

use App\Laravel\Requests\Api\PageRequest;
 
use App\Laravel\Requests\Api\RegisterRequest;
 

use App\Laravel\Models\TransactionHeader as Header;
use App\Laravel\Models\TransactionDetail as Detail;
use App\Laravel\Models\TransactionDiscount as Discount;
use App\Laravel\Models\TransactionProductIngredient as Ingredient;
 
use App\Laravel\Models\Product;
use App\Laravel\Models\User;

use Illuminate\Support\Collection;

use GuzzleHttp\Client as Curl;

use Auth, JWTAuth, Helper, Str, ImageUploader, Carbon, DB;

class RegisterController extends Controller {

	protected $response = array();

	public function __construct() {
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

    public function index ($format="") {
       return 'dfdsfsfs';
    }

    public function store(RegisterRequest $request, $format=""){
        // dd($request->all());
      // $user = $request->user();  

        try{
            DB::beginTransaction();

            $user = new User;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->save();
            DB::commit();

            $this->response['status'] = TRUE;
            $this->response['status_code'] = "NEW_ACCOUNT_CREATED";
            $this->response['msg'] = "New order was created.";
            $this->response['data'] = $this->transformer->transform($user, new TransactionTransformer,'item');
            $this->response_code = 200;
            goto callback;
                 

        }catch(\Exception $e){
            DB::rollBack();
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "SERVER_ERROR";
            $this->response['msg'] = "Server Error. Please try again.".$e->getMessage();
            $this->response_code = 500;
            goto callback;
        }

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function user_list($format=""){
        
            $user  = User::all();
            if($user)
            {
                $this->response['status'] = TRUE;
            $this->response['status_code'] = "USER_ACCOUNT_LIST";
            $this->response['msg'] = "USER LIST.";
            $this->response['data'] = $this->transformer->transform($user, new TransactionTransformer,'Collection');
            $this->response_code = 200;
            }
            
            goto callback;
    
        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

 

    

    public function destroy(PageRequest $request, $format=""){

      $user = User::find($request->id);
        if(!$user)
        {
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "USER_NOT_FOUND";
            $this->response['msg'] = "Server Error. Please try again.";
            $this->response_code = 500;
            goto callback;
        }

       if($user->delete()){
            $this->response['status'] = TRUE;
            $this->response['status_code'] = "DATA_DELETED";
            $this->response['msg'] = "User has been deleted.";
             
            $this->response_code = 200;
            goto callback;
       } 
            
         callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function search(PageRequest $request,$format = "")
    {
       $user =   User::find($request->id);
       if($user)
        {
          $this->response['status'] = TRUE;
            $this->response['status_code'] = "USER_ACCOUNT_LIST";
            $this->response['msg'] = "USER LIST.";
            $this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
            $this->response_code = 200;
        }


       callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
    }
}
}