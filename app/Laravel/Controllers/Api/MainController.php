<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Controller;

use App\Laravel\Transformers\TransformerManager;

use App\Laravel\Requests\Api\PageRequest;

use Auth, JWTAuth, Helper, Str, ImageUploader, Carbon, Input;

class MainController extends Controller {

	protected $response = array();

	public function __construct() {
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

    public function index (PageRequest $request, $format="") {

        $this->response['status'] = TRUE;
         $this->response['msg'] = "INDEX PAGE";
        $this->response['status_code'] = "INDEX PAGE";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}