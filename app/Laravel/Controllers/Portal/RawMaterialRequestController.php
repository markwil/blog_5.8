<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\RawMaterialRequest;
use App\Laravel\Models\Order;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\AddCartRequest;
use App\Laravel\Requests\Portal\RawMaterialRequestRequest;
use Illuminate\Http\Request;
// use Illuminate\Support\Str;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Auth,Session;

class RawMaterialRequestController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['raw'] = StockIn::all();
	}


	public function addtocart(AddCartRequest $request)
	{
		
		$code = $request->product_code;
		$qty = $request->product_qty;
		$unit = $request->product_unit;

		// return count($code);
		$order_id = new RawMaterialRequest;
		$lastInvoiceID = $order_id->orderBy('id', 'DESC')->pluck('id')->first();
		$newInvoiceID = $lastInvoiceID.date('Y'). + 1;
		for ($i=0; $i < count($code) ; $i++) { 
			
			$data = array( 
				"order_id"		=> $newInvoiceID,
				"branch_id"		=> Auth::user()->branch_id,
				"product_unit"	=>$unit[$i] ,
				"product_code" => $code[$i], 
				"product_qty"		=> $qty[$i], 
				"created_at"		=> date('Y-m-d'), 

			);
			$insert_data[] = $data;

		}
			if (RawMaterialRequest::insert($insert_data)) {
				$request->session()->forget('cart');
				session()->flash('notification-status',"success");
				session()->flash('notification-msg',"Order Added Successfully");
				return redirect()->route('portal.raw-material-request.index');
			} 


	}

	public function index(PageRequest $request){

		// $this->data['accounts'] = Account::orderBy('created_at',"DESC")->paginate($per_page);
		$this->data['page_title'] = "Product";
		$this->data['material_request'] = RawMaterialRequest::where('branch_id',Auth::user()->branch_id)
																->groupby('order_id')
																->get();
		$this->data['raw'] = DB::table('raw_materials')->paginate(10);
		return view('portal.raw-material-request.index',$this->data);
	}

	public function create(PageRequest $request){

		$this->data['page_title'] = "Add Product";

		$this->data['product'] = StockIn::all();
		
		$this->data['cart'] = $request->session()->get('cart');
		$oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
		$cart = new Order($oldCart);

		return view('portal.raw-material-request.create',$this->data);
	}

	public function store(AddCartRequest $request){
		$order_id = new RawMaterialRequest();
		$lastInvoiceID = $order_id->orderBy('id', 'DESC')->pluck('id')->first();
		$newInvoiceID = $lastInvoiceID + 1;

		$x = $request->only('product_qty','product_unit','product_code','branch_id');

		$oldCart = Session::has('cart') ?  Session::get('cart') : null;

		Session::push('cart',$x);
		return redirect()->route('portal.raw-material-request.create');

		try{
			

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Order Added Successfully");
			return redirect()->route('portal.raw-material-request.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){

		$this->data['raw_material'] = RawMaterialRequest::where('order_id',$id)->get();
		if(!$this->data['raw_material']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->back();
		}

		return view('portal.raw-material-request.edit',$this->data);
	}

	public function update(RawMaterialRequest $request, $id = NULL){
		$account = RawMaterial::find($id);

		if(!$account){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.raw-material.index');
		}
		try{

			$data = DB::Table('raw_materials')
			->where('id',$id)
			->update([
				"name" => $request->name,

				"slug" => str_slug($request->name,'-'),
				"type" => $request->type,
				"value" => $request->value,
			]);

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

	public function transaction_complete($id=NULL)
	{
		$account = RawMaterialRequest::where('id',$id)->first();
										
		if(!$account){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.stock-request.index');
		}
		try{

			$data = DB::Table('raw_material_requests')
						->where('order_id',$id)
						->update([
								 "product_request_status" => "completed",
								]);

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Order  has been recieve Successfully.");
			return redirect()->route('portal.raw-material-request.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}


	public function destroy($id=NULL)
	{

		$data = RawMaterial::find($id);

		if (!$data)
		{
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.raw-material.index');
		}

		if($data->delete())
		{
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Data has been Deleted Successfully.");
			return redirect()->back();
		}


	}

public function search(Request $request)
{

	$data = StockIn::where('product_name','LIKE',$request->name.'%')->get();

	return json_decode($data,true);

	
}





}
