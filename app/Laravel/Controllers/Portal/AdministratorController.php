<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
 use App\Laravel\Models\User as Account;
use App\Laravel\Models\Branch;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\AdministratorRequest;
use App\Laravel\Requests\Portal\AdministratorPasswordRequest;

use App\Laravel\Requests\Portal\PageRequest;

/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB;

class AdministratorController extends Controller{

	protected $data;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['types'] = ['super_user' => "Admin",'store_admin'=>'store-admin','accounting'=>"accounting"];

	}

	public function index(PageRequest $request){

		$this->data['page_title'] = "Administrator";
		$per_page = $request->get('per_page',10);
		if(auth()->user()->type == "super_user")
		{
		$this->data['accounts'] = Account::where('id','<>',1)
		->where('type','<>','cashier')
		->orderBy('created_at',"DESC")
		->paginate($per_page);
		}
		else
		{
			$this->data['accounts'] = Account::where('id','<>',1)
			->where('branch_id','=',auth()->user()->branch_id)
			->orderBy('created_at',"DESC")->paginate($per_page);
		}
		
		return view('portal.administrator.index',$this->data);
	}

	public function create(){
		$this->data['page_title'] = "Administrator | Create";
		if(auth()->user()->branch_id <> 1)
		{
			$this->data['branches'] = DB::Table('branches')->where('id','<>','1')->get();
		}
		else
		{
			$this->data['branches'] = DB::Table('branches')->get();
		}
		
		return view('portal.administrator.create',$this->data);
	}

	public function store(AdministratorRequest $request){

		$branch = explode(':',$request->branch_id);
	

		try{
			DB::beginTransaction();
			$account = new Account;

			$account->name = $request->name;
			$account->username = Str::lower($request->username);
			$account->email = Str::lower($request->email);
			$account->password = bcrypt($request->password);
      		$account->status = $request->status;
			$account->branch_id = $branch[0];
			$account->branch_location = $branch[1];
			$account->type = $request->type;
			$account->pin_code = $request->pin_code;
			$account->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New account has been created.");
			return redirect()->route('portal.administrator.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($administrator_id = NULL){
		$this->data['account'] = Account::find($administrator_id);
		$this->data['branch'] = Branch::where('id','<>',1)->get();

		if(!$this->data['account']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.administrator.index');
		}
		return view('portal.administrator.edit',$this->data);
	}

	public function update(PageRequest $request, $administrator_id = NULL){
		
		// return $request->all();
		$account = Account::find($administrator_id);

		if(!$account){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.administrator.index');
		}
		try{
			DB::beginTransaction();
			$account->name = $request->get('name');
			$account->username = Str::lower($request->get('username'));
			$account->email = Str::lower($request->get('email'));
			$account->pin_code = $request->pin_code;
			$account->status = $request->get('status');
			$account->branch_location = $request->get('branch_location');
			$account->type = $request->get('type');
			$account->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.administrator.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function reset_password($administrator_id = NULL){
		$this->data['page_title'] = "Reset Password";
		$this->data['account'] = Account::find($administrator_id);

		if(!$this->data['account']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.administrator.index');
		}
		return view('portal.administrator.reset-password',$this->data);
	}

	public function update_reset_password(AdministratorPasswordRequest $request,$administrator_id){

		$account = Account::find($administrator_id);

		if(!$account){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.administrator.index');
		}

		try{
			DB::beginTransaction();
			$account->password = bcrypt($request->get('password'));
			$account->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New password has been configured.");
			return redirect()->route('portal.administrator.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}
}
