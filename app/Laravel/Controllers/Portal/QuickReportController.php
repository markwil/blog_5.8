<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\StockOut;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class QuickReportController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
 
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];

	}
	
	public function index($id=NULL){
      	
		$this->data['page_title'] = "Inventory Quick Reports";
		$this->data['stocks'] =  DB::table('stock_in')
		->orderBy('created_at','desc')
		->paginate(10);
		return view('portal.quick-report.index',$this->data);
	}

	public function stock_out_report($id=NULL){
 
	$this->data['page_title'] = "Inventory Quick Reports";
	
	$this->data['stocks'] =  StockOut::orderBy('created_at','desc')->paginate(10);
	$this->data['total_sales'] =  StockOut::sum('price');
											
		return view('portal.quick-report.stock-out-report',$this->data);
	}

	public function create($id=NULL){
		return "create";
		$this->data['products'] = SupplierProduct::where('supplier_id',$id)->paginate('5');
		return view('portal.purchase-order-supplier.create',$this->data);
	}

	public function store(SupplierProductRequest $request,$id=NULL){
		
  // dd($request->all());
		try{
			DB::beginTransaction();
			
			$supplier = new SupplierProduct;
			$supplier->fill($request->all());

			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.supplier-product.create',[Request::segment(3)]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['supplier'] = Supplier::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}

	public function update(SupplierRequest $request, $id = NULL){
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->name = $request->name;
			$supplier->address = $request->address;
			$supplier->contact = $request->contact;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

public function add_to_cart(AddToCartRequest $request,$id=NULL)
{
			try{
			DB::beginTransaction();
			$cart = new CartOrder;
			$cart->fill($request->all());
			$cart->save();
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product has been added to Order List.");
			return redirect()->route('portal.purchase-order-supplier.create',[Request::segment(3)]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

}

public function show(Request $request,$id=NULL){
	$this->data['supplier_name'] = Supplier::where('id',$id)->first();
	$this->data['suppliers'] = CartOrder::where('supplier_id',$id)->get();
	 $this->data['count'] = CartOrder::where('supplier_id',$id)->count();
	return view('portal.order_list.show',$this->data);
	}

	public function stock_in(StockInRequest $request){

       // return $price = $request->price; 
		try{
			
			DB::beginTransaction();
			$total = 0;
			$item_code = $request->only('item_code'); 
			foreach ($item_code as $code) {
			for ($i=0; $i < count($code); $i++) { 
				$data = new StockIn;
				$data->product_name = $code[$i];
				$data->supplier_price = $request->price[$i];
				$data->product_qty = $request->qty[$i];
				$data->marked_up += $request->price[$i] * $request->qty[$i];
				$data->save();
			}
		}
		DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.order_list.show',[1]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

	public function order_details($id=NULL)
	{


		 $this->data['cart'] = DB::Table('cart_orders as orders')
		->select(DB::raw('SUM(DISTINCT orders.total_amount) as total_amount,detail.description,detail.price,SUM(DISTINCT detail.qty) as qty,SUM(DISTINCT stock.qty) as available_stock,detail.product_name,detail.id as pid'))
		->join('cart_order_details as  detail','orders.product_id','=','detail.product_id')
		->join('stock_in as stock','stock.description','=','detail.description')
		->where('detail.supplier_id',$id)
		->groupBy('detail.description')
		->orderBy('orders.created_at','desc')
		->get();

		return view('portal.order-list-details.index',$this->data);
	}


	public function export_pdf()
	{
		
		
		$this->data['page_title'] = "Sales Journal";
			$header = array_merge([
				"Date", "Client/Buyer", "Particulars", "Transaction #","Quantity" ,"Unit Price","Amount"
			]);
			Excel::create("Exported Summary Data", function($excel) use($header){
				$excel->setTitle("Exported Summary Data")
			    		->setCreator('HSI')
			          	->setCompany('ZII TEA.')
			          	->setDescription('Exported Summary data.');
			    $excel->sheet('Summary', function($sheet) use($header){
			    	$sheet->freezeFirstRow();
			    	$sheet->setOrientation('landscape');
			       	$sheet->row(1, $header);
			       	$counter = 2;

			       switch (Request::segment(2)) {
			       	case 'export-pdf-out':
			       	$transactions =  $this->data['stocks'] =  DB::table('stock_out')->orderBy('created_at','desc')->get();
			       	break;
			       	default:
			       	$transactions =  $this->data['stocks'] =  DB::table('stock_in')->orderBy('created_at','desc')->get();
			        break;
			       }
			       

			       	foreach ($transactions as $transaction) {
			       		$data = [$transaction->created_at, $transaction->description,$transaction->transaction_id,$transaction->product_code,$transaction->qty,$transaction->price,$transaction->qty * $transaction->price];
			       		$sheet->row($counter++, $data);
			       	}  
			    });
			})->export('xls');
	
	}


	
}