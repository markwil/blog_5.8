<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\User;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\StockOut;
 use App\Laravel\Models\PurchaseOrderBranch;
 use App\Laravel\Models\ApprovedOrder;
 

use App\Notifications\AddToCart;
use App\Notifications\StockRequestNotification;




/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class PurchaseOrderBranchController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['page_title'] = "PURCHASE ORDER";
	}
	
	public function index(){
    	
    	$this->data['products'] = DB::table('stock_in')
    			->select(DB::raw('*,sum(qty) as total_qty,(select sum(qty) from stock_out where product_code = stock_in.product_code) as out_qty'))
    			->groupBy('product_code') 
    			->get();
		 return view('portal.purchase-order-branch.index',$this->data);
	}

	public function create($id=NULL){

		$this->data['products'] = SupplierProduct::where('supplier_id',$id)->paginate('5');
		return view('portal.purchase-order-supplier.create',$this->data);
	}

	public function store(SupplierProductRequest $request,$id=NULL){
		
  // dd($request->all());
		try{
			DB::beginTransaction();
			
			$supplier = new CartOrder;
			$supplier->fill($request->all());

			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.supplier-product.create',[Request::segment(3)]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['supplier'] = Supplier::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}

	public function update(SupplierRequest $request, $id = NULL){


		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->name = $request->name;
			$supplier->address = $request->address;
			$supplier->contact = $request->contact;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}
// AddToCartRequest
public function add_to_cart(AddToCartRequest $request,$id=NULL) //add to cart / supplier  
{
			// try{
			DB::beginTransaction();

			$order = CartOrder::where('cart_id',$request->cart_id)->where('status',5)->count();
			if($order > 0)
			{
				session()->flash('notification-status',"error");
				session()->flash('notification-msg',"You currently have a pending order on this supplier. Please wait until your Order has been approved");
				return redirect()->back();
			}
		$item = CartOrderDetail::where('product_code',$request->product_code)->where('cart_id',$request->cart_id)
		->where('supplier_id',$request->supplier_id)->count();
			if ($item > 0) {
			session()->flash('notification-status',"error");
			session()->flash('notification-msg',"Product has been already added to your  Order List.");
			return redirect()->back();
			}
		
			$cart_details = new CartOrderDetail;
			$cart_details->cart_id = auth()->user()->branch_id;
			$cart_details->supplier_id = $request->supplier_id;
			$cart_details->branch_id = $request->branch_id;
			$cart_details->product_code = $request->product_code;
			$cart_details->description = $request->description;
			$cart_details->qty = $request->qty;
			$cart_details->measurement = $request->measurement;
			$cart_details->measurement_qty = $request->measurement_qty;
			$cart_details->price = $request->price;
			$cart_details->total_amount = $request->price * $request->qty;
			if(	$cart_details->save())
			{
				$myApprovedOrder = ApprovedOrder::where('cart_id',auth()->user()->branch_id)->count();
				session()->put('approved_order',$myApprovedOrder);
			}
			DB::commit();
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product has been added to Order List.");
			return redirect()->back();

		// }catch(\Exception $e){
		// 	DB::rollBack();
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
		// 	return redirect()->back();
		// }

}

public function search(PageRequest $request)
{

	return StockIn::where('product_code',$request->code)->get();
}
	
}


