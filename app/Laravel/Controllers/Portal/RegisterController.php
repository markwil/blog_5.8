<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\Branch;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\ApprovedOrderDetail;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\StockInRequest;
use App\Laravel\Requests\Portal\StockOutRequest;
use App\Laravel\Requests\Portal\BranchRequest;
use App\Laravel\Requests\Portal\DeliverRequest;


use App\Notifications\Shipped;
use App\Notifications\StockRequestNotification;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel,Auth,PDF,Alert,Validator;

class RegisterController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){

		parent::__construct();


		array_merge($this->data, parent::get_data());
		 

	}

	public function index(PageRequest $request){

	$this->data['page_title'] = "register";
	toast('Success Toast','success');
	return view('portal.register.index',$this->data);

	
	}



	public function create($id=NULL){
    $this->data['page_title'] = "register";
	
	return view('portal.register.index',$this->data);
	}



	public function store(DeliverRequest $request){


		try{
		
					$data = new User;
					$data->name = $request->user;
					$data->username = $request->username;
					$data->save();
				 toast("Data added Successfully",'success');
			
				return redirect()->route('portal.register.create',$this->data);


		}catch(\Exception $e){
			DB::rollBack();
			$this->data['alert'] = toast("error",'error');
			return redirect()->route('portal.register.create',$this->data);
		}



	}

	 

 


}
