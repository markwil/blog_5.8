<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\ProductCategory;



/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request,ImageUploader;

class SupplierProductController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];

	}
	
	public function index(PageRequest $request,$id=NULL){
		// $per_page = $request->get('per_page',10);
		$this->data['products'] = SupplierProduct::orderBy('created_at',"DESC")->paginate(10);
		return view('portal.supplier-product.index',$this->data);
	}

	public function create($id=NULL){

		$this->data['products'] = SupplierProduct::where('supplier_id',$id)->get();
		$this->data['categories'] = ProductCategory::all();
		$this->data['category_count'] = ProductCategory::count();
		return view('portal.supplier-product.create',$this->data);
	}

	public function store(SupplierProductRequest $request){
		
// dd($request->all());

		DB::beginTransaction();
			
			$supplier = new SupplierProduct;
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/supplier");
			    $supplier->path = $image['path'];
			    $supplier->directory = $image['directory'];
			    $supplier->filename = $image['filename'];
			}
				
				$supplier->fill($request->all());
				$supplier->save();

			DB::commit();
		try{
			

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Proudct has been created.");
			return redirect()->route('portal.supplier-product.create',[Request::segment(3)]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['supplier'] = Supplier::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}

	public function update(SupplierRequest $request, $id = NULL){


		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->name = $request->name;
			$supplier->address = $request->address;
			$supplier->contact = $request->contact;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$supplier = SupplierProduct::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->back();
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}


	
}