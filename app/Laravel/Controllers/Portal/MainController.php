<?php 

namespace App\Laravel\Controllers\Portal;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Product;
use App\Laravel\Models\Branches;
use App\Laravel\Models\User;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\Branch;
use App\Laravel\Models\TransactionDetail;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\CartOrder;

use Carbon,Auth,DB;

class MainController extends Controller{

	protected $data;
	
	public function __construct(){
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());

	
				}
	public function index(){
		$this->data['page_title'] = "DASHBOARD";
		$a = CartOrder::where('cart_id',auth()->user()->branch_id)->count();
		session()->put('order_list',$a);
		$this->data['branch'] = Branch::where('id',auth()->user()->branch_id)->first();
		$this->data['total_partners'] = Partner::where('status','active')->count();
		$fod_month = Carbon::now()->startOfMonth()->format("Y-m-d");
		$eod_month = Carbon::now()->endOfMonth()->format("Y-m-d");
		$today = Carbon::now()->format("Y-m-d");
		$yesterday = Carbon::now()->subDay()->format("Y-m-d");

		$fod_week = Carbon::now()->startOfWeek()->format("Y-m-d");
		$eod_week = Carbon::now()->endOfWeek()->format("Y-m-d");

	    $this->data['daily_report'] =  DB::table('transaction_detail')
											// ->where('created_at',$today)
											->where('branch_id',auth()->user()->branch_id)
											->sum('amount');

									

		$commision_rate = 0;
		if(session()->get('is_admin','no') == "no"){
			$commision_rate = Auth::guard('partner')->user()->margin/100;
		}


		$this->data['today_sales'] = Sale::where('sale_date',$today)->sum('total');
		$this->data['today_sales'] -=($this->data['today_sales']*$commision_rate);
		$this->data['yesterday_sales'] = Sale::where('sale_date',$yesterday)->sum('total');
		$this->data['yesterday_sales'] -=($this->data['yesterday_sales']*$commision_rate);

		$this->data['this_month'] = Sale::where('sale_date','>=',$fod_month)
										->where('sale_date','<=',$eod_month)
										->sum('total');
										
		$this->data['this_month'] -=($this->data['this_month']*$commision_rate);

		$this->data['this_week'] = Sale::where('sale_date','>=',$fod_week)
										->where('sale_date','<=',$eod_week)
										->sum('total');
		$this->data['this_week'] -=($this->data['this_week']*$commision_rate);

		$this->data['thisweek'] = [];
		$this->data['lastweek'] = [];

		$start_week = Carbon::now()->startOfWeek();
		$last_week = Carbon::now()->startOfWeek()->subWeek();
		foreach(range(0,6) as $index => $value){
			$date = Carbon::now()->startOfWeek()->addDays($value);
			$lw_date = Carbon::now()->startOfWeek()->subWeek()->addDays($value);

			$w_sale = Sale::where('sale_date','>=',$date->format("Y-m-d"))
							->where('sale_date','<=',$date->format("Y-m-d"))
							->where(function($query){
								if(session()->get('is_admin','no') == "no"){
									$partner_code = Auth::guard('partner')->user()->code;
									return $query->where('partner_code',$partner_code);
								}
							})
							->sum('total');
			$lw_sale = Sale::where('sale_date','>=',$lw_date->format("Y-m-d"))
							->where('sale_date','<=',$lw_date->format("Y-m-d"))
							->where(function($query){
								if(session()->get('is_admin','no') == "no"){
									$partner_code = Auth::guard('partner')->user()->code;
									return $query->where('partner_code',$partner_code);
								}
							})
							->sum('total');
								
			$this->data['thisweek'][] = $w_sale-($w_sale*$commision_rate);
			$this->data['lastweek'][] = $lw_sale-($lw_sale*$commision_rate);

		}
		// if(session()->get('is_admin','no') == "no"){

		// });

		$this->data['branch_product_count'] = Product::where('branch_id',auth()->user()->branch_id)->count();

		$this->data['product_count'] = Stockin::count();

				$a = DB::Table('user')
										->leftjoin('branches','branches.id','=','user.branch_id')
										->where('branches.id',Auth::user()->branch_id)
										->where('user.id',Auth::user()->id)
										->pluck('branches.branch_name','user.name');
		
		foreach ($a as $key => $value) {
				$b = $value;
			}			

			// $this->data['branch_name'] = $b;		


		$this->data['daily_sales'] =  DB::table('transaction_detail')
			->where('created_at','>=',Carbon::today())
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');

		$this->data['mothly_sales'] =  DB::table('transaction_detail')
			->whereBetween('created_at',[ $fod_month, $eod_month])
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
		
			
		
			// warehouse

		if(auth()->user()->branch_id == 1)
		{
		$this->data['jan'] =  DB::table('transaction_in')->whereMonth('created_at','=',1)->sum('amount');

		 	$this->data['feb'] =  DB::table('transaction_in')->whereMonth('created_at','=',2)->sum('amount');
			$this->data['mar'] =  DB::table('transaction_in')->whereMonth('created_at','=',3)->sum('amount');
			$this->data['apr'] =  DB::table('transaction_in')->whereMonth('created_at','=',4)->sum('amount');
			$this->data['may'] =  DB::table('transaction_in')->whereMonth('created_at','=',5)->sum('amount');
			$this->data['june'] =  DB::table('transaction_in')->whereMonth('created_at','=',6)->sum('amount');
			$this->data['july'] =  DB::table('transaction_in')->whereMonth('created_at','=',7)->sum('amount');
			$this->data['aug'] =  DB::table('transaction_in')->whereMonth('created_at','=',8)->sum('amount');
			$this->data['sep'] =  DB::table('transaction_in')->whereMonth('created_at','=',9)->sum('amount');
			$this->data['oct'] =  DB::table('transaction_in')->whereMonth('created_at','=',10)->sum('amount');
			$this->data['nov'] =  DB::table('transaction_in')->whereMonth('created_at','=',11)->sum('amount');
			$this->data['dec'] =  DB::table('transaction_in')->whereMonth('created_at','=',12)->sum('amount');
		
			
		}
		else
		{
			$this->data['jan'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',1)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');

			$this->data['feb'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',2)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['mar'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',3)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['apr'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',4)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['may'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',5)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['june'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',6)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['july'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',7)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['aug'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',8)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['sep'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',9)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['oct'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',10)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['nov'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',11)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
			$this->data['dec'] =  DB::table('transaction_detail')
			->whereMonth('created_at','=',12)
			->where('branch_id',auth()->user()->branch_id)
			->sum('amount');
		}

		return view('portal.index',$this->data);
	}




	public function cashier($value='')
	{
		
		$this->data['page_title'] = "cashier";
		return view('portal.cashier',$this->data);
	}
}