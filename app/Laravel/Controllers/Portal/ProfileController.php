<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\User as Account;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\ProfilePasswordRequest;
use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Auth;

class ProfileController extends Controller{

	protected $data;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}
	
	
	public function edit_password(){
		return view('portal.profile.edit-password',$this->data);
	}

	public function update_password(ProfilePasswordRequest $request){
		// return $guard = session()->get('is_admin','no') == "yes" ? 'admin' :'partner';
		$guard = session()->get('is_admin','no') == "yes" ? 'admin' : 'partner';
		 $account = Auth::guard($guard)->user();

		try{
			DB::beginTransaction();
			$account->password = bcrypt($request->get('password'));
			$account->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New password has been configured.");
			return redirect()->route('portal.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}
}