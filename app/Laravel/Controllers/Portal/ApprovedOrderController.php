<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\ApprovedOrder;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class ApprovedOrderController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
 
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];

	}
	
	public function index(){
      	
     

	$this->data['items'] = DB::table('approved_order_header')
		->where('branch_id',auth()->user()->branch_id)
		->where('status',1)
		->orwhere('status',3)
		->get();	
		auth()->user()->notifications->markAsRead();
		return view('portal.approved-order.index',$this->data);
	}
	public function destory($id=NULL)
	{
		$supplier = ApprovedOrder::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"order not found.");
			return redirect()->back();
		}
		try{
			DB::beginTransaction();

			CartOrderDetail::where('cart_id',$supplier->cart_id)->delete();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Order has been removed.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}
	public function details($id=NULL) //Display order Details
	{
		$this->data['cart'] = CartOrderDetail::where('cart_id',$id)->where('branch_id',auth()->user()->branch_id)->get();
		return view('portal.approved-order.details',$this->data);
	}
	
	public function cancel_order($id=NULL)
	{
		// $item = CartOrder::find($id);
		ApprovedOrder::where('id',$id)->update(['status'=>3]);
		CartOrderDetail::where('cart_id',$id)->update(['status'=>3]);
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Your Order is Successfully Canceled");
		return redirect()->back();
	}
	


	
}