<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\TransactionIn;
use App\Laravel\Models\TransactionInDetail;
use App\Laravel\Models\Branch;
use App\Laravel\Models\User;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\TransactionDetail;
use App\Laravel\Models\Attendance;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;


// use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class TransactionController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();

	}
	
	public function index(){	
	

	  $this->data['transactions'] = DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('a.status',"paid")
	  				->where('b.product_name','<>','Seniors Citizen/PWD (20%)')
	  				->paginate(10);
		
		$this->data['page_title'] = "Transaction";
		return view('portal.transaction.index',$this->data);
	}

	public function export_all(){

		$this->data['page_title'] = "Sales Journal";
			$header = array_merge([
				"Date", "Client/Buyer", "Particulars", "Transaction #","Month","Quantity" ,"Unit Price","Amount"
			]);
			Excel::create("Exported Summary Data", function($excel) use($header){
				$excel->setTitle("Exported Summary Data")
			    		->setCreator('HSI')
			          	->setCompany('ZII TEA')
			          	->setDescription('Exported  Monthly Sales Report.');
			     
			    $excel->sheet('Summary', function($sheet) use($header){
			    	$sheet->freezeFirstRow();
			    	$sheet->setOrientation('landscape');
			       	$sheet->row(1, $header);

			       	$counter = 2;
			       	$today = Carbon::now()->format("Y-m-d");
			       $fod_week = Carbon::now()->startOfWeek()->format("Y-m-d");
					$eod_week = Carbon::now()->endOfWeek()->format("Y-m-d");
			    	 $transactions = DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('b.created_at','>=',$fod_week)
	  				->where('b.created_at','<=',$eod_week)
	  				->where('a.status',"paid")
	  				->get();

			       	foreach ($transactions as $transaction) {
			       		$data = [date('Y M d',strtotime($transaction->created_at)),"Walkin",$transaction->product_name,$transaction->transaction_id,date('M',strtotime($transaction->created_at)),$transaction->qty,$transaction->price,$transaction->amount];
			       		$sheet->row($counter++, $data);
			       	}  
			    });
			})->export('xls');
	}

	public function week_sales($dateFrom=NULL,$dateTo=NULL){	

		$this->data['page_title'] = "Transaction";
		$today = Carbon::now()->format("Y-m-d");
		$yesterday = Carbon::now()->subDay()->format("Y-m-d");
		$fod_week = Carbon::now()->startOfWeek()->format("Y-m-d");
		$eod_week = Carbon::now()->endOfWeek()->format("Y-m-d");
		
		$this->data['transactions'] = DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('a.status',"paid")
	  				->where('b.created_at','<=',$eod_week)
	  				->where('b.created_at','>=',$fod_week)
					->paginate(10);
		return view('portal.transaction.index',$this->data);
	}

	public function export_week_sales(){

		$this->data['page_title'] = "Sales Journal";
			$header = array_merge([
				"Date", "Client/Buyer", "Particulars", "Transaction #","Month","Quantity" ,"Unit Price","Amount","Sales Revenue","Cash","Account Recievable","Representation","Salse Discount","Category",
			]);
			Excel::create("Exported Summary Data", function($excel) use($header){
				$excel->setTitle("Exported Summary Data")
			    		->setCreator('HSI')
			          	->setCompany('ZII TEA')
			          	->setDescription('Exported  Monthly Sales Report.');
			     
			    $excel->sheet('Summary', function($sheet) use($header){
			    	$sheet->freezeFirstRow();
			    	$sheet->setOrientation('landscape');
			       	$sheet->row(1, $header);

			       	$counter = 2;
			       	$today = Carbon::now()->format("Y-m-d");
			       $fod_week = Carbon::now()->startOfWeek()->format("Y-m-d");
					$eod_week = Carbon::now()->endOfWeek()->format("Y-m-d");
			    	 $transactions =  DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('a.status',"paid")
	  				->where('b.created_at','<=',$eod_week)
	  				->where('b.created_at','>=',$fod_week)
					->get();

			       	foreach ($transactions as $transaction) {
			       		$data = [date('Y M d',strtotime($transaction->created_at)),"Walkin",$transaction->product_name,$transaction->transaction_id,date('M',strtotime($transaction->created_at)),$transaction->qty,$transaction->price,$transaction->amount,$transaction->amount,$transaction->amount,"--",];
			       		$sheet->row($counter++, $data);
			       	}  
			    });
			})->export('xls');
	}


	public function monthly_sales(){	
		$this->data['page_title'] = "Transactions";
		$today = Carbon::now()->format("Y-m-d");
		$fod_month = Carbon::now()->startOfMonth()->format("Y-m-d");
		$eod_month = Carbon::now()->endOfMonth()->format("Y-m-d");
		$this->data['transactions'] =  DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('a.status',"paid")
	  				->whereBetween('a.created_at',[ $fod_month, $eod_month])
	  				->where('a.branch_id',auth()->user()->branch_id)
					->paginate(10);
		return view('portal.transaction.index',$this->data);
	}
	public function export_monthly_sales(){

		$this->data['page_title'] = "Sales Journal";
			$header = array_merge([
				"Date", "Client/Buyer", "Particulars","Item Code","Month", "Transaction #","Quantity" ,"Unit Price","Amount"
			]);
			Excel::create("Exported Summary Data", function($excel) use($header){
				$excel->setTitle("Exported Summary Data")
			    		->setCreator('HSI')
			          	->setCompany('ZII TEA')
			          	->setDescription('Exported  Monthly Sales Report.');
			     
			    $excel->sheet('Summary', function($sheet) use($header){
			    	$sheet->freezeFirstRow();
			    	$sheet->setOrientation('landscape');
			       	$sheet->row(1, $header);

			       	$counter = 2;
			       	$today = Carbon::now()->format("Y-m-d");
			       	$fod_month = Carbon::now()->startOfMonth()->format("Y-m-d");
			       	$eod_month = Carbon::now()->endOfMonth()->format("Y-m-d");
			     $transactions =  DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('a.status',"paid")
	  				->whereBetween('a.created_at',[ $fod_month, $eod_month])
	  				->where('a.branch_id',auth()->user()->branch_id)
					->get();
			  
			       	foreach ($transactions as $transaction) {
			       	$data = [date('Y M d',strtotime($transaction->created_at)),"Walkin",$transaction->product_name,$transaction->transaction_id,date('M',strtotime($transaction->created_at)),$transaction->qty,$transaction->price,$transaction->amount];
			       		$sheet->row($counter++, $data);
			       	}  
			    });
			})->export('xls');
	}

	public function sales_today(){	
		$this->data['page_title'] = "Transactions";
		$today = Carbon::now()->format("Y-m-d");

// $this->data['transactions'] = TransactionDetail::whereDate('created_at',$today)
// 														->where('price','>',0)
// 														->paginate(10);
		 $this->data['transactions'] = DB::table('transaction_header as a')
	  				->join('transaction_detail as b','b.transaction_id','=','a.id')
	  				->where('b.branch_id',auth()->user()->branch_id)
	  				->where('a.status',"paid")
	  				->whereDate('a.created_at',$today )
					->paginate(10);

		return view('portal.transaction.index',$this->data);
	}
	public function export_sales_today(){

		$this->data['page_title'] = "Sales Journal";
			$header = array_merge([
				"Date", "Client/Buyer", "Particulars", "Transaction #","Quantity" ,"Unit Price","Amount"
			]);
			Excel::create("Exported Summary Data", function($excel) use($header){
				$excel->setTitle("Exported Summary Data")
			    		->setCreator('HSI')
			          	->setCompany('ZII TEA')
			          	->setDescription('Exported Summary data.');
			     
			    $excel->sheet('Summary', function($sheet) use($header){
			    	$sheet->freezeFirstRow();
			    	$sheet->setOrientation('landscape');
			       	$sheet->row(1, $header);

			       	$counter = 2;
			       	$today = Carbon::now()->format("Y-m-d");
			       	$fod_month = Carbon::now()->startOfMonth()->format("Y-m-d");
			       	$eod_month = Carbon::now()->endOfMonth()->format("Y-m-d");
			     $transactions = TransactionDetail::whereDate('created_at',$today)->where('branch_id',auth()->user()->branch_id)->get();

			       	foreach ($transactions as $transaction) {
			       		$data = [date('Y M d',strtotime($transaction->created_at)),"Walkin",$transaction->product_name,$transaction->transaction_id,date('M',strtotime($transaction->created_at)),$transaction->qty,$transaction->price,$transaction->amount];
			       		$sheet->row($counter++, $data);
			       	}  
			    });
			})->export('xls');
	}


	public function all_transaction(){

		$this->data['page_title'] = "Sales Journal";
			$header = array_merge([
				"Date", "Client/Buyer", "Particulars", "Transaction #","Quantity" ,"Unit Price","Amount"
			]);

			
			Excel::create("Exported Summary Data", function($excel) use($header){
				$excel->setTitle("Exported Summary Data")
			    		->setCreator('HSI')
			          	->setCompany('ZII TEA.')
			          	->setDescription('Exported Summary data.');
			     
			    $excel->sheet('Summary', function($sheet) use($header){
			    	$sheet->freezeFirstRow();
			    	$sheet->setOrientation('landscape');
			       	$sheet->row(1, $header);

			       	$counter = 2;
			       	$today = Carbon::now()->format("Y-m-d");
			       	$fod_month = Carbon::now()->startOfMonth()->format("Y-m-d");
			       	$eod_month = Carbon::now()->endOfMonth()->format("Y-m-d");
			       	 $transactions = TransactionDetail::whereDate('created_at',$today)->where('branch_id',auth()->user()->branch_id);

			       	foreach ($transactions as $transaction) {
			       		$data = [$transaction->created_at,"Walkin",$transaction->product_name,$transaction->transaction_id,$transaction->qty,$transaction->price,$transaction->amount];
			       		$sheet->row($counter++, $data);
			       	}  
			    });
			})->export('xls');
	}
	
}