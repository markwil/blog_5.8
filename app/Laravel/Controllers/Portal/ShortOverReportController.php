<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\ApprovedOrder;
use App\Laravel\Models\Attendance;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class ShortOverReportController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
 
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];

	}
	
	public function index(){
		$this->data['short_over'] = DB::table('attendance as  a')
									->leftJoin('transaction_header as b','a.user_id','=','b.cashier_user_id')
									->leftJoin('transaction_detail as c','c.transaction_id','=','b.id')
									->select(DB::raw('a.*,sum(b.total) as total_sales,
									sum((a.one_thousand * 1000) + (five_hundred * 500) + (two_hundred * 200) + (one_hundred * 100) + (fifty_peso * 50) + (twenty_peso * 20) + (ten_peso * 10) + (five_peso * 5)   + (one_peso * 1)) as denomination_total,b.*,c.*'))

									->groupBy('a.user_id')
									->get();


	return view('portal.short-over-report.index',$this->data);
	}
	public function branch($id=NULL){
		 $this->data['short_over'] = DB::table('attendance as  a')
									->leftJoin('transaction_header as b','a.user_id','=','b.cashier_user_id')
									->leftJoin('transaction_detail as c','c.transaction_id','=','b.id')
									->select(DB::raw('a.*,sum(b.total) as total_sales,
									sum((a.one_thousand * 1000) + (five_hundred * 500) + (two_hundred * 200) + (one_hundred * 100) + (fifty_peso * 50) + (twenty_peso * 20) + (ten_peso * 10) + (five_peso * 5)   + (one_peso * 1)) as denomination_total,b.*,c.*'))
									->where('b.branch_id',$id)
									->groupBy('a.created_at')
									->get();


	return view('portal.short-over-report.index',$this->data);
	}
	
	
}