<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;



use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel;

class CashierController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){

		
		parent::__construct();
		
		
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['category'] = ProductCategory::all();
		$this->data['ingredients'] = RawMaterial::all();

	}

	public function index(PageRequest $request){
		
		// $this->data['accounts'] = Account::orderBy('created_at',"DESC")->paginate($per_page);
		$this->data['page_title'] = "Product";
	
		return view('portal.cashier.index',$this->data);
	}

	public function create(){

		$this->data['page_title'] = "Add Product";
		return view('portal.product.create',$this->data);
	}

	public function store(ProductRequest $request){
		
		
		
		

		try{
			
	 		$property = collect($request->ingredients)->combine(collect($request->quantity));
			$product = new Product;
			$product->name = $request->name;
			
			$product->price = $request->price;
			$product->description = $request->description;
			
			$product->slug = str_slug($request->name,'-');
			$product->property = $property;
			$product->category = $request->category;
			

			$product->save();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Added Successfully");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['data'] = Product::find($id);

		if(!$this->data['data']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.partner.index');
		}
		return view('portal.product.edit',$this->data);
	}

	public function update(ProductRequest $request, $id = NULL){
		$product = Product::find($id);

		if(!$product){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.raw-material.index');
		}
		try{
		
			$data = DB::Table('products')
						->where('id',$id)
						->update([
								 "name" => $request->name,
								 "quantity" => $request->quantity,
								 "slug" => str_slug($request->name,'-'),
								 "type" => $request->type,
								 "value" => $request->value,
								 "category" => $request->category,
								]);

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}
	
public function display()
{

	$this->data['products']=  Product::all();
	$this->data['page_title']= "show";
	return view('portal.product.show',$this->data);
}

public function destroy($id=NULL)
{

$data = Product::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.product.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}	
	

	

		
	
}