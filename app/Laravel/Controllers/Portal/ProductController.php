<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\TransactionInDetail;
use App\Laravel\Models\Ingredient;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;



use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel,Auth,ImageUploader;

class ProductController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){

		parent::__construct();


		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['category'] = ProductCategory::all();
		$this->data['ingredients'] = RawMaterial::all();
		$this->data['page_title'] = "PRODUCTS";
	}

	public function index(PageRequest $request){
		
	 $this->data['products'] = DB::table('products as a')
		->select(DB::raw('a.id,a.description,a.price,a.product_code,b.description as ingredient,sum(b.cost_unit) as total_cost_unit'))
		->leftJoin('ingredients as b','a.product_code','=','b.product_code')
		->where('a.status','<>',0)
		->groupBy('b.product_code')
		->orderBy('a.created_at','desc')
		->get();
		return view('portal.product.index',$this->data);
	}

	public function remove_product($id=NULL){

		try {
		DB::beginTransaction();
		$product = Product::find($id);
		$product->status = 0;
		$product->save();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Product has been Removed!");
		DB::commit();
		return redirect()->back();

		} catch (Exception $e) {
		DB::rollBack();
		session()->flash('notification-status',"error");
		session()->flash('notification-msg',"Product has been Removed!".$e->getMessage());
		return redirect()->back();
		}
		}
		public function remove_ingredient($id=NULL){
		try {
		DB::beginTransaction();
		$product = Ingredient::find($id);
		$product->delete();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Product Ingredient has been Removed!");
		DB::commit();
		return redirect()->back();
		} catch (Exception $e) {
		DB::rollBack();
		session()->flash('notification-status',"error");
		session()->flash('notification-msg',"Product has been Removed!".$e->getMessage());
		return redirect()->back();
		}
	}
	public function edit_qty($id=NULL,$qty=NULL){
		try {
		DB::beginTransaction();
		return $product = Ingredient::find($id);
		$product->qty = $qty;
		$product->save();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Ingredient has been modified!");
		DB::commit();
		return redirect()->back();

		} catch (Exception $e) {
		DB::rollBack();
		session()->flash('notification-status',"error");
		session()->flash('notification-msg',"Went something Wrong!".$e->getMessage());
		return redirect()->back();
		}
		
	}

	public function create(){

		$this->data['page_title'] = "Add Product";
		$this->data['raw_material'] = TransactionInDetail::groupBy('product_code')->get();
		$this->data['categories'] = ProductCategory::all();

		return view('portal.product.create',$this->data);
	}

	public function store(ProductRequest $request){
		// return $request->all();

			 $coode = $request['product_code'];
		try{
			
			DB::beginTransaction();

			$product = new Product;
				if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/about");
			    $product->path = $image['path'];
			    $product->directory = $image['directory'];
			    $product->filename = $image['filename'];
			}

				$product->branch_id = auth()->user()->branch_id;
				$product->product_code = $request->product_code;
				$product->category = $request->tea_category;
				$product->price = $request->price;
				$product->description = $request->name;
				$product->save();

				$x = $request->only('ingredients');

				foreach ($x as $data) {
					
					for ($i=0; $i <  count($data) ;$i++) { 

						$ingredients = explode(":",$request->ingredients[$i]); 
						$unit_cost = $ingredients[1] / $ingredients[2];

						$ingridient = new Ingredient;

						$ingridient->product_code = $request->product_code;
						$ingridient->description = $ingredients[0];
						$ingridient->cost_unit = $unit_cost * $request->qty[$i] ;
						$ingridient->qty = $request->qty[$i];
						$ingridient->mou = $request->mou[$i];
						$ingridient->measurement = $request->measurement;
						$ingridient->size = $request->size;
						$ingridient->category = $request->category;
						$ingridient->save();
					}

				}

				
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Added Successfully");
			return redirect()->route('portal.product.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['data'] = Product::find($id);

		if(!$this->data['data']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.partner.index');
		}
		return view('portal.product.edit',$this->data);
	}

	public function update(PageRequest $request, $id = NULL){

		try{
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/about");
			    $data = DB::Table('products')
						->where('id',$id)
						->update([
							"product_code" => $request->product_code,
							"description" => $request->description,
							"price" => $request->price,
							"price" => $request->price,
							"path" => $image['path'],
							"directory" => $image['directory'],
							"filename" => $image['filename'],
								]);
			}
			 $data = DB::Table('products')
						->where('id',$id)
						->update([
							"product_code" => $request->product_code,
							"description" => $request->description,
							"price" => $request->price,
							"price" => $request->price,
								]);
			

			// $product->fill($request->all());
			// $product->save();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}
	
public function display()
{

	$this->data['products']=  Product::all();
	$this->data['page_title']= "show";
	return view('portal.product.show',$this->data);
}

public function destroy($id=NULL)
{

$data = Product::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.product.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}	
	

	

		
	
}