<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
 use App\Laravel\Models\User as Account;
use App\Laravel\Models\Branch;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\User;
/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\AdministratorRequest;
use App\Laravel\Requests\Portal\AdministratorPasswordRequest;

use App\Laravel\Requests\Portal\PageRequest;

/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB;

class LogsController extends Controller{

	protected $data;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['types'] = ['super_user' => "Admin",'store_admin'=>'store-admin','accounting'=>"accounting"];

	}

	public function index(PageRequest $request){
		$this->data['page_title'] = "Logs";

		$trans = new StockIn;
		
	
		$sql = "*,(select name from user where user.id = audits.user_id)  as created_by,(select branch_id from user where user.id = audits.user_id)  as bid ";
		$this->data['logs'] = DB::table('audits')
		->select(DB::raw($sql))
		// ->where('branch_id',auth()->user()->branch_id)
		->get();

		
		return view('portal.logs.transaction',$this->data);
	}
public function auth_logs(PageRequest $request){
		$this->data['page_title'] = "Logs";

		$this->data['auth_logs'] = DB::table('authentication_log')
		->join('user','user.id','=','authentication_log.authenticatable_id')
		->get();
		return view('portal.logs.authenticate',$this->data);
	}

	
	

}
