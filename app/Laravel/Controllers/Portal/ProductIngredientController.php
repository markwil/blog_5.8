<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\TransactionInDetail;
use App\Laravel\Models\Ingredient;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;



use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel,Auth;

class ProductIngredientController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){

		parent::__construct();


		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['category'] = ProductCategory::all();
		$this->data['ingredients'] = RawMaterial::all();

	}

	public function index(PageRequest $request,$id=NULL){
		
		$this->data['page_title'] = "Product Ingredient";
		$this->data['ingredients'] = Ingredient::where('product_code',$id)->get();
		
		return view('portal.product-ingredient.index',$this->data);
	}

	public function create(){

		$this->data['page_title'] = "Add Product";
		$this->data['raw_material'] = TransactionInDetail::all();

		return view('portal.product.create',$this->data);
	}

	public function store(ProductRequest $request){
		
			 // dd($request->all());
		
		// return str_slug($request->ingredients,'-');
	

		try{
			
			DB::beginTransaction();
				$x = $request->only('ingredients');
			foreach ($x as $data) {
				for ($i=0; $i <  count($data) ;$i++) { 
					$ingridient = new Ingredient;
					$ingridient->product_code = $request->product_code;
					$ingridient->description = $request->ingredients[$i];
					$ingridient->qty = $request->qty[$i];
					$ingridient->measurement = $request->measurement;
					$ingridient->size = $request->size;
					$ingridient->category = $request->size;
					$ingridient->save();
				}
				
			}

				$product = new Product;

				$product->branch_id = auth()->user()->branch_id;
				$product->product_code = $request->product_code;
				$product->price = $request->price;
				$product->description = $request->name;
				$product->save();
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Added Successfully");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['data'] = Product::find($id);

		if(!$this->data['data']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.partner.index');
		}
		return view('portal.product.edit',$this->data);
	}

	public function update(PageRequest $request){
	
	 $ingredient = Ingredient::find($request->id);

		if(!$ingredient){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.raw-material.index');
		}
		try{
		
			$ingredient->qty = $request->qty;
			$ingredient->save();
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Ingredient has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}
	
public function display()
{

	$this->data['products']=  Product::all();
	$this->data['page_title']= "show";
	return view('portal.product.show',$this->data);
}

public function destroy($id=NULL)
{

$data = Product::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.product.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}	
	

	

		
	
}