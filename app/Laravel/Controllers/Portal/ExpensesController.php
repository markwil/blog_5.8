<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\Expense;
use App\Laravel\Models\AccountTitle;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\ExpensesRequest;
use Illuminate\Http\Request;
// use Illuminate\Support\Str;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Auth,Session;

class ExpensesController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
	}


	public function index()
	{
		$this->data['page_title'] = "Expenses";
		$this->data['expenses'] = Expense::paginate('10');
		return view('portal.expenses.index',$this->data);
	}

	public function create()
	{
			$this->data['page_title'] = "Expenses";
			$this->data['accounts'] = AccountTitle::all();
			return view('portal.expenses.create',$this->data);
	}
	public function store(ExpensesRequest $request){
			// return $request->all();
		try{
			DB::beginTransaction();
			
			$expenses = new Expense;
			$expenses->title  = $request->title;
			$expenses->transaction_date  = $request->transaction_date;
			$expenses->supplier  = $request->supplier;
			$expenses->tin  = $request->tin;
			$expenses->address  = $request->address;
			$expenses->description  = $request->description;
			$expenses->convertion  = $request->convertion;
			$expenses->uom  = $request->uom;
			$expenses->qty  = $request->qty;
			$expenses->remarks  = $request->remarks;
			$expenses->line_amount  = $request->line_amount;
			$expenses->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.expenses.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}


	}
	public function Edit($id=NULL){
		$this->data['expense'] =  Expense::find($id);
		$this->data['accounts'] = AccountTitle::all();	
		return view('portal.expenses.edit',$this->data);
	}
	public function update(ExpensesRequest $request,$id=NULL){
		$data =  Expense::find($id);
		if(!$data)
		{
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"No Data found.".$e->getMessage());
			return redirect()->back();
		}

		try {
			DB::beginTransaction();
		$data->title = $request->title;
		$data->transaction_date = $request->transaction_date;
		$data->supplier = $request->supplier;
		$data->tin = $request->tin;
		$data->description = $request->description;
		$data->convertion = $request->convertion;
		$data->remarks = $request->remarks;
		$data->qty = $request->qty;
		$data->address = $request->address;
		$data->line_amount = $request->line_amount;
		$data->save();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Data has been modified.");
		DB::commit();
		return redirect()->back();
		} catch (Exception $e) {
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Update Failed ,Please try again.".$e->getMessage());
			return redirect()->back();
		}
		
	}
public function destroy($id=NULL){
		$data =  Expense::find($id);
		if(!$data)
		{
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"No Data found.".$e->getMessage());
			return redirect()->back();
		}

		try {
			DB::beginTransaction();
			$data->delete();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Data has been been Deleted.");
		DB::commit();
		return redirect()->back();
		} catch (Exception $e) {
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Update Failed ,Please try again.".$e->getMessage());
			return redirect()->back();
		}
		
	}



















}
