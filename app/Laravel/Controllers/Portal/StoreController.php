<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\Branch;
use App\Laravel\Models\RawMaterialRequest;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\StockInRequest;
use App\Laravel\Requests\Portal\StockOutRequest;
use App\Laravel\Requests\Portal\BranchRequest;






use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel,Auth;

class StoreController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){

		parent::__construct();


		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['category'] = ProductCategory::all();
		$this->data['ingredients'] = RawMaterial::all();
		$this->data['supplier_count'] = Supplier::count();
		$this->data['products'] = Product::all();
		$this->data['supplier'] = [''=>"--Choose Supplier--"]+Supplier::all()->pluck('name','name')->toArray();

	}

	public function index(PageRequest $request,$id=NULL){

		$this->data['page_title'] = "Stocks";
	
		// $this->data['stocks'] =  DB::table('raw_material_stocks')->where('branch_id',request()->segment(2))->get();
		 $this->data['stocks'] = DB::table('transaction_in_details')
		  ->select(DB::raw('*,sum( measurement_qty * qty ) as total_qty,
		  	(select sum(qty) from transaction_product_ingredients where description = transaction_in_details.product_code ) as total_out'))
		  	->where('branch_id',$id)
			->groupBy('product_code')
		  	->get();

		return view('portal.store.index',$this->data);
	}

	public function create($id=NULL){

		$this->data['page_title'] = "Deliver";
		$this->data['stocks'] = StockIn::all();
		$this->data['data'] = Branch::find($id);
		return view('portal.deliver.create',$this->data);
	}

	public function store(BranchRequest $request,$id=NULL){
		// dd($request->all());
		try{
			$product = collect($request->product)->combine(collect($request->quantity));
			$data = new StockOut;
			$data->branch = $request->branch;
			$data->product = $product;
			$data->save();


			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Stock Added Successfully");

			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['data'] = Branch::find($id);

		if(!$this->data['data']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.partner.index');
		}
		return view('portal.branch.edit',$this->data);
	}

	public function update(BranchRequest $request, $id = NULL){
		$product = Branch::find($id);

		if(!$product){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.branch.index');
		}
		try{

			$data = DB::Table('branches')
						->where('id',$id)
						->update([
								 "branch_name" => $request->branch_name,
								 "branch_contact" => $request->branch_location,
								 "branch_location" => $request->branch_location,
								]);

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Branch Inforamtion has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

public function display()
{

	$this->data['products']=  Product::all();
	$this->data['page_title']= "show";
	return view('portal.product.show',$this->data);
}

public function destroy($id=NULL)
{

$data = Product::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.product.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}






}
