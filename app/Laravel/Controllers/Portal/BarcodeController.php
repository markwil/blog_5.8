<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Barcode;
use App\Laravel\Models\Partner;
use App\Laravel\Models\Product;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\BarcodeRequest;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,PDF,Auth;

class BarcodeController extends Controller{

	protected $data;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		// $this->data['partners'] = ['' => "--Choose Partner--"]+Partner::pluck('name','id')->toArray();

		 
		// $this->data['product'] = ['' => "--Choose Product--"]+Product::pluck('name','name')->toArray();
	}
	
	public function index(PageRequest $request){
		$per_page = $request->get('per_page',10);
		$guard = session()->get('is_admin','no') == "yes" ? 'admin' : 'partner';
		$user = Auth::guard($guard)->user();
		$this->data['user'] = $user;
		$this->data['barcodes'] = Barcode::where(function($query) use($user){
											if(session()->get('is_admin','no') == "no"){
												return $query->where('partner_id',$user->id);
											}
										})
										->orderBy('created_at',"DESC")->paginate($per_page);
		 $this->data['count'] = Barcode::count();
		return view('portal.barcode.index',$this->data);
	}

	public function create(PageRequest $request){

		
		return view('portal.barcode.create',$this->data);
	}


	public function store(BarcodeRequest $request){

		// dd($request->all());

			$bcodes = Barcode::count();
			if($bcodes > 0)
			{
				Barcode::truncate();
			}
			$item = $request->only('code');
			foreach ($item as $data) {
				// return count($data);
				
				for ($i=0; $i < count($data) ; $i++) { 
				$barcode = new Barcode;
				$barcode->code =  $request->code[$i];
				$barcode->price = $request->price[$i];
				$barcode->product_name = $request->description[$i];
				$barcode->qty = 3;
				$barcode->save();
				}
			}
		try{
			DB::beginTransaction();
		
			if(session()->get('is_admin','no') == "yes") $barcode->partner_id = $request->get('partner_id');
			else $barcode->partner_id = Auth::guard('partner')->user()->id;
		

			// $barcode->code = Str::upper($request->get('code'));
			// $barcode->product_name = Str::upper($request->get('description'));
			// $barcode->qty = $request->get('qty');
			// $barcode->product_category = $request->get('product_category');
			// if($barcode->qty%3 != 0){
			// 	$barcode->qty = $barcode->qty + (3-($barcode->qty%3)); 
			// }
			// $barcode->price = $request->get('price');
			// $barcode->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New set of barcode has been created.");
			return redirect()->route('portal.barcode.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"No product found!.");
			return redirect()->back();
		}

	}

	public function edit($barcode_id = NULL){
		$guard = session()->get('is_admin','no') == "yes" ? 'admin' : 'partner';
		$user = Auth::guard($guard)->user();

		$this->data['barcode'] = Barcode::where(function($query) use($user){
									if(session()->get('is_admin','no') == "no"){
										return $query->where('partner_id',$user->id);
									}
								})->find($barcode_id);

		if(!$this->data['barcode']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Barcode record not found.");
			return redirect()->route('portal.barcode.index');
		}
		return view('portal.barcode.edit',$this->data);
	}

	public function update(BarcodeRequest $request, $barcode_id = NULL){
		$guard = session()->get('is_admin','no') == "yes" ? 'admin' : 'partner';
		$user = Auth::guard($guard)->user();
		$barcode = Barcode::where(function($query) use($user){
						if(session()->get('is_admin','no') == "no"){
							return $query->where('partner_id',$user->id);
						}
					})->find($barcode_id);

		if(!$barcode){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Barcode record not found.");
			return redirect()->route('portal.barcode.index');
		}
		try{
			DB::beginTransaction();
			if(session()->get('is_admin','no') == "yes") $barcode->partner_id = $request->get('partner_id');
			
			$barcode->code = Str::upper($request->get('code'));
			$barcode->product_name = Str::upper($request->get('name'));
			$barcode->qty = $request->get('qty');
			$barcode->product_category = $request->get('product_category');
			if($barcode->qty%3 != 0){
				$barcode->qty = $barcode->qty + (3-($barcode->qty%3)); 
			}
			$barcode->price = $request->get('price');
			$barcode->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Barcode record has been modified.");
			return redirect()->route('portal.barcode.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function download(PageRequest $request,$barcode_id = NULL){
		$guard = session()->get('is_admin','no') == "yes" ? 'admin' : 'partner';
		$user = Auth::guard($guard)->user();

		$barcode = Barcode::where(function($query) use($user){
						if(session()->get('is_admin','no') == "no"){
							return $query->where('partner_id',$user->id);
						}
					})->find($barcode_id);

		if(!$barcode){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Barcode record not found.");
			return redirect()->route('portal.barcode.index');
		}
		
		$this->data['barcode'] = $barcode;
		$pdf = PDF::loadView('pdf.barcode', $this->data);
		return $pdf->download("{$barcode->code}_BARCODE.pdf");
		// Barcode::truncate();
		// return view('portal.barcode.import',$this->data);
	}

	public function download_all(PageRequest $request,$barcode_id = NULL){
		$guard = session()->get('is_admin','no') == "yes" ? 'admin' : 'partner';
		$user = Auth::guard($guard)->user();

		// $barcode = Barcode::where(function($query) use($user){
		// 				if(session()->get('is_admin','no') == "no"){
		// 					return $query->where('partner_id',$user->id);
		// 				}
		// 			})->find($barcode_id);
		$barcode= Barcode::all();
		$this->data['count']= Barcode::all()->count();


		// if(!$barcode){
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Barcode record not found.");
		// 	return redirect()->route('portal.barcode.index');
		// }
		
		$this->data['barcodes'] = $barcode;
		$pdf = PDF::loadView('pdf.barcode_all', $this->data);
		// Barcode::truncate();
		return $pdf->download("{$barcode[0]}_BARCODE.pdf");
		// Barcode::truncate();
		// return view('portal.barcode.import',$this->data);
	}
}


