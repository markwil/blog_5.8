<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\ProductCategory;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;
use App\Laravel\Requests\Portal\ProductCategoryRequest;



use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class ProductCategoryController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
 
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];

	}
	
	public function index(){

		$this->data['categories'] = ProductCategory::all();
		return view('portal.product-category.index',$this->data);
	}

	public function create($id=NULL){
		$this->data['page_title'] = "Product Category";
		return view('portal.product-category.create',$this->data);
	}

	public function store(ProductCategoryRequest $request,$id=NULL){
		
    // dd($request->all());
		try{
			DB::beginTransaction();
			
			$pcat = new ProductCategory;
			$pcat->fill($request->all());

			$pcat->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Category has been created.");
			return redirect()->route('portal.product_category.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['categories'] = ProductCategory::find($id);

		if(!$this->data['categories']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.categories.index');
		}
		return view('portal.product-category.edit',$this->data);
	}

	public function update(ProductCategoryRequest $request, $id = NULL){
		$item = ProductCategory::find($id);

		if(!$item){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Category name not found.");
			return redirect()->back();
		}
		try{
			DB::beginTransaction();
			$item->category_name = $request->category_name;
			$item->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"product Category has been modified.");
			return redirect()->route('portal.product_category.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$item = ProductCategory::find($id);

		if(!$item){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Product category not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$item->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product category  has been deleted.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

public function add_to_cart(AddToCartRequest $request,$id=NULL)
{
			try{
			DB::beginTransaction();
			$cart = new CartOrder;
			$cart->fill($request->all());
			$cart->save();
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Has been added to Order List.");
			return redirect()->route('portal.purchase-order-supplier.create',[Request::segment(3)]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

}

public function show(Request $request,$id=NULL){
	$this->data['supplier_name'] = Supplier::where('id',$id)->first();
	$this->data['suppliers'] = CartOrder::where('supplier_id',$id)->get();
	 $this->data['count'] = CartOrder::where('supplier_id',$id)->count();
	return view('portal.order_list.show',$this->data);
	}

	public function stock_in(StockInRequest $request){

       // return $price = $request->price; 
		try{
			
			DB::beginTransaction();
			$total = 0;
			$item_code = $request->only('item_code'); 
			foreach ($item_code as $code) {
			for ($i=0; $i < count($code); $i++) { 
				$data = new StockIn;
				$data->product_name = $code[$i];
				$data->supplier_price = $request->price[$i];
				$data->product_qty = $request->qty[$i];
				$data->marked_up += $request->price[$i] * $request->qty[$i];
				$data->save();
			}
		}
		DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.order_list.show',[1]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

	public function order_details($id=NULL)
	{
		$this->data['cart'] = DB::Table('cart_orders as orders')
		->select(DB::raw('SUM(DISTINCT orders.total_amount) as total_amount,detail.description,detail.price,SUM(DISTINCT detail.qty) as qty,detail.product_name,detail.id as pid'))
		->join('cart_order_details as  detail','orders.product_id','=','detail.product_id')
		->where('detail.supplier_id',$id)
		->groupBy('detail.description')
		->orderBy('orders.created_at','desc')
		->get();

		return view('portal.order-list-details.index',$this->data);
	}


	
}