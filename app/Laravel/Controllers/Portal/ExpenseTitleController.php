<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\Expense;
use App\Laravel\Models\Order;
use App\Laravel\Models\AccountTitle;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\ExpensesRequest;
use Illuminate\Http\Request;
// use Illuminate\Support\Str;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Auth,Session;

class ExpensetitleController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
	
	}


	public function index()
	{
		$this->data['page_title'] = "Expenses";
		$this->data['account_title'] = AccountTitle::paginate('10');
		return view('portal.expense-title.index',$this->data);
	}

	public function store(ExpensesRequest $request){
			// return $request->all();
		try{
			DB::beginTransaction();
			
			$expenses = new AccountTitle;
			$expenses->account_title  = $request->account_title;
			$expenses->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Account Title has been created.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Something went wrong ".$e->getMessage());
			return redirect()->back();
		}


	}
	public function Edit($id=NULL){
		$this->data['expense'] =  Expense::find($id);	
		return view('portal.expenses.edit',$this->data);
	}


	public function update(PageRequest $request){
		$data =  AccountTitle::find($request->id);
		if(!$data)
		{
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"No Data found.".$e->getMessage());
			return redirect()->back();
		}

		try {
		DB::beginTransaction();
		$data->account_title = $request->account_title;
		$data->save();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Data has been modified.");
		DB::commit();
		return redirect()->back();
		} catch (Exception $e) {
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Update Failed ,Please try again.".$e->getMessage());
			return redirect()->back();
		}
		
	}
public function destroy($id=NULL){
		$data =  AccountTitle::find($id);
		if(!$data)
		{
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"No Data found.".$e->getMessage());
			return redirect()->back();
		}

		try {
			DB::beginTransaction();
			$data->delete();
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Data has been been Deleted.");
		DB::commit();
		return redirect()->back();
		} catch (Exception $e) {
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Update Failed ,Please try again.".$e->getMessage());
			return redirect()->back();
		}
		
	}



















}
