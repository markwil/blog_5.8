<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request,PDF;

class OrderListController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
 
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];

	}
	
	public function index(){
      	
      	if(auth()->user()->type == "super_user")
      	{
			$this->data['cart'] = CartOrder::groupBy('supplier_id')->where('supplier_id','<>',1)->get();
			return view('portal.order-list.index',$this->data);
      	} 
		$this->data['cart'] = CartOrder::groupBy('supplier_id')->get();
		return view('portal.order-list.index',$this->data);
	}

	public function create($id=NULL){
		$this->data['products'] = SupplierProduct::where('supplier_id',$id)->paginate('5');
		return view('portal.purchase-order-supplier.create',$this->data);
	}

	public function store(SupplierProductRequest $request,$id=NULL){
		
  // dd($request->all());
		try{
			DB::beginTransaction();
			
			$supplier = new SupplierProduct;
			$supplier->fill($request->all());

			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.supplier-product.create',[Request::segment(3)]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['supplier'] = Supplier::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}
	public function remove_product($id = NULL,$cart_id){
		$order = CartOrderDetail::find($id);

		if(!$order){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->back();
		}
		
		if($order->delete())
		{
			$orders = CartOrderDetail::where('cart_id',$cart_id)->count();
			if(!$orders > 0)
			{
			 CartOrder::where('cart_id',$cart_id)->delete();
			 session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Item has been removed!");
			 return redirect()->route('portal.order-list-branch.index');
			}
			else
			{
				session()->flash('notification-status',"success");
			    session()->flash('notification-msg',"Item has been removed!");
				return redirect()->back();
			}
		}
		
	}

	public function update(SupplierRequest $request, $id = NULL){
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->name = $request->name;
			$supplier->address = $request->address;
			$supplier->contact = $request->contact;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}



public function show(Request $request,$id=NULL){
	$this->data['supplier_name'] = Supplier::where('id',$id)->first();
	$this->data['suppliers'] = CartOrder::where('supplier_id',$id)->get();
	 $this->data['count'] = CartOrder::where('supplier_id',$id)->count();
	return view('portal.order_list.show',$this->data);
	}

	public function stock_in(StockInRequest $request){

       // return $price = $request->price; 
		try{
			
			DB::beginTransaction();
			$total = 0;
			$item_code = $request->only('item_code'); 
			foreach ($item_code as $code) {
			for ($i=0; $i < count($code); $i++) { 
				$data = new StockIn;
				$data->product_code = $code[$i];
				$data->supplier_price = $request->price[$i];
				$data->product_qty = $request->qty[$i];
				$data->marked_up += $request->price[$i] * $request->qty[$i];
				$data->save();
			}
		}
		DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.order_list.show',[1]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

	public function order_details($id=NULL,$supplier_id) //Display order Details
	{
		$this->data['page_title'] = "Order List";
	if(auth()->user()->type == "super_user")
	{
		$this->data['cart_details'] = DB::table('cart_order_details')
		->select(DB::raw('*,(select status from cart_orders where cart_id = cart_order_details.cart_id) as order_status'))
		->where('supplier_id','<>',1)
		->get();
		$this->data['order_header'] = CartOrder::where('cart_id',$id)->first();
		return view('portal.order-list-details.index',$this->data);
	}
	 $this->data['cart_details'] = DB::table('cart_order_details')
		->select(DB::raw('*,(select status from cart_orders where cart_id = cart_order_details.cart_id) as order_status'))
		->where('branch_id',auth()->user()->branch_id)
		->where('cart_id',auth()->user()->branch_id)
		->where('supplier_id',$supplier_id)
		->get();

	$this->data['order_header'] = CartOrder::where('cart_id',$id)->first();
			

		return view('portal.order-list-details.index',$this->data);
	}
	// public function order_details_branch($id=NULL,$sid) //Display order Details
	// {
	// 	$this->data['page_title'] = "Order List";
	
	// 	 $this->data['cart_details'] = DB::table('cart_order_details')
	// 	->select(DB::raw('*,(select status from cart_orders where cart_id = cart_order_details.cart_id) as order_status'))
	// 	->where('branch_id',auth()->user()->branch_id)
	// 	->where('supplier_id',$supplier_id)
	// 	->get();

	// $this->data['order_header'] = CartOrder::where('cart_id',$id)->first();
			

	// 	return view('portal.order-list-details.index',$this->data);
	// }


	public function recieve($id=NULL)
	{
		$this->data['page_title'] = "Order List";

		$this->data['orders'] = CartOrderDetail::where('cart_id',$id)
		->get();

		$this->data['order_header'] = CartOrder::where('cart_id','=',$id)
		// ->where('supplier_id',$id)
		->first();
	
		return view('portal.order-list-details.recieve',$this->data);
	}

	public function print_purchase_oder($id=NULL)
		{
		$this->data['order_header'] = CartOrder::find($id);
		$this->data['items'] = CartOrderDetail::where('cart_id',$id)
		->get();

			$pdf = PDF::loadView('pdf.purchaseReport',$this->data)->setPaper('letter', 'portrait');
			return $pdf->download('purchaseReport.pdf');
		}


	
}