<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\TransactionInDetail;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\StockInRequest;




use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel,Auth;

class StockInController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['category'] = ProductCategory::all();
		$this->data['ingredients'] = RawMaterial::all();
		$this->data['supplier_count'] = Supplier::count();
		$this->data['supplier'] = [''=>"--Choose Supplier--"]+Supplier::all()->pluck('name','name')->toArray();

	}

	public function index(PageRequest $request){
	 // dd(StockIn::all());
		$this->data['page_title'] = "Stocks";
		
		 $this->data['stocks'] =  DB::table('stock_in')
		->select(DB::raw('*,sum(qty) as total_qty,(select sum(qty) from stock_out where product_code = stock_in.product_code) as out_qty'))
		->groupBy('product_code')
		->get();
		auth()->user()->notifications->markAsRead();
		return view('portal.stock.index',$this->data);
	}

	public function create(){

		$this->data['page_title'] = "Add Stock";
		return view('portal.stock.create',$this->data);
	}

	public function store(StockInRequest $request){

		 // return $request->all();	
		try{

	 		DB::beginTransaction();
			$stock = new StockIn;

			$stock->product_supplier = $request->supplier_name;
			$stock->product_code = $request->product_code;
			// $stock->product_name = $request->product_name;
			$stock->product_qty = $request->product_qty;
			$stock->product_unit = $request->product_unit;
			$stock->supplier_price = $request->supplier_price;
			$stock->marked_up = $request->marked_up /$request->supplier_price;


			if($stock->save())
			{
				return $request->cart_id/* CartOrders::where('cart_id','')*/;
			}
			DB::commit();
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Stock Added Successfully");

			return redirect()->route('portal.stock.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['data'] = StockIn::find($id);
		if(!$this->data['data']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.stock.index');
		}
		return view('portal.stock.edit',$this->data);
	}

	public function update(StockInRequest $request, $id = NULL){
		$product = StockIn::find($id);

		if(!$product){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.raw-material.index');
		}
		try{

			$data = DB::Table('stock_in')
						->where('id',$id)
						->update([
								 "product_supplier" => $request->product_supplier,
								 "product_name" => $request->product_name,
								 "product_qty" => $request->product_qty,
								 "product_code" => $request->product_code,
								 "product_unit" => $request->product_unit,
								]);

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

public function display()
{

	$this->data['products']=  Product::all();
	$this->data['page_title']= "show";
	return view('portal.product.show',$this->data);
}

public function destroy($id=NULL)
{

$data = Product::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.product.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}


public function update_marked_up(Request $request)
{
	try {
		DB::beginTransaction();
		$marked_up = StockIn::where('product_code',$request->product_code)
		->update(['marked_up'=> $request->marked_up ]);
		db::commit();

		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Marked-up Price Update Successfully! ");
		return redirect()->back();
	} catch (Exception $e) {
		DB::rollBack();
		session()->flash('notification-status',"failed");
		session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
		return redirect()->back();
	}
	
}






}
