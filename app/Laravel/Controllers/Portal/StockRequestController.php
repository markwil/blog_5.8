<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\RawMaterialRequest;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\Branch;
use App\Laravel\Models\ApprovedOrder;
use App\Laravel\Models\ApprovedOrderDetail;
use PhpUnitConversion\Unit\Mass;
/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
// use App\Laravel\Requests\Portal\RawMaterialRequest;
use App\Laravel\Requests\Portal\RawMaterialRequestRequest;

use App\Notifications\ApprovedOrderNotifaction;
// use Illuminate\Support\Str;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request,Auth,Conversion,Form;

class StockRequestController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['raw'] = StockIn::all();
		$this->data['pending_requests'] = RawMaterialRequest::all()->groupBy('order_id')->count();
	}

	public function index(PageRequest $request){

		$this->data['page_title'] = "Product";
		$this->data['page_title'] = "Deliver";

		$this->data['orders'] = DB::table('cart_orders')
		->select(DB::raw('*,(select branch_location from branches where id = cart_orders.cart_id ) as branch'))
		->where('status','<>',0)
		->where('cart_id','<>',auth()->user()->branch_id)
		->where('supplier_id','<>',2)
		->get();
		auth()->user()->notifications->markAsRead();
		return view('portal.stock-request.index',$this->data);
	}

	public function create(PageRequest $request){
      	
		$this->data['page_title'] = "Add Product";

		$this->data['product'] = StockIn::all();
		return view('portal.raw-material-request.create',$this->data);
	}

	public function store(RawMaterialRequestRequest $request){
		
		$order_id = new RawMaterialRequest;
		$lastInvoiceID = $order_id->orderBy('id', 'DESC')->pluck('id')->first();
		$newInvoiceID = $lastInvoiceID + 1;

			 
		try{
			// return $request->product_unit;
			$raw_material_req =  collect($request->product_code)->combine(collect($request->product_qty))->toArray();
			
			// foreach ($raw_material_req as $key => $value) {

				$raw = new RawMaterialRequest;
				$raw->branch_id = Auth::user()->branch_id;
				$raw->order_id = $newInvoiceID;
				$raw->product_code = $request->product_code;
				$raw->product_qty = $request->product_qty;
				// $raw->product_unit = $request->product_unit;
				// $raw->product_unit = $request->product_unit;
				$raw->save();

			// }


		
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Added Successfully");
			return redirect()->route('portal.raw-material-request.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		// return Conversion::convert(1,'kilogram')->to('gram');
		$this->data['status'] = RawMaterialRequest::where('order_id',$id)->first();
		$this->data['store_requests'] = RawMaterialRequest::where('order_id',$id)->get();

		if(!$this->data['store_requests']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.stock-request.index');
		}

		return view('portal.stock-request.edit',$this->data);
	}

	public function update(PageRequest $request){
		// return  $request->cart_id;
		
		try{

			$approve = DB::table('cart_orders')
			->where('cart_id',$request->cart_id)
			->update(['status'=>'1']);
			if(!$approve){
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Account not found.");
				return redirect()->route('portal.stock-request.index');
			}
			$approves = DB::table('cart_order_details')
			->where('cart_id',$request->cart_id)
			->update(['status'=>'1']);
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Order  has been Approved.");
			return redirect()->route('portal.deliver.index');
			

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}


public function destroy($id=NULL)
{

$data = RawMaterial::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.raw-material.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}

public function details($id=NULL) //Display order Details
	{
		// return  $this->data['cart'] = DB::table('get_cartDetails')->where('branch_id',$id)->where('status',0)->get	
	$this->data['page_title'] = "Order Details";	
	 $this->data['cart'] = DB::table('cart_order_details')->select(DB::raw('*,SUM(qty) as qty'))
		->where('branch_id',$id)
		->where('status',0)
		->groupBy('product_code')->get();
		return view('portal.stock-request.details',$this->data);
	}
public function approve_order($id=NULL)
{
		// return  	$user = User::find($id);
       $order_items = CartOrder::where('cart_id',$id )->get()->toArray();
        $order_id = new CartOrder;
		$lastInvoiceID = $order_id->orderBy('id', 'DESC')->pluck('id')->first();
		$newInvoiceID = $lastInvoiceID."000".auth()->user()->id + 100;
        foreach ($order_items as $item) 
        {
            $items = new ApprovedOrder;
            $items->cart_id =  $newInvoiceID;
            $items->supplier_id =  $item['supplier_id'];
            $items->branch_id =  $item['cart_id'];
            $items->product_qty =  $item['product_qty'];
            $items->total_amount =  $item['total_amount'];	
            $items->status =  1;
           
           if( $items->save())
           {
           	$order_detail = CartOrderDetail::where('cart_id',$id )->update(['status'=>2,'cart_id'=> $newInvoiceID]);
           	if(	$order_detail)
           	{
           	CartOrder::where('cart_id',$id)->delete();
         	$user = User::find($id);
         	$user->notify(new ApprovedOrderNotifaction);
           	session()->flash('notification-status',"success");
           	session()->flash('notification-msg',"Order has been approved!.");
           	return redirect()->route('portal.deliver.index');
           	}

       
           }


        }

     
	
}

   public function print_purchase_oder($id=NULL)
		{
		return $this->data['order_header'] = CartOrder::find($id);
		$this->data['items'] = CartOrderDetail::where('cart_id',$id)
		->where('supplier_id',$sid)
		->get();

			$pdf = PDF::loadView('pdf.purchaseReport',$this->data)->setPaper('letter', 'portrait');
			return $pdf->download('purchaseReport.pdf');
		}



}
