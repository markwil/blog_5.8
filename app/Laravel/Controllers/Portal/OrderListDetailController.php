<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\StockInHeader;
use App\Laravel\Models\Branch;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;
use App\Laravel\Requests\Portal\RecieveOrderRequest;

use Haruncpi\LaravelIdGenerator\IdGenerator;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request,PDF,Auth;

class OrderListDetailController extends Controller{


	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		$this->data = [];
		array_merge($this->data, parent::get_data());
 
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['page_title'] = "ORDER LIST";
	}
	
	public function index(){
		
 // return "a";
	$this->data['orders'] = DB::table('suppliers')
		->select(DB::raw('Suppliers.name,suppliers.id,detail.product_name,SUM(DISTINCT detail.qty) AS qty,detail.price,SUM(orders.total_amount) AS total_amount,detail.created_at,COUNT(detail.product_name) as total_items'))
						->join('cart_order_details as detail','suppliers.id','=','detail.supplier_id')
						->join('cart_orders as orders','orders.product_id','=','detail.product_id')
						->groupBy('detail.supplier_id')
						->get();
		return view('portal.order-list.index',$this->data);
	}

	public function create($id=NULL){
		$this->data['products'] = SupplierProduct::where('supplier_id',$id)->paginate('5');
		return view('portal.purchase-order-supplier.create',$this->data);
	}

	public function store(PageRequest $request,$id=NULL){
		  $bill_due =  Carbon::createFromFormat('Y-m-d',$request->date)->addDays($request->terms)->format('Y-m-d');

  		DB::beginTransaction();
				$StockInHeader = new StockInHeader;

				
				$config=['table'=>'stock_in_headers','length'=>10,'prefix'=>'ZIITEA-'];
				$transaction_id = IdGenerator::generate($config);
				$StockInHeader->id = $transaction_id ;
				$StockInHeader->transaction_id = $transaction_id ;
				$StockInHeader->amount = $request->due;
				$StockInHeader->terms = 0;
				$StockInHeader->bill_due = $bill_due;
				$StockInHeader->discount = $request->discount;
				
				$StockInHeader->save();
				$name = $request->only('product_code'); 
				foreach ($name as $data) {
				for ($i=0; $i < count($data) ; $i++) { 
				$stock = new StockIn;
				$stock->transaction_id = $transaction_id;
				$stock->transaction_type =  $request->transaction_type;
				$stock->supplier = $request->supplier_idd[$i];
				$stock->qty = $request->qty[$i];
				$stock->measurement = $request->measurement[$i];
				$stock->product_code = $request->product_code[$i];
				$stock->description = $request->description[$i];
				$stock->measurement_qty = $request->measurement_qty[$i];
				$stock->price = $request->price[$i];
				$stock->save();
				
				}

				}
			
					CartOrder::where('cart_id',$request->cart_id)
					->where('supplier_id',$request->supplier_iddd)
					->delete();
					CartOrderDetail::where('cart_id',$request->cart_id)->where('supplier_id',$request->supplier_iddd)->delete();
					DB::commit();
				
			     session()->flash('notification-status',"success");
				session()->flash('notification-msg',"Transaction Completed.");
				return redirect()->route('portal.quick_report.index');
				
			}

				

	public function edit($id = NULL){
		$this->data['product'] = CartOrderDetail::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}

	public function update(Request $request, $id = NULL){
		  	
		
		$item = CartOrderDetail::find($request::input('id'));

		if(!$item){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"item not found.");
			return redirect()->route('portal.order_list_details.index');
		}
		try{
			DB::beginTransaction();
			$item->qty = $request::input('qty');
			if($item->save())
			{
			$item->total_amount =  $item->qty  * $item->price;
			$item->save();
			}
			DB::commit();
	
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Product Quantity has been modified.");
			// return redirect()->route('portal.order-list-details.index',[$request::segment(2)]);
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}
		public function print_purchase_oder($id=NULL,$sid)
		{
		$this->data['order_header'] = CartOrder::find($id);
		$this->data['items'] = CartOrderDetail::where('cart_id',$id)
		->where('supplier_id',$sid)
		->get();

			$pdf = PDF::loadView('pdf.purchaseReport',$this->data)->setPaper('letter', 'portrait');
			return $pdf->download('purchaseReport.pdf');
		}
	public function destroy($id=NULL)
	{
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}



public function show(Request $request,$id=NULL){

	$this->data['supplier_name'] = Supplier::where('id',$id)->first();
	$this->data['suppliers'] = CartOrder::where('supplier_id',$id)->get();
	 $this->data['count'] = CartOrder::where('supplier_id',$id)->count();
	return view('portal.order_list.show',$this->data);
	}

	public function recieve($id=NULL)
	{
		$this->data['page_title'] = "Order List";

		$this->data['orders'] = CartOrderDetail::where('cart_id','=',auth()->user()->branch_id)
		->where('cart_id',$id)
		->get();

		$this->data['order_header'] = CartOrder::where('cart_id','=',auth()->user()->branch_id)
		// ->where('supplier_id',$id)
		->first();
	
		return view('portal.order-list-details.recieve',$this->data);
	}
	
	}

	

	
