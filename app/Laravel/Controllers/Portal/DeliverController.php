<?php

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\User;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\RawMaterial;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\Branch;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\ApprovedOrderDetail;


/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\ProductRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\PartnerImportRequest;
use App\Laravel\Requests\Portal\StockInRequest;
use App\Laravel\Requests\Portal\StockOutRequest;
use App\Laravel\Requests\Portal\BranchRequest;
use App\Laravel\Requests\Portal\DeliverRequest;


use App\Notifications\Shipped;
use App\Notifications\StockRequestNotification;

use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Http\Request;
use Str,Carbon,Helper,DB,Excel,Auth,PDF;

class DeliverController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){

		parent::__construct();


		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['category'] = ProductCategory::all();
		$this->data['ingredients'] = RawMaterial::all();
		$this->data['supplier_count'] = Supplier::count();
		$this->data['products'] = Product::all();
		$this->data['supplier'] = [''=>"--Choose Supplier--"]+Supplier::all()->pluck('name','name')->toArray();

	}

	public function index(PageRequest $request){

	$this->data['page_title'] = "Deliver";
	switch (auth()->user()->type) {
		case 'super_user':
		$this->data['deliver'] = DB::table('approved_order_header')
		->select(DB::raw('*,(select branch_location from branches where id = approved_order_header.branch_id) as branch'))
		->where('status','<>',3)
		->groupBy('cart_id')
		->get();
		auth()->user()->notifications->markAsRead();
		return view('portal.deliver.index',$this->data);
			break;
		
		default:
		$this->data['deliver'] = DB::table('approved_order_header')
		->select(DB::raw('*,(select branch_location from branches where id = approved_order_header.branch_id) as branch'))
		->where('status','<>',3)
		->where('status','<>',1)
		->where('branch_id',auth()->user()->branch_id)
		->groupBy('cart_id')
		->get();
		auth()->user()->notifications->markAsRead();
		return view('portal.deliver.index',$this->data);
			break;
	}
	
	}
	public function details(PageRequest $request,$id=NULL){

		$this->data['page_title'] = "Deliver";
		$this->data['cart'] = DB::table('cart_order_details')->select(DB::raw('*,SUM(qty) as qty'))
		->where('cart_id',$id)
		->groupBy('product_code')->get();
		return view('portal.deliver.details',$this->data);
	}

	public function create($id=NULL){

		$this->data['page_title'] = "Deliver";
		$this->data['stocks'] = StockIn::all();
		$this->data['data'] = Branch::find($id);
		return view('portal.deliver.create',$this->data);
	}

	public function store(DeliverRequest $request,$id=NULL){
	 // dd($request->all());
		try{
		$a =  collect($request->product)->combine(collect($request->quantity));
				foreach ($a as $key => $value)
				{
					$data = new StockOut;
					$data->raw_material_code = $key;
					$data->raw_material_qty = $value;
					$data->branch = $request->branch;
					$data->save();
				}

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Stock Added Successfully");

			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['data'] = Branch::find($id);

		if(!$this->data['data']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.partner.index');
		}
		return view('portal.branch.edit',$this->data);
	}

	public function update(BranchRequest $request, $id = NULL){
		$product = Branch::find($id);

		if(!$product){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.branch.index');
		}
		try{

			$data = DB::Table('branches')
						->where('id',$id)
						->update([
								 "branch_name" => $request->branch_name,
								 "branch_contact" => $request->branch_location,
								 "branch_location" => $request->branch_location,
								]);

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Branch Inforamtion has been modified.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

public function display()
{

	$this->data['products']=  Product::all();
	$this->data['page_title']= "show";
	return view('portal.product.show',$this->data);
}

public function destroy($id=NULL)
{

$data = Product::find($id);

if (!$data)
 {
 	session()->flash('notification-status',"failed");
 	session()->flash('notification-msg',"Account not found.");
 	return redirect()->route('portal.product.index');
 }

 if($data->delete())
 {
 	session()->flash('notification-status',"success");
	session()->flash('notification-msg',"Data has been Deleted Successfully.");
	return redirect()->back();
 }


}



public function complete_transaction($id=NULL,$bid=NULL)
{

	$this->data['page_title'] = "WAREHOUSE : COMPLETE TRANSACTION";
		try{
			DB::beginTransaction();
			$data = DB::Table('approved_order_header')
						->where('cart_id',$id)
						->update(["status" => 2]);	
			
			$user = User::find($bid);
			$user->notify(new Shipped);

			DB::commit();
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Order has been scheduled for delivery");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
}

public function print_purchase_oder($id=NULL)
{
	
$this->data['items'] = CartOrderDetail::where('cart_id',$id)
	->get();

	$pdf = PDF::loadView('pdf.purchaseReport',$this->data)->setPaper('letter', 'portrait');
	return $pdf->download('purchaseReport.pdf');
}


}
