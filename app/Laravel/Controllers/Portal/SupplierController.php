<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\Branch;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel;

class SupplierController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());

		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		
		
	}
	
	public function index(PageRequest $request){
		// $per_page = $request->get('per_page',10);
		$this->data['branch'] = Branch::where('id',auth()->user()->branch_id)->first(); 
		$this->data['suppliers'] = Supplier::where('id','<>','1')->orderBy('created_at',"DESC")->paginate(10);
		return view('portal.supplier.index',$this->data);
	}

	public function create(){
		return view('portal.supplier.create',$this->data);
	}

	public function store(SupplierRequest $request){
	
		try{
			DB::beginTransaction();
			
			$supplier = new Supplier;
			$supplier->name  = $request->name;
			$supplier->email  = $request->email;
			$supplier->contact  = $request->contact;
			$supplier->address  = $request->address;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['supplier'] = Supplier::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}

	public function update(SupplierRequest $request, $id = NULL){


		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->name = $request->name;
			$supplier->email = $request->email;
			$supplier->address = $request->address;
			$supplier->contact = $request->contact;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function update_status($id = NULL,$status=NULL){


		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->status = $status;
			$supplier->save();
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Supplier  Status has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}


	
}