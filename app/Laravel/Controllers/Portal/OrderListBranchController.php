<?php 

namespace App\Laravel\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Laravel\Models\Partner as Account;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\SupplierProduct;
use App\Laravel\Models\CartOrder;
use App\Laravel\Models\CartOrderDetail;
use App\Laravel\Models\StockIn;
use App\Laravel\Models\TransactionIn;
use App\Laravel\Models\TransactionInDetail;
use App\Laravel\Models\Branch;
use App\Laravel\Models\User;
use App\Laravel\Models\StockOut;
use App\Laravel\Models\ApprovedOrder;

use App\Notifications\AddToCart;
use App\Notifications\RecievedNotification;

/*
 * Requests used in this Controller
 */
use App\Laravel\Requests\Portal\PartnerRequest;
use App\Laravel\Requests\Portal\PartnerPasswordRequest;
use App\Laravel\Requests\Portal\SupplierRequest;
use App\Laravel\Requests\Portal\SupplierProductRequest;
use App\Laravel\Requests\Portal\AddToCartRequest;
use App\Laravel\Requests\Portal\StockInRequest;


use App\Laravel\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Request;

class OrderListBranchController extends Controller{

	protected $data,$import_process,$counter;
	public function __construct(){
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['supplier'] = Supplier::all()->pluck('name','name')->toArray();
		$this->data['suppliers'] = Supplier::all();

		$this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
		$this->data['page_title'] = "ORDER LIST";
	}
	
	public function index(){
	
	$this->data['cart'] = CartOrder::groupBy('supplier_id')
							->where('cart_id',auth()->user()->branch_id)
							->get();
		
		return view('portal.order-list-branch.index',$this->data);
	}

	public function create($id=NULL){
		$this->data['products'] = SupplierProduct::where('supplier_id',$id)->paginate('5');
		return view('portal.purchase-order-supplier.create',$this->data);
	}

	public function store(PageRequest $request,$id=NULL){

		// return $request->all();
		// return  $request->measurement[0];



		DB::beginTransaction();
			$tid =  str_pad($request->cart_id, 4, "0", STR_PAD_LEFT);
			$items = $request->description;
			$trans_in = new TransactionIn;
			$trans_in->branch_id = auth()->user()->branch_id;
			$trans_in->transaction_id = $tid;
			$trans_in->amount = $request->total_due;
			$trans_in->discount = $request->discount;
			if($trans_in->save())
			{
				$items = $request->only('description');
				foreach ($items as $item) {

					for ($i=0; $i < count($item) ; $i++) { 

						//INSERT TO TRANSACTION IN BRANCH
						$trans_in_detail = new TransactionInDetail;
						$trans_in_detail->transaction_id = str_pad($request->cart_id, 4, "0", STR_PAD_LEFT);
						$trans_in_detail->branch_id = auth()->user()->branch_id;
						$trans_in_detail->transaction_type = "bill";
						$trans_in_detail->product_code = $request->product_code[$i];
						$trans_in_detail->description = $request->description[$i];
						$trans_in_detail->supplier_id = "1";
						$trans_in_detail->price = $request->price[$i];
						$trans_in_detail->qty = $request->qty[$i];
						

						switch ($request->measurement[0]) {
							case 'L':
							$trans_in_detail->measurement_qty = 	$request->measurement_qty[$i] * 33.814;
							$trans_in_detail->measurement = "oz";
							break;
							default:
							$trans_in_detail->measurement_qty = $request->measurement_qty[$i];
							$trans_in_detail->measurement = $request->measurement[$i];
							break;
						}
						
						$trans_in_detail->bill_due = $request->date;
						$trans_in_detail->save();
						//INSERT TO STOCK OUT WAREHOUSE
						$stock_out = new StockOut;
						$stock_out->transaction_id = $tid;
						$stock_out->transaction_type = "invoice";
						$stock_out->product_code = $request->product_code[$i];
						$stock_out->description = $request->description[$i];
						$stock_out->price = $request->price[$i];
						$stock_out->qty = $request->qty[$i];
						$stock_out->measurement = $request->measurement[$i];
						$stock_out->save();


					}
				}
			}
			ApprovedOrder::where('cart_id',$request->cart_id)->delete();
			CartOrderDetail::where('cart_id',$request->cart_id)->delete();
			$user = User::find(1);
			$user->notify(new RecievedNotification);
			
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Transaction Completed.");
			return redirect()->route('portal.deliver.index');
		try{
			

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit($id = NULL){
		$this->data['supplier'] = Supplier::find($id);

		if(!$this->data['supplier']){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Account not found.");
			return redirect()->route('portal.supplier.index');
		}
		return view('portal.supplier.edit',$this->data);
	}

	public function update(SupplierRequest $request, $id = NULL){
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->name = $request->name;
			$supplier->address = $request->address;
			$supplier->contact = $request->contact;
			$supplier->save();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}

	public function destroy($id=NULL)
	{
		$supplier = supplier::find($id);

		if(!$supplier){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"supplier not found.");
			return redirect()->route('portal.supplier.index');
		}
		try{
			DB::beginTransaction();
			$supplier->delete();

			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Account has been modified.");
			return redirect()->route('portal.supplier.index');

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}



	public function show(Request $request,$id=NULL){
		$this->data['supplier_name'] = Supplier::where('id',$id)->first();
		$this->data['suppliers'] = CartOrder::where('supplier_id',$id)->get();
		$this->data['count'] = CartOrder::where('supplier_id',$id)->count();
		return view('portal.order_list.show',$this->data);
	}

	public function checked_out($id=NULL){
	
		try{

			$approve = DB::table('cart_orders')
			->where('id',$id)
			->update(['status'=>'5']);
			if(!$approve){
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Account not found.");
				return redirect()->route('portal.stock-request.index');
			}
			$approves = DB::table('cart_order_details')
			->where('cart_id',$id)
			->update(['status'=>'5']);
			$user = User::find(1);
			$user->notify(new AddToCart(User::findOrFail(auth()->user()->id)));
			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Your Order has been Checked out!");
			return redirect()->back();
			

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

	public function stock_in(StockInRequest $request){

        return $price = $request->price; 
		try{
			
			DB::beginTransaction();
			$total = 0;
			$item_code = $request->only('item_code'); 
			foreach ($item_code as $code) {
				for ($i=0; $i < count($code); $i++) { 
					$data = new StockIn;
					$data->product_code = $code[$i];
					$data->supplier_price = $request->price[$i];
					$data->product_qty = $request->qty[$i];
					$data->marked_up += $request->price[$i] * $request->qty[$i];
					$data->save();
				}
			}
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"New Supplier has been created.");
			return redirect()->route('portal.order_list.show',[1]);

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}
	}

	public function order_details($id=NULL) //Display order Details
	{

		$this->data['cart'] = DB::table('cart_order_details')
		->select(DB::raw('*,SUM(qty) as qty'))
		->where('branch_id',auth()->user()->branch_id)
		->groupBy('product_code')->paginate(10);

		$this->data['status'] = DB::table('cart_orders')->select(db::raw('status'))
		->where('cart_id','99999999'.auth()->user()->id)
		->groupBy('cart_id')
		->get();

		return view('portal.order-list-branch.details',$this->data);
	}
	public function recieve($id=NULL) //Display order Details
	{
		// ->where('cart_id',$id)->get();
		 // return $this->data['cart'] = DB::table('cart_order_details')
		// ->select(DB::raw('*,
		// 	(select total_amount from approved_order_header where cart_id = cart_order_details.cart_id) as amount,
		// 	(select status from approved_order_header where cart_id = cart_order_details.cart_id) as order_status,
		// 	 (select updated_at from approved_order_header where cart_id = cart_order_details.cart_id) as date,
		// 	(select branch_location from branches where id = cart_order_details.branch_id) as branch
		// 	'))->where('cart_id',$id)->get();

		$this->data['cart'] = DB::table('cart_order_details')
		->select(DB::raw('*'))
		->where('branch_id',auth()->user()->branch_id)
		->where('cart_id',$id)->get();

		$this->data['count'] = ApprovedOrder::where('cart_id',$id)->first();

		return view('portal.order-list-branch.recieve',$this->data);
	}

	public function cancel_order($id=NULL,$cart_id)
	{

		$header = CartOrder::find($id);
		if($header->destroy())
			{
				$detail = CartOrderDetail::find($id);
				$detail->destroy();
			}
		
		// CartOrder::where('id',$id)->update(['status'=>3]);
		// CartOrderDetail::where('cart_id',$id)->update(['status'=>3]);
		session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Your Order is Successfully Canceled");
		return redirect()->back();

	}

	public function remove_order($id=NULL)
	{
		// $item = CartOrder::find($id);
		

		$item = CartOrder::find($id);
		

		if(!$item){
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"item not found.");
			return redirect()->bacj();
		}
		try{
			DB::beginTransaction();
			CartOrderDetail::where('cart_id',$item->cart_id)->delete();
			$item->delete();
			
			DB::commit();

			session()->flash('notification-status',"success");
			session()->flash('notification-msg',"Order  has been canceled and  removed from Order list.");
			return redirect()->back();

		}catch(\Exception $e){
			DB::rollBack();
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Server Error. Please try again.".$e->getMessage());
			return redirect()->back();
		}

	}
	
}