<?php namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class VoidRequest extends ApiRequestManager{

	public function rules(){
		$rules = [
			'pin_code' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'numeric'	=> "Please indicate a valid amount.",
		];
	}
}