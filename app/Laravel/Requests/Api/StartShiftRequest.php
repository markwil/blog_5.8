<?php namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class StartShiftRequest extends ApiRequestManager{

	public function rules(){
		$rules = [
			'cash_on_float' => 'required|numeric'
		];

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'numeric'	=> "Please indicate a valid amount.",
		];
	}
}