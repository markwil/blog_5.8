<?php namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class PaymentRequest extends ApiRequestManager{

	public function rules(){
		$rules = [
			'amount_received' => "required_if:type,cash|nullable|numeric",
			'remarks' => "required_if:type,representation,account_receivable",
			'type' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required_if'	=> "Field is required.",
			'required'	=> "Field is required.",
			'numeric'	=> "Please indicate a valid amount.",
		];
	}
}