<?php namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class TransactionRequest extends ApiRequestManager{

	public function rules(){
		$rules = [
			'item.*.product_id' => 'required|exist_product',
			'item.*.qty' => 'required|integer',
			'item.*.type' => 'required|valid_type',
			'item.*.discounted' => 'required',
			'total' => 'required|numeric',
			'buyer_type' => 'required',
		];

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'numeric'	=> "Please indicate a valid amount.",
			'integer'	=> "Please indicate a valid quantity.",
			'exist_product' => "Product does not exist.",
			'valid_type' => "Please indicate a valid type.",
		];
	}
}