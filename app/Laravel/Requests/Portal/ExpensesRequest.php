<?php namespace App\Laravel\Requests\Portal;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

use Request;
class ExpensesRequest extends RequestManager{

	public function rules(){

		
		$id = Auth::user()->id;
		switch (Request::segment(1)) {
			case 'expense-title':
				$rules = ['account_title' => "required|unique:account_title,account_title,{$id}"];
				return $rules;
				break;
			
			default:
			$rules = [
			'title' => "required",
			'transaction_date' => "required",
			'supplier' => "required",
			'tin' => "required",
			'convertion' => "required", 
			'description' => "required",
			'uom' => "required",
			'qty' => "required", 
			'line_amount' => "required",
			'remarks' => "required",
			'address' => "required", 
	

		];
			return $rules;
				break;
		}
	

	
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'unique_partner_code'	=> "Partner Code already used. Try another",
			'unique_username'	=> "Username already used. Try another",
			'unique_email'	=> "Email already used. Try another",
			'password.confirmed'	=> "Password mismatch.",
			'password_format' => "Password must be 6-20 alphanumeric and some allowed special characters only.",
			'username_format' => "Username must be 6-20 alphanumeric and some allowed special characters only.",
			'margin.numeric' => "Indicate sales margin as 10 for 10%, 15 for 15% etc.",
			'margin.min' => "Indicate sales margin as 10 for 10%, 15 for 15% etc.",

		];
	}
}

