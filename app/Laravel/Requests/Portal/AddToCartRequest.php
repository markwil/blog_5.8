<?php namespace App\Laravel\Requests\Portal;

use Session,Auth;
use App\Laravel\Requests\RequestManager;


class AddToCartRequest extends RequestManager{

	public function rules(){

		$id = $this->route('partner_id')?:0;
		// $id = Auth::user()->id;
		// if(auth()->user()->type == "store_admin"){
		// 	unset($rules['password']);
		// }
		$rules = [
			'cart_id' => "required",
			'supplier_id' => "required",
			'branch_id' => "required",
			'product_code' => "required",
			'description'	=> "required",
			'price'	=> "required",
			'measurement'	=> "required",
			'measurement_qty'	=> "required",
			'qty'=>"required",
			'total_amount' => "",
		];

		if($id > 0){
			unset($rules['password']);
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'unique_partner_code'	=> "Partner Code already used. Try another",
			'unique_username'	=> "Username already used. Try another",
			'unique_email'	=> "Email already used. Try another",
			'password.confirmed'	=> "Password mismatch.",
			'password_format' => "Password must be 6-20 alphanumeric and some allowed special characters only.",
			'username_format' => "Username must be 6-20 alphanumeric and some allowed special characters only.",
			'margin.numeric' => "Indicate sales margin as 10 for 10%, 15 for 15% etc.",
			'margin.min' => "Indicate sales margin as 10 for 10%, 15 for 15% etc.",

		];
	}
}

