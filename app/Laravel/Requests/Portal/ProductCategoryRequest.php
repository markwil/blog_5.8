<?php namespace App\Laravel\Requests\Portal;

use Session,Auth;
use App\Laravel\Requests\RequestManager;
use Request;
class ProductCategoryRequest extends RequestManager{

	public function rules(){

		// $id = $this->route('$id')?:0;
		$id = Auth::user()->id;
		switch (Request::segment(2)) {
			case 'edit':
			$rules = [
			'category_name'	=> "required"];
			return $rules;
			break;
			default:
			$rules = ['category_name'	=> "required|unique:product_categories,category_name,".$id];
			return $rules;
			break;
		}
		

		
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'unique_partner_code'	=> "Partner Code already used. Try another",
			'unique_username'	=> "Username already used. Try another",
			'unique_email'	=> "Email already used. Try another",
			'password.confirmed'	=> "Password mismatch.",
			'password_format' => "Password must be 6-20 alphanumeric and some allowed special characters only.",
			'username_format' => "Username must be 6-20 alphanumeric and some allowed special characters only.",
			'margin.numeric' => "Indicate sales margin as 10 for 10%, 15 for 15% etc.",
			'margin.min' => "Indicate sales margin as 10 for 10%, 15 for 15% etc.",

		];
	}
}

