<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" href="{{asset('assets/images/logo.png')}}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{csrf_token()}}">
    
        <title>{{ $page_title }} </title>
        <!-- <script src='https://kit.fontawesome.com/a076d05399.js'></script> -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/datatables/datatables.min.css') }}">


           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
           <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
            
        <link id="nooklifestylestore" rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css')}}">

        <link rel="stylesheet" href="{{asset('assets/styles/vendor/perfect-scrollbar.css')}}">
        @yield('page-styles')
        <style>
          .card-icon-bg .card-body .content { max-width: 100px; }
        h1,h2,h3,h4,h5,p,span {
          font-family: 'Montserrat', sans-serif !important;
        }
        .text-white {
          color: white;
        }
        .bg-white {
          background-color: white;
        }
          .card-1:hover {
            box-shadow: 0 3px 3px rgba(0,0,0,0.23), 0 3px 3px rgba(0,0,0,0.22);
          }   
        </style>

    </head>


    <body class="text-left">
        {{-- <!-- Pre Loader Strat  -->
        <div class='loadscreen' id="preloader">
            <div class="loader spinner-bubble spinner-bubble-primary">
            </div>
        </div>
        <!-- Pre Loader end  --> --}}
        <div class="app-admin-wrap layout-sidebar-large clearfix">
        @include('portal.components.header')
        @include('portal.components.nav')
        </div>

        <div class="main-content-wrap sidenav-open d-flex flex-column">
          <div class="main-content">
            @yield('breadcrumb')
            @yield('content')
          </div>

          <div class="flex-grow-1"></div>
         {{--  <div class="app-footer">
              <div class="footer-bottom d-flex flex-column flex-sm-row align-items-center">
                  <span class="flex-grow-1"></span>
                  <div class="d-flex align-items-center">
                      <img class="logo" src="{{asset('assets/images/logo.png')}}" alt="">
                      <div>
                          <p class="m-0">&copy; 2019 {{env('APP_NAME')}}</p>
                          <p class="m-0">All rights reserved</p>
                      </div>
                  </div>
              </div>
          </div> --}}
        </div>
    <script src="{{asset('assets/js/common-bundle-script.js')}}"></script>
    <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
    <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
    {{-- <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script> --}}
    <script src="{{asset('assets/js/script.js')}}"></script>
    <script src="{{asset('assets/js/sidebar.large.script.js')}}"></script>
    <script src="{{asset('assets/js/customizer.script.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    @yield('page-scripts')
    </body>

</html>
