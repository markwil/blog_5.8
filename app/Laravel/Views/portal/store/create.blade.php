@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Branch Create  Form</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-4">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data </h3>
          <span class="float-right">
            <a href="{{route('portal.branch.index')}}" class="btn btn-primary">Back to Branch List</a>
          </span>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <!-- <div class="row justify-content-center"> -->


    <!-- <div class="col-md-5 mt-5 mb-5"> -->
      <form action="" method="POST">
        {{ csrf_field() }}
                <div class="form-group">
                  <label for="branch_name">Branch Name</label>
                  <input type="text" name="branch_name" value="{{old('branch_name')}}" class="form-control" >
                  @if($errors->first('branch_name'))
                  <p class="form-text text-danger">{{$errors->first('branch_name')}}</p>
                  @endif
                </div>
                <div class="form-group">
                  <label for="branch_name">Branch Location/Address</label>
                  <input type="text" name="branch_location" value="{{old('branch_location')}}" class="form-control" >
                  @if($errors->first('branch_location'))
                  <p class="form-text text-danger">{{$errors->first('branch_location')}}</p>
                  @endif
                </div>
                <div class="form-group">
                  <label for="branch_name">Contact</label>
                  <input type="text" name="branch_contact" value="{{old('branch_contact')}}" class="form-control" >
                  @if($errors->first('branch_contact'))
                  <p class="form-text text-danger">{{$errors->first('branch_contact')}}</p>
                  @endif
                </div>


                <div class="form-group">
                  <button type="submit" class="btn btn-primary">Proceed</button>
                </div>
              <!-- </div> -->
            </form>

          <!-- </div> -->
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })


 })
</script>
@stop
