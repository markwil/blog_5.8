@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>DELIVER FORM LIST</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Out for Deliver List - Record Data</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2">
        
           
          </span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center table-responsive">
            <table class="table table-bordered text-center">
            <thead class="thead-dark">
              <th>Order ID</th>
              <th>Date</th>
              <th {{ auth()->user()->id  == 1 ? "" : "hidden"  }} >Branch Destination</th>
              <th>Status</th>
              <th ></th>
              {{-- <th>Action</th> --}}
            </thead>
            <tbody>
              @foreach($deliver as $data)
              <tr>
                <td><span class="badge">{{   str_pad($data->cart_id, 4, "0", STR_PAD_LEFT )  }}</span></td>
                <td><span class="badge">{{  Helper::date_format($data->created_at) }}</span></td>
                <td {{ auth()->user()->id  == 1 ? "" : "hidden"  }}><span class="badge">{{ $data->branch  }} </span></td>
                <td><span class="badge">{!! auth()->user()->type == "super_user" ?  Helper::status_request( $data->status) : Helper::status( $data->status)   !!}</span></td>
                <td >
                 <div class="btn-group">
                   <button type="button"  data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle form-control" aria-expanded="false">ACTIONS 
                    <span class="icon-dropdown mdi mdi-chevron-down"></span>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ auth()->user()->type == 'super_user' ?  route('portal.deliver.details',[$data->cart_id]) :  route('portal.order_list_branch.recieve',[$data->cart_id])}}">View Details</a>
                    @if($data->status == 1 && auth()->user()->type == "super_user")
                     <a class="dropdown-item" {{ $data->status == 3 ? "hidden" : "" }} href="{{ route('portal.deliver.complete_transaction',[$data->cart_id,$data->branch_id]) }}">Ship Order</a>
                    @endif
                   
                  </div>
                </div>
              </td>
              </tr>
              @endforeach

            </tbody>
          </table>
            

          </div>
        </div>
      </div>
    </div>

  </div>




@stop

@section('page-scripts')



<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
