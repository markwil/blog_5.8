@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Transctions Logs</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Transaction log History</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
        
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">
            <table class="table table-bordered text-center">
            <thead class="thead-dark">
            
            
             {{--  <th>Item Count</th> --}}
              <th>Date</th>
              <th>Description</th>
            </thead>
            <tbody>
              @foreach($logs as $log)
              <tr>
                <td>{{ Helper::date_format($log->created_at) }}</td>
              

                @php
                $s =  json_decode($log->new_values, true) ;
                @endphp
              
                <td>
                  @foreach($s as $key => $value)
              {{--     <span class="badge badge-info p-2">{{ $key }}</span><span class="badge badge-success p-2"> {{ $value }}</span> --}}

                  @if(in_array($key,['transaction_id']))
                  <span>Transaction has been created successfully with Transction # {{ $value }} by {{ $log->created_by }} </span>
                  @elseif(in_array($key,['username']))
                  <span>Accoount  has been created successfully with username  {{ $value }} created by {{ $log->created_by }} </span>
                
                  @else
            
                  @endif
                  @endforeach
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
            

          </div>
        </div>
      </div>
    </div>

  </div>



@stop

@section('page-scripts')

@stop
