@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Authentication Logs</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Transaction log History</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
        
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">
            <table class="table table-bordered text-center">
            <thead class="thead-dark">
            
            
             {{--  <th>Item Count</th> --}}
              <th>Date</th>
              <th>Employee Name</th>
              <th>Login</th>
              <th>logout</th>
            </thead>
            <tbody>
             @foreach($auth_logs as $logs)
             <tr>
               <td><span>{{ Helper::date_only($logs->login_at) }}</span></td>
               <td><span>{{ $logs->name }}</span></td>
              <td><span class="badge badge-success p-2">{{ Helper::time_only($logs->login_at) }}</span></td>
              <td><span class="badge badge-warning p-2">{{  $logs->logout_at == NULL ? "--" : Helper::time_only($logs->logout_at ) }}</span></td>
             </tr>
             @endforeach
           </tbody>
          </table>
            

          </div>
        </div>
      </div>
    </div>

  </div>



@stop

@section('page-scripts')

@stop
