@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Cashier Coming Soon</h1>
  
</div>

<div class="separator-breadcrumb border-top"></div>
@stop


 @section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
     $("#removes").attr({"href" : btn.data('url')});
   })
  })
</script>
 @stop