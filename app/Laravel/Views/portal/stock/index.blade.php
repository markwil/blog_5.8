@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Stock Inventory</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2">
            @if($supplier > 0)
             {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a>
             <a href="{{route('portal.stock.create')}}" class="btn btn-sm btn-primary">Recieve Order</a> --}}
            @endif
           
          </span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <div class="row d-flex justify-content-end mb-2">

            <a href="{{ route('portal.quick_report.index',[auth()->user()->branch_id]) }}"  class="btn btn-outline-primary m-1"></i>&nbsp;Transaction In Report</a>
            <a href="{{ route('portal.quick_report.stock_out_report',[auth()->user()->branch_id]) }}"  class="btn btn-outline-secondary m-1"></i>&nbsp;Transaction Out Report</a>
          </div>
          <div class="row justify-content-center">
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Product Code</th>
                    <th scope="col">Description</th>
                    {{-- <th scope="col">Supplier</th> --}}
                    <th scope="col">Stock Available</th>
                    <th scope="col">Supplier Price</th>
                    <th scope="col">Selling Price</th>
                    <th scope="col">Marked-up</th>
                  
                   
                    <th scope="col" {{ in_array(auth()->user()->type,["accounting"]) ? "": "hidden" }}></th>

                  </tr>
                </thead>
                <tbody>

                  @php

                    $totals = 0;
                    $total = 0;
                    $selling_price = 0;
                  @endphp
                    @forelse($stocks as $data)
                    <tr>
                      <td><span>{{ $data->product_code}}</span></td>
                      <td><span>{{ $data->description }}</span></td>
                      <td><span>{{  $data->total_qty - $data->out_qty  }} pcs </span></td>
                      <td><span>{{ $data->price }}</span></td>
                      <td><span>{{ Helper::money_format($data->price + $data->marked_up) }}</span></td>
                      <td><span>{!! Helper::marked_up($data->marked_up) !!}</span></td>
  
                     <td {{ in_array(auth()->user()->type,["accounting"]) ? "": "hidden" }}>
             
                          <a  data-role="edit" data-pcode="{{ $data->product_code }}" data-mark="{{ $data->marked_up }}" href="#marked_up" data-toggle="modal">Edit / Add Marked-up</a>
          
                     </td>
                      
                      @php
                       
                       @endphp
                    </tr>
                    @empty
                    @endforelse
                  <tr>
                     
                    </tr>
                </tbody>
              </table>

            </div>

          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="marked_up">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <form action="" method="POST">
          {{  csrf_field() }}
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
            <input type="hidden" id="product_code" name="product_code">
             <input type="number" name="marked_up" step="any"  id="marked" class="form-control" >
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" > Update </button>
      </div>
    </form>
    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
      $(document).on('click','a[data-role=edit]',function(){
        var marked_up = $(this).data('mark');
        var pcode = $(this).data('pcode');
        $('#marked').val(marked_up)
        $('#product_code').val(pcode)
      })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
