@extends('portal._layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-2"></div>
        <div class="col-md-8 col-lg-6 ml-5">
            <div style="height: 6px;background-color: #663399"></div>
            <div class="card o-hidden p-4" style="border-radius: 0px !important;">
                <div class="row">

                  @if(session()->has('errors'))
                  {{ toast($errors->first(),'error') }}
                  @endif
                    <div class="col-md-12">
                        <div class="p-3">
                             

                             
                            <form action="" method="POST">
                                {!!csrf_field()!!}
                               <div class="form-group">
                                   <input type="text" name="user" id="" class="form-control">
                               </div>
                               <div class="form-group">
                                   <input type="text" name="username" id="" class="form-control">
                               </div>
                               <div class="form-group">
                                   <button type="submit" class="btn btn-primary">submit</button>
                               </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-lg-3 col-md-2"></div>
    </div>
</div>

@include('sweetalert::alert')

@stop