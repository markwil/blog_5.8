@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>REQUEST ORDER LIST</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Pending Order</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
      

        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">
            <table class="table table-bordered text-center">
            <thead class="thead-dark">
              <th>Order ID</th>
              <th>Branch</th>
              <th>Order Date</th>
              <th>Status</th>
              <th>Action</th>
            </thead>
            <tbody>
              @forelse($orders as $data)
              <tr id="">
                <td data-target="cart_id"><span class="badge">{{ $data->cart_id}}</span></td>
                <td data-target="cart_id"><span class="badge">{{ $data->branch }}</span></td>
                <td data-target="product_name"><span class="badge">{{ Helper::date_format($data->created_at) }}</span></td>
      
                <td data-target="Status">{!! Helper::status($data->status) !!}</td>
       
                <td>
                  
                    <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-outline-primary" {{ $data->status == 3 ? "hidden" : "" }} href="{{ route('portal.stock-request.details',[$data->cart_id]) }}" type="button">Details</a>
                    <a class="btn btn-outline-success" href="#approved" {{ in_array($data->status,[1,2,3,4]) ? "hidden" : "" }} data-role="approve" data-url="{{ route('portal.stock-request.approve',[$data->cart_id]) }}" data-supplier="{{ $data->supplier_id }}" data-amount="{{ $data->total_amount }}" data-toggle="modal" id="approve" type="button">Approve </a>
                {{-- 
                    <a class="btn btn-danger" {{ $data->status == 3 ? "" : "hidden" }} href="#remove" data-role="remove" data-url="{{ route('portal.order-list-branch.delete',[$data->id]) }}" data-toggle="modal" id="cancel" type="button">&times;</a> --}}

                  </div>
                </td>
              </tr>
             
              @empty
              @endforelse
            </tbody>
          </table>
            

          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- The Modal -->
  <div class="modal fade" id="approved">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
    
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">

             <span>Are you sure want to approve this Order?</span>
              
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-outline-primary" href="#" data-id="" id="removes" >Yes</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=approve]',function(){
    var btn = $(this);
    // alert(btn.data('amount'))
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
