@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material</h1>

  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header d-flex justify-content-between">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
             <a {{ $status != "pending" ? "hidden" : "" }} class="btn btn-primary"  href="#approve" data-toggle="modal">Recieve order</a>
              <a href=" {{ route('portal.stock-request.index') }} " class="btn btn-primary" >Back</a>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
         
          <table class="table table-bordered">
            <thead>
              <th>Product Code</th>
              <th>Qty</th>
              <th>Unit</th>
              <th class="text-right">Total</th>
            </thead>
            <tbody>
              @foreach ($store_requests as $data)
                <tr>
                  <td>{{ $data->product_code }} </td>
                  <td>
                   
                    {{ $data->product_qty - floor($data->product_qty) == 0 ? floor($data->product_qty) : $data->product_qty  }}</td>
                  <td>{{ $data->product_unit }}</td>
                  <td class="text-right">1000</td>
                </tr> 

              @endforeach
              <tr>
                  <td>TOTAL</td>
                  <td colspan="3" class="text-right">1000</td>
                </tr> 
            </tbody>
          </table>
          {{-- <div class="form-group">
           
            <a class="btn btn-primary"  href="#approve" data-toggle="modal">Recieve order</a>
          </div> --}}
           
        </div>
    </div>
  </div>
</div>



  <div class="modal fade" id="approve">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             <span>Are you sure want to Recieve this order?</span>
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <form action="" method="post">
        {{ csrf_field() }}
        <div class="modal-footer" style="background:#ddd">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Recieve it!</button>
        </div>
      </form>

    </div>
  </div>
</div>

{{-- end modal --}}



<script>  

  
</script>
@stop
