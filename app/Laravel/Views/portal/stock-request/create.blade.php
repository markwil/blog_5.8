@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material(s) Request</h1>
  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-10">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
          <div class="row">

            <form action="" method="POST">
                <div class="col-lg-4 col-lg-4">
                        <div class="form-group">
                            <label for="product_code">Raw Materials</label>
                            <input type="text" id="test">
                        </div>
                        
              </div>
            <div class="col-lg-4 col-lg-4">
                        <div class="form-group">
                            <label for="product_code">Raw Materials</label>
                            <select name="product_code"  class="form-control">
                                <option value="">--Choose raw materials--</option>
                                  @foreach($product as $data)
                                <option value="{{ $data->product_code  }}">{{ $data->product_name }}</option>
                                  @endforeach     
                            </select>
                        </div>
                        
              </div>
              <div class="col-lg-2 col-lg-2">
                        <div class="form-group">
                            <label for="ingredients">Qty</label>
                            <input type="number" name="product_qty" class="form-control" placeholder="0">
                        </div>    
              </div>
               <div class="col-lg-2 col-lg-3">
                        <div class="form-group">
                            <label for="ingredients">MoU</label>
                            <select name="product_unit" id="" class="form-control" >
                              <option value="">--select unit--</option>
                              <option value="kg">Kilogram(s)</option>
                              <option value="g">gram(s)</option>
                              <option value="ml">mililitre(s)</option>
                              <option value="L">litre(s)</option>
                            </select>
                        </div>    
              </div>
              <div class="col-lg-2 mt-4 col-md-2">
                <button type="submit" class="btn  btn-primary">Submit</button>
              </div>
               </form>
            </div>

   
          </form>
        </div>
    </div>
  </div>
</div>
@stop

