@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>ORDER LIST</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-12">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Item List</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2">
           
             {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a> --}}
             @if(auth()->user()->type == "super_user")
             <a  href="{{ route( 'portal.order_list.recieve',[Request::segment(2)]) }}"  class="btn  btn-outline-primary">Transaction In</a>
             <a  href="{{ route( 'portal.order_list.print',[Request::segment(2)]) }}"  class="btn btn-outline-success">Print</a>
             @else
            {{--   <a  {{ $order_header->status == 0 ? "hidden": "" }} href="{{ route( 'portal.order-list.recieve',[Request::segment(2)]) }}"  class="btn btn-sm btn-primary">Transaction In</a> --}}
             @endif
            
         
           
          </span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Item Description</th>
                    <th scope="col">Quantity</th>
                    {{-- <th scope="col">Stock(s) on Hand</th> --}}
                    <th scope="col">Unit Price</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col"></th>
                  </tr>
                  
                </thead>
                <tbody>
                      @php 
                    $total = 0;
                  @endphp
                  @forelse($cart_details as $data)
                  <tr id="{{ $data->id }}">
                    <td>{{ strtoupper($data->description) }}</td>
                    <td>{{ $data->qty }}</td>
                    {{-- <td>{{ $data->available_stock == NULL ? 0 : $data->available_stock }}</td> --}}
                    <td>{{ Helper::money_format($data->price) }}</td>
                    <td>{{ Helper::money_format($data->total_amount ) }}</td>
                    {{-- td>{{ Helper::money_format($data->total_price)}}</td> --}}
                    <td>
                    <a  class="btn btn-outline-primary" href="#quantity" data-toggle="modal" id="qty" data-role="edit" data-id="{{ $data->id  }}" data-qty="{{ $data->qty }}">Edit</a>
                     <a  class="btn btn-outline-danger" href="#remove" data-toggle="modal" data-url="{{ route('portal.order_list.remove-product',[$data->id,$data->cart_id]) }}"  data-role="remove" data-id="{{ $data->id  }}" data-qty="{{ $data->qty }}">Remove</a>
                    </td>
                  </tr>
                  @php
                  // $total += $data->total_price;
                  @endphp
                  @empty

                  <tr><td colspan="5" class="text-center">No Data Found</td></tr>
                  @endforelse
                  <tr>

                    </tr>
                </tbody>
              </table>

            </div>

          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Supplier List</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
          <span>Are you sure want to remove this product ?</span>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-outline-primary" data-url="" href="#" data-id="" id="r_product" >Proceed</a>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}

  <!-- The Modal -->
  <div class="modal fade" id="quantity">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Quantity</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="{{ route('portal.order_list_details.update')}}" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
              <input type="hidden" name="id" id="id"  class="form-control">
              <input type="number" name="qty" id="qtys" value="" class="form-control">
           </div>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" href="#" data-id="" id="removes"  >Update</button>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
    $(document).on('click','a[data-role=edit]',function(){
      var qtys = $(this).data('qty');
     
      $('#qtys').val(qtys);
      $('#id').val($(this).data('id'));
    })

   $(document).on('click','a[data-role=remove]',function(){
    var btn = $(this);

    $("#r_product").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
