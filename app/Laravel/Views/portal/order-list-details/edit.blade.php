@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material</h1>
  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="product_supplier"><b>Supplier</b></label>
              {!!Form::select('product_supplier',$supplier,old('type'),['class' => "form-control",'id' => "input_type"])!!}
                @if($errors->first('supplier'))
                <p class="form-text text-danger">{{$errors->first('product_supplier')}}</p>
                @endif

            </div>
            <div class="form-group">
                <label for="product_name"><b>Product Name</b></label>
                <input type="text" class="form-control" id="product_name" placeholder="" value="{{old('name')}}" name="product_name">
                @if($errors->first('product_name'))
                <p class="form-text text-danger">{{$errors->first('product_name')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="product_name"><b>Product Code</b></label>
                <input type="text" class="form-control" id="product_code" placeholder="" value="{{old('product_code')}}" name="product_code">
                @if($errors->first('product_code'))
                <p class="form-text text-danger">{{$errors->first('product_code')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="product_name"><b>Supplier Price</b></label>
                <input type="number" class="form-control" id="supplier_price" placeholder="" value="{{old('supplier_price')}}" name="supplier_price">
                @if($errors->first('supplier_price'))
                <p class="form-text text-danger">{{$errors->first('supplier_price')}}</p>
                @endif
            </div>
             <div class="form-group">
                <label for="marked_up"><b>Marked-up Price (%)</b></label>
                <input type="number" class="form-control" id="supplier_price" placeholder="" min="0" max="100" value="{{old('marked_up')}}" name="marked_up">
                @if($errors->first('marked_up'))
                <p class="form-text text-danger">{{$errors->first('marked_up')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="qty"><b>Quantity</b></label>
                <input type="number" min="0" step="any" class="form-control" id="qty" placeholder="" value="{{old('product_qty')}}" name="product_qty">
                @if($errors->first('product_qty'))
                <p class="form-text text-danger">{{$errors->first('product_qty')}}</p>
                @endif


            </div>



            <div class="form-group">
                <label for="input_type"><b>Measurement of Unit</b></label>
               {{--  <input type="text" class="form-control" id="product_unit" placeholder="" value="{{old('product_unit')}}" name="product_unit"> --}}

               <select name="product_unit" id="" class="form-control">
                 <option value="{{old('product_unit') }}">{{ old('product_id','Select Measurement of Unit') }}</option>
                 <option value="kg">kilograms</option>
                 <option value="g">grams</option>
                 <option value="L">Litre</option>
                 <option value="ml">Mililetre</option>
               </select>
                @if($errors->first('product_unit'))
                <p class="form-text text-danger">{{$errors->first('product_unit')}}</p>
                @endif
            </div>



            <div class="form-group">
              <a href="{{ route('portal.stock.index') }}" class="btn btn-danger mr-2">Back</a>
               <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop
