  @extends('portal._layouts.main')

  @section('breadcrumb')
  <div class="breadcrumb">
    {{-- <h1>SUPPLIER : {{ strtoupper($supplier_name->name) }}</h1> --}}
    {{-- <ul>
        <li><a href="">Dashboard</a></li>
        <li>Version 1</li>
      </ul> --}}
    </div>

    <div class="separator-breadcrumb border-top"></div>
    @stop

    @section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="card o-hidden mb-4">
          <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Transaction In</h3>
            <span class="float-right">
              <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
            </span>
            <span class="float-right mr-2">
              {{-- @if($supplier > 0) --}}
               {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a> --}}
               {{-- <a href="#confirm"  data-toggle="modal" class="btn btn-sm btn-primary">Confirm Order</a> --}}
           {{--     <a href="{{ route('portal.stock.index') }}"  class="btn btn-sm btn-primary">Go To Inventeory</a>
              @endif --}}
             
            </span>

          </div>
          <div class="card-body">
            @include('portal.components.notifications')


           
           
  <form action="{{ route('portal.order_list_details.purchase',[Request::segment(3)]) }}" method="POST">
            <div class="row">

         

              {{-- LIST DETAILS --}}

              <div class="col-lg-12">
              <div class="table-responsive">
               
                {{--   <h3>ORDER SUMMARY <a type="submit" href="{{ route('portal.order-list-details.print',[$order_header->cart_id,$order_header->supplier_id]) }}" class="btn btn-primary float-right">Print</a></h3> --}}
         
                  <hr>
                   <table class="table table-bordered">
                     <thead class="thead-dark">
                        {{-- <th>Order ID</th> --}}
                        <th>Item Code</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total Amount</th>
                     </thead>
                     <tbody>
                       @forelse($orders as $data)

                       <tr>
                           <input type="hidden" name="supplier_idd[]" value="{{ $data->supplier_id }}">
                       <td hidden=""><input type="hidden"  value="{{ $data->measurement }}" name="measurement[]"></td>
                       <td hidden=""><input type="text"  value="{{ $data->measurement_qty }}" name="measurement_qty[]"></td>
                       <td>{{ strtoupper($data->product_code) }} <input type="hidden" name="product_code[]" value="{{ $data->product_code }}"></td>
                       <td>{{ $data->description }}<input type="hidden" name="description[]" value="{{ $data->description }}"></td>
                       <td>{{ $data->qty }} <input type="hidden" name="qty[]" value="{{ $data->qty }}"></td>
                       <td>{{ Helper::money_format($data->price) }}<input type="hidden" name="price[]" value="{{ $data->price }}"></td>
                       <td class="text-right">₱ {{ Helper::money_format($data->price * $data->qty ) }}</td>
                       </tr>
                       @empty
                       <tr>
                        <td colspan="5"><center>Transaction Completed</center></td>
                      </tr>
                       @endforelse
                     </tbody>
                   </table>  

              </div>
                <div class="text-right">
                  <h2 class="ul-ul-widget1__title">Grand Total : ₱ <span class="ul-widget__number text-primary">{{ Helper::money_format($order_header->total_amount) }}</span> </h2>
               </div>
              </div>
              <div class="offset-lg-7 col-lg-5">

              <div class="table-responsive">
                     
                    {{ csrf_field() }}

                    <input type="hidden" value="" hidden="" name="cart_id">
                    <input type="hidden" value="bill" hidden="" name="transaction_type">
                    <input type="text" hidden="" value="{{ Request::segment(3) }}" name="supplier_id">
                    
                   <div class="form-group" hidden="">
                      <label for="">Terms</label>
                      <input type="number" name="terms" id="" value="0" class="form-control">
                     
                    </div>
                    
               
                    <div class="form-group" hidden="">
                      <label for="">Date of Deliver</label>
                      <input type="text" name="date" readonly="" id="" value="{{ date('Y-m-d')}}" class="form-control">
                    </div>
                 

                    <div class="form-group" >
                      <input type="number" hidden="" name="due" value="{{ $order_header->total_amount ? $order_header->total_amount : "" }}" readonly="" id="" class="form-control">
                      <input type="number" hidden="" name="cart_id" value="{{ $order_header->cart_id ? $order_header->cart_id: "" }}" readonly="" id="" class="form-control">
                      <input type="number"  hidden="" name="supplier_iddd" value="{{ $order_header->supplier_id ?  $order_header->supplier_id : ""}}" readonly="" id="" class="form-control">
                    </div>

                    <div class="form-group text-right">
                      <button type="submit" class="btn btn-outline-primary" {{ $order_header->total_amount == 0 ? 'hidden':"" }}>Recieved Deliver </button>
                    </div>
               
              </div>
              </div>
            </div>

                <!-- The Modal -->
    <div class="modal fade" id="confirm">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Confirm</h4>
            {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
          </div>
           
          <div class="modal-body">
           
           <span>Are You Sure Want To Complete Transacation</span>
          
         </div>

         <!-- Modal footer -->
         <div class="modal-footer" style="background:#ddd">
         
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline-primary" id="btns" >Proceed</button>
        </form>
        </div>
   
      </div>
   
    </div>
  </div>

  {{-- end modal --}}
           </form>
          </div>


        </div>
      </div>

    </div>




  @stop

  @section('page-scripts')
  <script>
    $(document).ready(function(){

      $('#btns').on('click',function(){
        window.location.reload()
      })

      // $('#terms').on('keyup',function(){
      //   var test = $(this).val()
      //   alert(test)
      //   $('#due_date').val('{{ Carbon::now()->addDays(10) }}'
      // })
     
      
     $(document).on('click','a[data-role=delete]',function(){
      var btn = $(this);
      $("#removes").attr({"href" : btn.data('url')});
    })
   })
  </script>
  @stop
