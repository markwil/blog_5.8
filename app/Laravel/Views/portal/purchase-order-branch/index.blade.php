@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Purchase Order</h1>
 
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Purchase Orders Form</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <div class="row">
    
            </div>
            <br>

    <div class="row justify-content-center">
            <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
            <thead class="thead-dark">      
              <th>Item Description</th>
              <th>Item Code</th>
              <th>Unit Price</th>
              <th>Available Stocks</th>
              <th>Action</th>
            </thead>
            <tbody>
              @forelse($products as $data)
             <tr>
               <td>{{ $data->description }}</td>
               <td>{{ $data->product_code }}</td>
                <td>{{ number_format($data->price + $data->marked_up,2 )}}</td>
               <td>{{ $data->total_qty - $data->out_qty }}</td>
               <td>
                 <div class="btn-group" role="group" aria-label="Basic example">
                  <a class="btn btn-outline-primary" href="#myModal" data-role="add"
                  data-toggle="modal"
                  data-price="{{$data->price + $data->marked_up}}"
                  data-code="{{$data->product_code}}"
                  data-description="{{$data->description}}"
                  data-measurement="{{ $data->measurement}}"
                  data-mqty="{{$data->measurement_qty}}"
                  {{-- data-qtya="{{ $data->current_stock == NULL ? $data->qty: $data->current_stock  }}" --}}
                  type="button">Add To Cart</a>
                </div>
               </td>
             </tr>
             
              @empty
              @endforelse
            </tbody>
          </table>
            

          </div>
        </div>
    </div>
  </div>
</div>


<!-- The Modal -->
   <div class="modal fade" id="myModal">
     <div class="modal-dialog">
       <div class="modal-content">
  
     <!-- Modal Header -->
         <div class="modal-header">
           <h4 class="modal-title">Order Details</h4>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
         <!-- Modal body -->
         <form action="" method="POST">
           {{ csrf_field() }}
         <div class="modal-body">
            <input type="hidden" id="quantity_a" name="description" placeholder="description">

          <input type="hidden" value="{{ auth()->user()->id }}" id="cart_id" name="cart_id">
          <input type="hidden" value="1" id="supplier_id" name="supplier_id">
          <input type="hidden" value="" id="product_codes" name="product_code" placeholder="product_code">

          <input type="hidden"  name="branch_id" value="{{ auth()->user()->branch_id }}">
          <input type="hidden" id="descriptions" name="description" placeholder="description">
        
          <input type="hidden" id="measurement" name="measurement" placeholder="measurement">
          <input type="hidden" id="measurement_qty" name="measurement_qty" placeholder="measurement_qty">
          <input type="hidden" id="price" name="price">
          {{-- <input type="text" value="" id="product_id" name="product_id" placeholder="product_id"> --}}
            {{-- <input type="hidden" value="" id="image" name="image"> --}}

           <table class="table table-bordered">
             <tr>
               <td><span class="badge">Description</span></td><td><span class="badge" id="desc"></span></td>
             </tr>
             <tr>
                <td><span class="badge">Price</span></td><td><span class="badge" id="prc"></span></td>
             </tr>
            {{--  <tr>
                <td><span class="badge">Your Stock(s)</span></td><td><span class="badge" id="prc"></span></td>
             </tr> --}}
             <tr>
                <td><span class="badge">Quantity</span></td><td><input type="number"  name="qty" id="qty"  min="1" value="1" class="form-control"></td>
             </tr>
           </table>

         </div>

         <!-- Modal footer -->
         <div class="modal-footer">
           <button type="submit" id="add_to_orderlist" class="btn btn-outline-primary">Add to Order List</button>
           <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
          
         </div>
       </form>
     </div>
   </div>
 </div>
           {{-- <a href="{{ route('portal.purchase-order-supplier.add-to-cart') }}" class="btn btn-primary">Add To Order List</a> --}}
@stop
@section('page-scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('assets/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/datatables/datatables.script.min.js') }}"></script>
<script type="text/javascript">
   $(document).ready(function(){


    $('#qty').on('keyup',function(){
      var x = parseInt($(this).val()) 
      var y = parseInt($('#quantity_a').val())
      if(x  >  y)
      {
        $('#add_to_orderlist').attr('disabled','disabled');
        alert('Unsuficient Quantity')
      }
      else
      {
        $('#add_to_orderlist').removeAttr('disabled','disabled');
      }
    })

$(document).on('click','a[data-role=add]',function(){

  $('#price').val($(this).data('price'))
  $('#descriptions').val($(this).data('description'))
  $('#measurement').val($(this).data('measurement')) 
  $('#measurement_qty').val($(this).data('mqty'))
  $('#product_codes').val($(this).data('code'))
  $('#product_ids').val($(this).data('pid'))
  $('#quantity_a').val($(this).data('qtya'))
   

    $('#prc').text($(this).data('price'))
    $('#desc').text($(this).data('description'))
    // $('#image').val($(this).data('image'))
})

$('#search').on('keyup',function(){
  var code = $(this).val();
  $.ajax({
    method:"post",
    url:"{{ route('portal.purchase_order_branch.search') }}",
    data: { "_token":"{{ csrf_token() }}", code:code },
    success:function(data)
    {

        if(data.length > 0 )
        {
            
        }
        else if(code == "" || code == null)
      {
        $('#result').css("display","block")
      }
        else
        {
          $('#result').css("display","none")
        }
    }
  })


})
});
  
</script>
@stop
