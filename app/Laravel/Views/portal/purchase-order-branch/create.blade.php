@extends('portal._layouts.main')
 
@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material</h1>
  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <div class="row">
            @foreach($products as $product)
           
            <div class="col-lg-4 mb-4">
              <div class="text-left p-4" style="border:1px solid #eee">
                <div class="row">
                  <div class="col-lg-6">
                    <p>Product Name</p>
                  </div>
                  <div class="col-lg-6">
                    <p class="font-weight-bold ">{{ strtoupper($product->product_name) }}</p>
                  </div>
                   <div class="col-lg-6">
                    <p>Price / Kilo</p>
                  </div>
                  <div class="col-lg-6">
                    <p>&#x20B1;{{ $product->selling_price }}</p>
                  </div>
                  <div class="col-lg-6">
                    <p>Price / Bottle</p>
                  </div>
                  <div class="col-lg-6">
                    <p>&#x20B1;{{ $product->selling_price * 5 }}</p>
                  </div>

                   <div class="col-lg-6">
                    <p>Availability</p>
                  </div>
                  <div class="col-lg-6">
                    <p>{{ $product->product_qty > 0 ? "on-stock" : "out of stock"}}</p>
                  </div>
                </div>
                
                  <a class="btn btn-primary w-100" data-role="order" style="color:#fff" id="add_order"  data-toggle="modal" data-target="#add"
                  data-code="{{ $product->product_code }}"
                  data-name="{{ $product->product_name }}"
                  data-price="{{ $product->selling_price }}"
                  >Add</a>
              </div>
            </div>
           @endforeach

          </div>
         </div>
    </div>
  </div>
</div>



<!-- The Modal -->
  <div class="modal fade" id="add">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <form action="">
           <div class="form-group">

            <select name="mou" id="mou" class="form-control">
              <option value="">-Choose Type of Measurement-</option>
              <option value="bottle">Bottle</option>
              <option value="kl">Kilo</option>
            </select>
             
           </div>
           <div class="form-group">
            <input type="number" class="form-control" name="qtys" value="1" min="1" >
           </div>
           <input type="text"  name="product_name" id="name" >
           <input type="text"  name="product_code" id="code" >
           <input type="text"  name="price" id="price">
          </form>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Add to Order List</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop
@section('page-scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
$(document).on('click','a[data-role=order]',function(){
  var code = $(this).data('code')
  var name = $(this).data('name')
  var price = $(this).data('price')

  $("#code").val(code)
  $("#name").val(name)
  $("#price").val(price)
})

$('#mou').on('change',function(){

  switch($(this).val())
  {
    case 1:"bottle"
    $('#price').val(200)
    break;
    case:2:"pack"
     $('#price').val(200)
    break;
    default:
    $('#price').val(300)

  }


})
var searchdata = [];
  var name = $('#search').val();
  $.ajax({
          type : 'GET',
          url  :  "{{url('search')}}",
          data :  {name:name},
        }).done(function(data)
        {
  
          $.each(data, function( index, value ) 
          {
            searchdata.push(data[index].product_name);
          });
          $("#search").autocomplete(
          {
            source    : searchdata,
            minlength :3,
            autofocus : true
          });       
    });
});
  
</script>
@stop
