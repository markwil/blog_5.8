@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material(s) Request</h1>
  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-10">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
          <div class="row">
           



            <form action="" method="POST">
              <input type="hidden" name="branch_id" value="{{ auth()->user()->branch_id }}">
            

            <div class="col-lg-4 col-lg-4">
                        <div class="form-group">
                            <label for="product_code">Raw Materials</label>
                           <input type="text" id="search" placeholder="Please Enter Product Name" name="product_code" value="{{old('product_code')}}" class="form-control" autocomplete="off">
                            @if ($errors->first('product_code'))
                              <span class="text text-danger">{{ $errors->first('product_code') }}</span>
                            @endif
                        </div>
                        
              </div>  
            
      
              <div class="col-lg-2 col-lg-2">
                        <div class="form-group">
                            <label for="ingredients">Qty</label>
                            <input type="number"  name="product_qty" value="{{ old('product_qty')}}" class="form-control" placeholder="0">
                             @if ($errors->first('product_qty'))
                              <span class="text text-danger">{{ $errors->first('product_qty') }}</span>
                            @endif
                        </div>    
              </div>
               <div class="col-lg-2 col-lg-3">
                        <div class="form-group">
                            <label for="ingredients">Mearment of Unit</label>
                            <select name="product_unit" id="" class="form-control" >
                              <option value="">--select unit--</option>
                              <option value="bottle">Bottle</option>
                              <option value="bag">Bag</option>
                              <option value="kg">Kilogram</option>
                              <option value="g">gram</option>
                              <option value="ml">mililitre</option>
                              <option value="L">litre</option>
                            </select>
                        </div>  
                         @if ($errors->first('product_unit'))
                              <span class="text text-danger">{{ $errors->first('product_unit') }}</span>
                            @endif  
              </div>
              <div class="col-lg-2 mt-4 col-md-2">
                <button type="submit" class="btn btn-sm  btn-primary">Add Order</button>
              </div>
               </form>
            </div>

 
          </form>


<form action="{{ route('portal.raw-material-request.addtocart') }}" method="post">
  {{ csrf_field() }}
          <table class="table table-bordered">
            <th>Product Name</th>
            <th>product Qty</th>
            <th>Measurement of Unit</th>
            <th>Price</th>

            @if (Session::has('cart'))
                @php
                  $x = 0;
                @endphp
              @foreach ($cart as $data)
              <tr>
                
                <td>
                  <input type="hidden" value="{{ $data['product_code'] }}" name="product_code[]">
                {{ $data['product_code'] }}
                
              </td>
                <td>
                   <input type="hidden" value="{{ $data['product_qty'] }}" name="product_qty[]">
                {{ $data['product_qty']}}
                

              </td> 

               <td>
                   <input type="hidden" value="{{ $data['product_unit'] }}" name="product_unit[]">
                {{ $data['product_unit']}}
                

              </td>   
              </tr>
            @endforeach
            <tr>
              {{-- <td>{{ Session::get('cart')->totalQty }}</td>
              <td>{{ Session::get('cart')->totalPrice }}</td> --}}
              
            </tr>
              @else

            @endif
          
          </table>
          <button type="submit">procceed</button>
      </form>    
        </div>
    </div>
  </div>
</div>
@stop
@section('page-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript">
   $(document).ready(function(){


      var searchdata = [];
  var name = $('#search').val();
  $.ajax({
          type : 'GET',
          url  :  "{{url('search')}}",
          data :  {name:name},
        }).done(function(data)
        {
  
          $.each(data, function( index, value ) 
          {
            searchdata.push(data[index].product_name);
          });
          $("#search").autocomplete(
          {
            source    : searchdata,
            minlength :3,
            autofocus : true
          });       
    });
});
  
</script>
@stop
