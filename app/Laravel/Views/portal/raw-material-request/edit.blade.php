@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material</h1>
  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
        </div>
        <div class="card-body">
          <div class="form-group d-flex justify-content-end">
              <a type="submit"  href="{{ route('portal.raw-material-request.index') }}"  class="btn btn-info btn-sm ">Back</a>
          </div>
        
          <form action="" method="POST">
            {!!csrf_field()!!}
           <table class="table table-borderd">
             <thead>
               <th scope="col">Order ID</th>
                          <th scope="col">Product Code</th>
                          {{-- <th scope="col">Slug</th> --}}
                          <th scope="col">Quantity</th>
                          <th scope="col">UOM</th>
                          <th scope="col">Date Ordered</th>
                          <th scope="col">Status</th>
                          <th scope="col"></th>
             </thead>

             @foreach ($raw_material as $data)
               <tr>
                 <td>{{ $data->order_id }}</td>
                 <td>{{ strtoupper($data->product_code) }}</td>
                 <td>{{ $data->product_qty - floor($data->product_qty ) == 0 ? floor($data->product_qty ) : $data->product_qty  }}</td>
                 <td>{{ $data->product_unit }}</td>
                 <td>{{ date('M d, Y', strtotime($data->created_at)) }}</td>
                 <td> <span class="badge badge-warning">{{  strtoupper($data->product_request_status ) }}</span></td>
                 <td>
                   <div class="btn-group">
                    <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle form-control" aria-expanded="false">Actions<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <div class="dropdown-menu">
                      @if ($data->product_request_status == "pending")
                      <a class="dropdown-item" href="">View Details</a>
                      @else
                      <a class="dropdown-item" href="">Recieve Order</a>
                      @endif
                    </div>
                  </div>
                 </td>
               </tr>
             @endforeach
           </table>
                
            
          </form>
        </div>
    </div>
  </div>
</div>
@stop
