@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Stock(s) Request</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('portal.raw-material-request.create')}}" class="btn btn-sm btn-primary">Create New Request</a>
            </span>
        </div>
        <div class="card-body">
            @include('portal.components.notifications')
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                  <thead class="thead-dark">
                      <tr>
                          <th scope="col">Order ID</th>
                          <th scope="col">Quantity</th>
                          <th scope="col">Date Ordered</th>
                          <th scope="col">Status</th>
                          <th scope="col"></th>
                      </tr>
                  </thead>
                  <tbody>
                      @forelse($material_request as $data)
                      <tr>
                        <td>{{  $data->order_id }}</td>
                       
                        <td>{{ $material_request->count() }}</td>
                      
                        <td>{{  $data->created_at }}</td>
                        <td>
                            @if($data->product_request_status == "completed")
                              <span class="badge badge-success">{{ $data->product_request_status }}</span>
                            @elseif($data->product_request_status == "pending")
                              <span class="badge badge-warning">{{ $data->product_request_status }}</span>
                            @else
                            <span class="badge badge-info">{{ $data->product_request_status }}</span>
                            @endif
                        </td>
                        <td>
                  <div class="btn-group">
                    <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle form-control" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <div class="dropdown-menu">
                      @if ($data->product_request_status == "pending")
                      <a class="dropdown-item" href="{{route('portal.raw-material-request.edit',[$data->order_id])}}">View Details</a>
                      @else
                      <a class="dropdown-item" href="{{route('portal.raw-material-request.completed',[$data->id])}}">Recieve Order</a>
                      @endif
                    </div>
                  </div>
                </td>
                      </tr>
                      @empty
                      @endforelse
                  </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>


<!-- The Modal -->
<div class="modal fade" id="remove">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
         <div class="form-group">
          {!! Helper::confirmation_delete() !!}
         </div>
       </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

 @section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
     $("#removes").attr({"href" : btn.data('url')});
   })
  })
</script>
 @stop
