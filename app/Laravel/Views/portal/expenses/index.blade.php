@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Expenses</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Expenses- Record Data </h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.branch.index')}}" class="btn btn-primary">Back</a> -->
          </span>
          <span class="float-right mr-1">
            <a href="{{route('portal.expenses.create')}}" class="btn btn-outline-primary">Add New Expenses</a>
          </span>
        </div>
        <div class="card-body" >
          @include('portal.components.notifications')
          <div class="row">
            {{-- <div class="form-group col-lg-12 text-right " >
               <input type="text" id="search" name="search" class="form-control" style="width: 300px"> 
           </div>   --}} 

           <div class="col-lg-12">
                  <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%" >
                     <thead class="thead-dark">
              <th>Transaction Date</th>
              <th>supplier</th>
              <th>Tin</th>
              <th>Address</th>
              <th>Month</th>
              <th>Account title</th>
              <th>Description</th>
              <th>Convertion</th>
              <th>UOM</th>
              <th>Qty</th>
              {{-- <th>Unit Price</th> --}}
              <th>Line Amount</th>
              <th>Remarks</th>
              <th></th>
            </thead>

            <tbody>
              @foreach($expenses as $expense)
              <tr>
                <td><span>{{ date('Y M,d',strtotime($expense->transaction_date)) }}</span></td>
                <td><span>{{ $expense->supplier }}</span></td>
                <td><span>{{ $expense->tin }}</span></td>
                <td><span>{{ $expense->address }}</span></td>
                <td><span>{{ date('M',strtotime($expense->tin)) }}</span></td>
                <td><span>{{ $expense->title }}</span></td>
                <td><span>{{ $expense->description }}</span></td>
                <td><span>{{ $expense->convertion }}</span></td>
                <td><span>{{ $expense->uom }}</span></td>
                <td><span>{{ $expense->qty }}</span></td>
                <td><span>{{ $expense->line_amount }}</span></td>
                <td><span>{{ $expense->remarks }}</span></td>
                <td>
                   <div class="btn-group">
                        <button type="button" data-toggle="dropdown" class="btn btn-sm btn-outine-secondary dropdown-toggle form-control" aria-expanded="false">ACTIONS <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ route('portal.expenses.edit',[$expense->id]) }} ">Edit</a>
                          <a class="dropdown-item"  data-url="{{ route('portal.expenses.delete',[$expense->id]) }}" href="#delete" data-toggle="modal" data-role="delete">Delete</a>
                        </div>
                      </div>
                </td>
              </tr>
              @endforeach      
            </tbody>
              </table>

         </div>
          <div class="row">
          
       </div>


     </div>



   </div>
 </div>

</div>



<!-- The Modal -->


<div class="modal fade" id="delete">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group">
           {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
           {!!Helper::confirmation_delete() !!}
         </div>
       </div>
     </div>

     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      <a  class="btn btn-primary" href="#" data-id="" data-url="" id="del" >Delete</a>
    </div>

  </div>
</div>
</div>

{{-- end modal --}}


@stop



@section('page-styles')
<style type="text/css">
  
   .table tbody tr td{ vertical-align: middle; }

</style>
@endsection

@section('page-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('assets/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/datatables/datatables.script.min.js') }}"></script>
<script>
  $(document).ready(function(){
   
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    // alert(btn.data('url'))
    $("#del").attr({"href" : btn.data('url')});
  })



 })
</script>
@stop
