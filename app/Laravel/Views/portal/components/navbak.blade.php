
{{-- @if (!Auth::user()->type == "cashier") --}}
<div class="side-content-wrap">
  <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
    <ul class="navigation-left">


      {{-- store branch only --}}
      <li class="nav-item {{ Request::segment(1) == NULL ? 'active':'' }}" data-item="" >
        <a class="nav-item-hold" href="{{route('portal.index')}}">
          <i class="nav-icon i-Bar-Chart"></i>
          <span class="nav-text">Dashboard</span>
        </a>
        <div class="triangle"></div>

      </li>
      
      @if(in_array(strtolower(Auth::user()->type),["store_admin"]))
      
    <li class="nav-item "  data-item="order">
        <a class="nav-item-hold" href="{{route('portal.purchase-order-branch.index')}}">
          <i class="nav-icon i-Add-Cart"></i>
          <span class="nav-text">Order</span>
        </a>
        <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(1) == 'purchase-order-branch' ? 'active':'' }}" data-item=""  >
        <a class="nav-item-hold" href="{{route('portal.purchase-order-branch.index')}}">
          <i class="nav-icon i-Add-Cart"></i>
          <span class="nav-text">Purchase Order</span>
        </a>
        <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(1) == 'order-list-branch' ? 'active':'' }}" data-item="">
        <a class="nav-item-hold" href="{{route('portal.order-list-branch.index')}}">
          <i class="nav-icon i-Cart-Quantity"></i>
          <span class="nav-text">Order List </span>
          
        </a>
        <div class="triangle"></div>
      </li>
      
      <li class="nav-item {{ Request::segment(1) == 'product' ? 'active':'' }}" data-item="">
        <a class="nav-item-hold" href="{{ route('portal.product.index') }}">
          <i class="nav-icon i-Bag-Items"></i>
          <span class="nav-text">Product(s)</span>
        </a>
        <div class="triangle"></div>
      </li>
      <li class="nav-item {{ Request::segment(1) == 'barcode' ? 'active':'' }} " data-item="">
        <a class="nav-item-hold" href="{{route('portal.barcode.index')}}">
          <i class="nav-icon i-Billing"></i>
          <span class="nav-text">Barcodes</span>
        </a>
        <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(1) == 'raw-material' ? 'active':'' }} " data-item="">
        <a class="nav-item-hold" href="{{route('portal.raw-material.index')}}">
          <i class="nav-icon i-Billing"></i>
          <span class="nav-text">Raw Material Inventory</span>
        </a>
        <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(1) == 'purchase-order-branch' ? 'active':'' }}" data-item="">
        <a class="nav-item-hold" href="{{route('portal.transaction.index')}}">
          <i class="nav-icon i-Add-Cart"></i>
          <span class="nav-text">Transaction</span>
        </a>
        <div class="triangle"></div>
      </li>
      
      <li class="nav-item {{ Request::segment(1) == 'terminal' ? 'active':'' }}" data-item="">
        <a class="nav-item-hold" href="{{ route('portal.terminal.index') }}">
          <i class="nav-icon i-Settings-Window"></i>
          <span class="nav-text">Terminal</span>
        </a>
        <div class="triangle"></div>
      </li>
      

      <li class="nav-item {{ Request::segment(1) == 'administrator' ? 'active':'' }}"  data-item="">
        <a class="nav-item-hold" href="{{route('portal.administrator.index')}}">
          <i class="nav-icon i-Administrator"></i>
          <span class="nav-text">Administrators</span>
        </a>
       
      </li>



      {{-- <li class="nav-item">
          <a class="nav-item-hold" href="{{route('portal.raw-material.index')}}">
              <i class="nav-icon i-Bag-Items"></i>
              <span class="nav-text">Store Stock(s)</span>
          </a>
          <div class="triangle"></div>
        </li> --}}
        @endif
        {{-- end of store branch user only--}}
        

        {{-- admin user only  --}}

        @if(in_array(strtolower(Auth::user()->type),["super_user"])) 
        
        <li class="nav-item {{ Request::segment(1) == "supplier" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('portal.supplier.index')}}">
            <i class="nav-icon i-Add-UserStar"></i>
            <span class="nav-text">Suppliers</span>
          </a>
          <div class="triangle"></div>
        </li>
        <li class="nav-item  {{ Request::segment(1) == "branch" ? 'active' : '' }} ">
        <a class="nav-item-hold" href="{{route('portal.branch.index')}}">
          <i class="nav-icon i-Billing"></i>
          <span class="nav-text">Branch</span>
        </a>
        <div class="triangle"></div>
      </li>
        
       <li class="nav-item {{ Request::segment(1) == "purchase-order-supplier" ? 'active' : '' }}">
         <a class="nav-item-hold" href="{{route('portal.purchase-order-supplier.index')}}">
           <i class="nav-icon i-Add-Cart"></i>
           <span class="nav-text">Purchase Order</span>
         </a>
         <div class="triangle"></div>
       </li>
       <li class="nav-item {{ Request::segment(1) == "order-list" ? 'active' : '' }}" >
         <a class="nav-item-hold" href="{{route('portal.order_list.index')}}" >
           <i class="nav-icon i-Cart-Quantity"></i>
           <span class="nav-text">Order List</span>
         </a>
         <div class="triangle"></div>
       </li>
       <li class="nav-item {{ Request::segment(1) == "stock" || Request::segment(1) == "stock" || Request::segment(1) == "quick-report" ? 'active' : '' }}">
         <a class="nav-item-hold" href="{{route('portal.stock.index')}}">
           <i class="nav-icon i-Clothing-Store"></i>
           <span class="nav-text">Inventory</span>
         </a>
         <div class="triangle"></div>
       </li>
      
        <li class="nav-item {{ Request::segment(1) == "stock-request" ? 'active' : '' }} " >
          <a class="nav-item-hold" href="{{route('portal.stock-request.index')}}">
            <i class="nav-icon i-Shop-3"></i>
            <span class="nav-text">Stock Request(s)</span>
          </a>
          <div class="triangle"></div>
        </li>
        <li class="nav-item {{ Request::segment(1) == "deliver" ? 'active' : '' }} " >
          <a class="nav-item-hold" href="{{route('portal.deliver.index')}}">
            <i class="nav-icon i-Shop-3"></i>
            <span class="nav-text">Delivery</span>
          </a>
          <div class="triangle"></div>
        </li>

        <li class="nav-item {{ Request::segment(1) == "administrator" ? 'active' : '' }}  " >
          <a class="nav-item-hold" href="{{route('portal.administrator.index')}}">
            <i class="nav-icon i-Administrator"></i>
            <span class="nav-text">Administrators</span>
          </a>
          <div class="triangle"></div>
        </li>


      </ul>
    </div>


      
    {{-- <div class="sidebar-overlay"></div> --}}
    @endif
 <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                <!-- Submenu Dashboards-->
                <ul class="childNav" data-parent="order" style="display: none;">
                    <li class="nav-item"><a href="dashboard1.html"><i class="nav-icon i-Clock-3"></i><span class="item-name">Purchase Order</span></a></li>
                    <li class="nav-item"><a href="dashboard2.html"><i class="nav-icon i-Clock-4"></i><span class="item-name">Order List</span></a></li>
                    <li class="nav-item"><a href="dashboard3.html"><i class="nav-icon i-Over-Time"></i><span class="item-name">Version 3</span></a></li>
                    <li class="nav-item"><a href="dashboard4.html"><i class="nav-icon i-Clock"></i><span class="item-name">Version 4</span></a></li>
                </ul>
      </div>
    {{-- end of super_user only --}}
