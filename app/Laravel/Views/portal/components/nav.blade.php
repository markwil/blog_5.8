
<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none ps ps--active-y" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
     <li class="nav-item {{ Request::segment(1) == NULL ? 'active':'' }}" data-item="" >
        <a class="nav-item-hold" href="{{route('portal.index')}}">
          <i class="nav-icon i-Bar-Chart"></i>
          <span class="nav-text">Dashboard</span>
        </a>
        <div class="triangle"></div>

 @if(in_array(strtolower(Auth::user()->type),["store_admin"]))
   </li>
         <li class="nav-item "  data-item="order">
            <a class="nav-item-hold" href="{{route('portal.purchase_order_branch.index')}}">
              <i class="nav-icon i-Add-Cart"></i>
              <span class="nav-text">Order</span> 
          </a>
          <div class="triangle"></div>
      </li>
<li class="nav-item {{ Request::segment(1) == 'product' ? 'active':'' }}" data-item="product">
    <a class="nav-item-hold" href="#">
      <i class="nav-icon i-Bag-Items"></i>
      <span class="nav-text">Product(s)</span>
  </a>
  <div class="triangle"></div>
</li>


<li class="nav-item {{ Request::segment(1) == 'raw-material' ? 'active':'' }} " data-item="">
    <a class="nav-item-hold" href="{{route('portal.raw-material.index')}}">
      <i class="nav-icon i-Billing"></i>
      <span class="nav-text">Raw Material Inventory</span>
  </a>
  <div class="triangle"></div>
</li>

<li class="nav-item {{ Request::segment(1) == 'purchase-order-branch' ? 'active':'' }}" data-item="">
    <a class="nav-item-hold" href="{{route('portal.transactions.today')}}">
      <i class="nav-icon i-Add-Cart"></i>
      <span class="nav-text">Transaction</span>
    </a>
  <div class="triangle"></div>
</li>
<li class="nav-item {{ Request::segment(1) == 'short-over' ? 'active':'' }}" data-item="">
    <a class="nav-item-hold" href="{{ route('portal.short_over_report.index') }}">
      <i class="nav-icon i-Settings-Window"></i>
      <span class="nav-text">Short & Over Report</span>
  </a>
  <div class="triangle"></div>
</li>

<li class="nav-item {{ Request::segment(1) == 'terminal' ? 'active':'' }}" data-item="">
    <a class="nav-item-hold" href="{{ route('portal.terminal.index') }}">
      <i class="nav-icon i-Settings-Window"></i>
      <span class="nav-text">Terminal</span>
  </a>
  <div class="triangle"></div>
</li>



<li class="nav-item {{ Request::segment(1) == 'administrator' ? 'active':'' }}"  data-item="administrator">
    <a class="nav-item-hold" href="{{route('portal.administrator.index')}}">
      <i class="nav-icon i-Administrator"></i>
      <span class="nav-text">Administrators</span>
  </a>

</li>

@endif


        @if(in_array(strtolower(Auth::user()->type),["super_user","accounting"])) 
        
        <li class="nav-item {{ Request::segment(1) == "supplier" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('portal.supplier.index')}}">
            <i class="nav-icon i-Add-UserStar"></i>
            <span class="nav-text">Suppliers</span>
          </a>
          <div class="triangle"></div>
        </li>
        <li class="nav-item  {{ Request::segment(1) == "branch" ? 'active' : '' }} ">
        <a class="nav-item-hold" href="{{route('portal.branch.index')}}">
          <i class="nav-icon i-Billing"></i>
          <span class="nav-text">Branch</span>
        </a>
        <div class="triangle"></div>
      </li>
        
       <li {{ auth()->user()->type == "super_user" ? "" : "hidden"  }}  class="nav-item {{ Request::segment(1) == "purchase-order-supplier" ? 'active' : '' }} " data-item="w-order">
         <a class="nav-item-hold" href="#">
           <i class="nav-icon i-Add-Cart"></i>
           <span class="nav-text">Orders</span>
         </a>
         <div class="triangle"></div>
       </li>
      
       <li class="nav-item {{ Request::segment(1) == "stock" || Request::segment(1) == "stock" || Request::segment(1) == "quick-report" ? 'active' : '' }}">
         <a class="nav-item-hold" href="{{route('portal.stock.index')}}">
           <i class="nav-icon i-Clothing-Store"></i>
           <span class="nav-text">Inventory</span>
         </a>
         <div class="triangle"></div>
       </li>
         <li {{ auth()->user()->type == "accounting" ? "" : "hidden"  }} class="nav-item {{ Request::segment(1) == "expenses" ? 'active' : '' }}  " data-item="expenses" >
          <a class="nav-item-hold" href="{{route('portal.expenses.index')}}">
            <i class="nav-icon i-Money"></i>
            <span class="nav-text">Expenses</span>
          </a>
          <div class="triangle"></div>
        </li>
        <li class="nav-item {{ Request::segment(1) == "administrator" ? 'active' : '' }}  " data-item="administrators" >
          <a class="nav-item-hold" href="{{route('portal.administrator.index')}}">
            <i class="nav-icon i-Administrator"></i>
            <span class="nav-text">Administrators</span>
          </a>
          <div class="triangle"></div>
        </li>
         <li hidden=""  class="nav-item {{ Request::segment(1) == "settings" ? 'active' : '' }}  " data-item="settings" >
          <a class="nav-item-hold" href="{{route('portal.administrator.index')}}">
            <i class="nav-icon i-Share"></i>
            <span class="nav-text">Settings</span>
          </a>
          <div class="triangle"></div>
        </li>

        {{-- accounting   --}}
        



        @endif
</ul>
<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 545px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 243px;"></div></div></div>
<div class="sidebar-left-secondary rtl-ps-none ps" data-perfect-scrollbar="" data-suppress-scroll-x="true">
    <!-- Submenu Order Store Admin-->
    <ul class="childNav" data-parent="order">
        <li class="nav-item"><a href="{{route('portal.purchase_order_branch.index')}}"><i class="nav-icon i-Add-Cart"></i><span class="item-name">Purchase Order</span></a></li>
        <li class="nav-item"><a href="{{route('portal.order_list_branch.index')}}"><i class="nav-icon i-Cart-Quantity"></i><span class="item-name">Order List</span></a></li>
        <li class="nav-item"><a href="{{ route('portal.approved-order.index') }}"><i class="nav-icon i-Over-Time"></i><span class="item-name">Approved Order</span></a></li>
        <li class="nav-item"><a href="{{ route('portal.deliver.index') }}"><i class="nav-icon i-Clock"></i><span class="item-name">Incoming Delivery</span></a></li>
    </ul>
 <!-- Submenu Products  Store Admin-->
    <ul class="childNav" data-parent="product">
        <li class="nav-item"><a href="{{ route('portal.product.index') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">Product List</span></a></li>
         <li class="nav-item"><a href="{{ route('portal.product_category.index') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">Product Category</span></a></li>
        <li class="nav-item"><a href="{{route('portal.barcode.index')}}"><i class="nav-icon  i-Billing"></i><span class="item-name">Barcodes</span></a></li>
    </ul>
    <ul class="childNav" data-parent="administrator">
        <li class="nav-item"><a href="{{ route('portal.administrator.index') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">User Account</span></a></li>
         <li class="nav-item"><a href="{{ route('portal.logs.index') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">Logs</span></a></li>
        
    </ul>

    {{-- WARE HOUSE --}}
     <ul class="childNav" data-parent="w-order" {{ auth()->user()->type == "accounting" ? "hidden": "" }}>
        <li class="nav-item"><a href="{{route('portal.purchase-order-supplier.index')}}"><i class="nav-icon i-Add-Cart"></i><span class="item-name">Purchase Order</span></a></li>
        <li class="nav-item"><a href="{{route('portal.order_list.index')}}"><i class="nav-icon i-Cart-Quantity"></i><span class="item-name">Order List</span></a></li>
        {{-- <li class="nav-item"><a href="dashboard3.html"><i class="nav-icon i-Over-Time"></i><span class="item-name">Approved Order</span></a></li> --}}
        <li class="nav-item"><a href="{{route('portal.deliver.index')}}"><i class="nav-icon i-Clock"></i><span class="item-name">Out for Deliver</span></a></li>
        <li class="nav-item"><a href="{{route('portal.stock-request.index')}}"><i class="nav-icon i-Clock"></i><span class="item-name">Stock Request</span></a></li>
    </ul>
        <ul class="childNav" data-parent="expenses" {{ in_array(auth()->user()->type,['accounting','super_user']) ? "":"hidden" }}>
        <li class="nav-item"><a href="{{route('portal.expense_title.index')}}"><i class="nav-icon i-Cart-Quantity"></i><span class="item-name">Account Title</span></a></li>
        {{-- <li class="nav-item"><a href="dashboard3.html"><i class="nav-icon i-Over-Time"></i><span class="item-name">Approved Order</span></a></li> --}}
        <li class="nav-item"><a href="{{route('portal.expenses.index')}}"><i class="nav-icon i-Clock"></i><span class="item-name">Expenses List</span></a></li>
        
    </ul>

    <ul class="childNav" data-parent="settings" {{ in_array(auth()->user()->type,['accounting','super_user']) ? "":"hidden" }}>
        <li class="nav-item"><a href="{{route('portal.expense_title.index')}}"><i class="nav-icon i-Cart-Quantity"></i><span class="item-name">Company Logo</span></a></li>
    </ul>

    <ul class="childNav" data-parent="administrators">
        <li class="nav-item"><a href="{{ route('portal.administrator.index') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">User Account</span></a></li>
         <li class="nav-item"><a href="{{ route('portal.logs.index') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">Transaction Logs</span></a></li>
          <li class="nav-item"><a href="{{ route('portal.logs.auth_logs') }}"><i class="nav-icon i-Bag-Items"></i><span class="item-name">Authentication Logs</span></a></li>
        
    </ul>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
        <div class="sidebar-overlay"></div>
    </div>