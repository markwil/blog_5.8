<div class="main-header">
   
    <div class="logo">
        <img src="{{asset('assets/images/ziilogo.png')}}" style="height: 50px;width: auto" alt="">
    </div>

    <div class="menu-toggle">
        <div></div>
              <div></div>
        <div></div>
    </div>
    
    <div class="d-flex justify-content-start" style="margin: auto"></div>

    <div class="header-part-right">
     <div class="dropdown">
                    <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(auth()->user()->unreadNotifications->count() > 0)
                         <span class="badge badge-danger">{{ auth()->user()->unreadNotifications->count() }}</span>
                        @endif
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                    <!-- Notification dropdown -->
                    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none ps" aria-labelledby="dropdownNotification" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                        @foreach(auth()->user()->notifications as $notification)
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span class=" mr-4" >{{ $notification->data['subject'] }}</span>
                                    <span class="badge badge-pill badge-primary ml-1 "></span>
                                    <span class="flex-grow-1"></span>
                                    <span class=" badge text-small text-muted ml-auto">{{ Helper::time_passed( $notification->created_at) }}</span>
                                </p>
                                <p class="badge text-small text-muted m-0"><a href="{{ $notification->data['url'] }}">{{ $notification->data['messages'] }}</a></p>
                                
                            </div>
                        </div>
                        @endforeach
                       
                    
                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                </div>

        <div>Hi <span class="text-primary">{{Auth::guard((session()->get('is_admin','no') == "yes" ?'admin' : 'partner'))->user()->name}}</span>, {{Helper::greet()}}!</div>
        
        <div class="dropdown">
            <div  class="user col align-self-end">
                <img src="{{asset('placeholder/user.png')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{Auth::guard((session()->get('is_admin','no') == "yes" ?'admin' : 'partner'))->user()->name}}
                    </div>
                    <a class="dropdown-item" href="{{route('portal.profile.edit_password')}}">Update Password</a>
                    <a class="dropdown-item" href="{{route('portal.logout')}}">Sign out</a>
                </div>
            </div>
        </div>
    </div>

</div>