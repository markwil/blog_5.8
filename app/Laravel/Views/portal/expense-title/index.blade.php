@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Expenses</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-8">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Expenses- Record Data </h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.branch.index')}}" class="btn btn-primary">Back</a> -->
          </span>
          <span class="float-right mr-1">
            <a href="#create" data-toggle="modal"  class="btn btn-outline-primary">Add New Account Title</a>
          </span>
        </div>
        <div class="card-body" >
          @include('portal.components.notifications')
          <div class="row">
               
           <div class="col-lg-12">
                  <table class="table table-bordered text-center" width="100%" >
                     <thead class="thead-dark">
              <th>Account Title</th>
              <th></th>
            </thead>

            <tbody>
              @foreach($account_title as $account)
              <tr>
                <td><span>{{ $account->account_title }}</span></td>
                <td>
                   <div class="btn-group">
                        <button type="button" data-toggle="dropdown" class="btn btn-sm btn-outine-secondary dropdown-toggle form-control" aria-expanded="false">ACTIONS <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="#edit" data-role="edit" data-toggle="modal" data-role="edit" data-title="{{ $account->account_title }}" data-id="{{ $account->id }}" >Edit</a>
                          <a class="dropdown-item"  data-url="{{ route('portal.expense_title.delete',[$account->id]) }}" href="#delete" data-toggle="modal" data-role="delete">Delete</a>
                        </div>
                      </div>
                </td>
              </tr>
              @endforeach      
            </tbody>
              </table>

         </div>
          <div class="row">
          
       </div>


     </div>



   </div>
 </div>

</div>



<!-- The Modal -->







<div class="modal fade" id="create">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Account Title</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <form action="" method="post">
        {{ csrf_field() }}
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group">
           <form action="" method="post">
             <div class="form-group">
               <label for="">Account Title</label>
               <input type="text" class="form-control" name="account_title" required="">
             </div>
           
         </div>
       </div>
     </div>


     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-outline-primary" >Add Account Title</button>
    </div>
</form>
  </div>
</div>
</div>


<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Account Title</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <form action="{{ route('portal.expense_title.update') }}" method="post">
        {{ csrf_field() }}
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group">
           <form action="" method="post">
             <div class="form-group">
               <label for="">Account Title</label>
               <input type="text" class="form-control" id="account_title" name="account_title" required="">
             </div>
             <input type="hidden" class="form-control" id="id" name="id" required="">
           
         </div>
       </div>
     </div>


     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-outline-primary" >Update</button>
    </div>
</form>
  </div>
</div>
</div>



<div class="modal fade" id="delete">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Account Title</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
     
      <div class="modal-body">
        <div class="col-md-12">
        {!!helper::confirmation_delete() !!}
       </div>
     </div>


     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
      <a class="btn btn-outline-primary"  data-url="" id="del" >Delete</a>
    </div>

  </div>
</div>
</div>

{{-- end modal --}}


@stop



@section('page-styles')
<style type="text/css">
  
   .table tbody tr td{ vertical-align: middle; }

</style>
@endsection

@section('page-scripts')
<script>
  $(document).ready(function(){
   
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#del").attr({"href" : btn.data('url')});
  })

   $(document).on('click','a[data-role=edit]',function(){
    var btn = $(this);
    $("#account_title").val(btn.data('title'));
    $("#id").val(btn.data('id'));
  })



 })
</script>
@stop
