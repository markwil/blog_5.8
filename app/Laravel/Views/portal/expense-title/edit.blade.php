@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Expenses Edit  Form</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Expenses - Edit Record Form </h3>
          <span class="float-right">
            
          </span>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <!-- <div class="row justify-content-center"> -->


    <!-- <div class="col-md-5 mt-5 mb-5"> -->
      <form action="" method="POST">
        {{ csrf_field() }}
                <div class="row">
              <div class="form-group col-lg-6">
                <label for="">Title</label>
                <select name="title" id="" class="form-control" >
                  <option value="Interest Receivables-Bank">Interest Receivables-Bank</option>
                  <option value="Interest Receivables-Credit Sales">Interest Receivables-Credit Sales</option>
                </select>
                @if($errors->first('title'))
                <p class="form-text text-danger">{{$errors->first('title')}}</p>
                @endif
              </div>
               <div class="form-group col-lg-6">
                <label for="">Transaction Date</label>
                <input type="date" id="" name="transaction_date" class="form-control" value="{{ old('transaction_date',$expense->transaction_date) }}">
               @if($errors->first('transaction_date'))
                <p class="form-text text-danger">{{$errors->first('transaction_date')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Supplier</label>
                <input type="text" id="" name="supplier" class="form-control" value="{{ old('supplier',$expense->supplier) }}">
                @if($errors->first('supplier'))
                <p class="form-text text-danger">{{$errors->first('supplier')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">TIN</label>
                <input type="text" id="" name="tin" class="form-control" value="{{ old('tin',$expense->tin) }}">
                @if($errors->first('tin'))
                <p class="form-text text-danger">{{$errors->first('tin')}}</p>
                @endif
              </div>
              
              <div class="form-group col-lg-6">
                <label for="">Description</label>
                <input type="text" id="" name="description" class="form-control" value="{{ old('description',$expense->description) }}">
                @if($errors->first('description'))
                <p class="form-text text-danger">{{$errors->first('description')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Convertion</label>
                <input type="text" id="" name="convertion" class="form-control" value="{{ old('convertion',$expense->convertion) }}">
                @if($errors->first('convertion'))
                <p class="form-text text-danger">{{$errors->first('convertion')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">UOM</label>
                <input type="text"  id="uom" name="uom" class="form-control" value="{{ old('uom',$expense->uom) }}">
                 @if($errors->first('uom'))
                <p class="form-text text-danger">{{$errors->first('uom')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Qty</label>
                <input type="text"  id="qty" name="qty" class="form-control" value="{{ old('qty',$expense->qty) }}">
                 @if($errors->first('qty'))
                <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                @endif
              </div>
             
              <div class="form-group col-lg-6">
                <label for="">Line Amount</label>
                <input type="text"  id="line_amount" name="line_amount" class="form-control" value="{{ old('line_amount',$expense->line_amount) }}">
                 @if($errors->first('line_amount'))
                <p class="form-text text-danger">{{$errors->first('line_amount')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Remarks</label>
                <input type="text"  id="remarks" name="remarks" class="form-control" value="{{ old('transaction_date',$expense->remarks) }}">
                 @if($errors->first('remarks'))
                <p class="form-text text-danger">{{$errors->first('remarks')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Address</label>
                <textarea  type="text" row="2" name="address"  class="form-control">{{ old('address',$expense->address )}}</textarea>
                 @if($errors->first('address'))
                <p class="form-text text-danger">{{$errors->first('address')}}</p>
                @endif
              </div>
               <div class="form-group col-lg-12 text-right">
                      <a href="{{ route('portal.expenses.index') }}" class="btn btn-outline-danger">Back</a>
                      <a class="btn btn-outline-primary" data-role="update" href="#update" data-toggle="modal">Update Record</a>
              </div>




  <!-- The Modal -->
  <div class="modal fade" id="update">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             <center><span>{{ Str::title('are you sure want to Update this data?') }}</span></center>
          
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
       
          {{ csrf_field()}}
        <button  type="submit" class="btn btn-outline-primary"  >Update</button>
  
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
</form>
</div>

           

          <!-- </div> -->
        </div>
      </div>
    </div>

  </div>



@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })


 })
</script>
@stop
