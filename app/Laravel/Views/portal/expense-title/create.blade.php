@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Expenses Create  Form</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Expenses - Add Record Form </h3>
          <span class="float-right">
            
          </span>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <!-- <div class="row justify-content-center"> -->


    <!-- <div class="col-md-5 mt-5 mb-5"> -->
      <form action="" method="POST">
        {{ csrf_field() }}
                <div class="row">
              <div class="form-group col-lg-6">
                <label for="">Title</label>
                <select name="title" id="" class="form-control" >
                  <option value="">select title</option>
                  <option value="Interest Receivables-Bank">Interest Receivables-Bank</option>
                  <option value="Interest Receivables-Credit Sales">Interest Receivables-Credit Sales</option>
                </select>
                @if($errors->first('title'))
                <p class="form-text text-danger">{{$errors->first('title')}}</p>
                @endif
              </div>
               <div class="form-group col-lg-6">
                <label for="">Transaction Date</label>
                <input type="date" id="" name="transaction_date" class="form-control">
               @if($errors->first('transaction_date'))
                <p class="form-text text-danger">{{$errors->first('transaction_date')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Supplier</label>
                <input type="text" id="" name="supplier" class="form-control">
                @if($errors->first('supplier'))
                <p class="form-text text-danger">{{$errors->first('supplier')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">TIN</label>
                <input type="text" id="" name="tin" class="form-control">
                @if($errors->first('tin'))
                <p class="form-text text-danger">{{$errors->first('tin')}}</p>
                @endif
              </div>
              
              <div class="form-group col-lg-6">
                <label for="">Description</label>
                <input type="text" id="" name="description" class="form-control">
                @if($errors->first('description'))
                <p class="form-text text-danger">{{$errors->first('description')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Convertion</label>
                <input type="text" id="" name="convertion" class="form-control">
                @if($errors->first('convertion'))
                <p class="form-text text-danger">{{$errors->first('convertion')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">UOM</label>
                <input type="text"  id="uom" name="uom" class="form-control">
                 @if($errors->first('uom'))
                <p class="form-text text-danger">{{$errors->first('uom')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Qty</label>
                <input type="text"  id="qty" name="qty" class="form-control">
                 @if($errors->first('qty'))
                <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                @endif
              </div>
             
              <div class="form-group col-lg-6">
                <label for="">Line Amount</label>
                <input type="text"  id="line_amount" name="line_amount" class="form-control">
                 @if($errors->first('line_amount'))
                <p class="form-text text-danger">{{$errors->first('line_amount')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Remarks</label>
                <input type="text"  id="remarks" name="remarks" class="form-control">
                 @if($errors->first('remarks'))
                <p class="form-text text-danger">{{$errors->first('remarks')}}</p>
                @endif
              </div>
              <div class="form-group col-lg-6">
                <label for="">Address</label>
                <textarea  type="text" row="2" name="address" class="form-control"></textarea>
                 @if($errors->first('address'))
                <p class="form-text text-danger">{{$errors->first('address')}}</p>
                @endif
              </div>
               <div class="form-group col-lg-12 text-right">
                      <a href="{{ route('portal.expenses.index') }}" class="btn btn-outline-danger">Back</a>
                      <button type="submit" class="btn btn-outline-primary">Add Record</button>
              </div>

</div>

            </form>

          <!-- </div> -->
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })


 })
</script>
@stop
