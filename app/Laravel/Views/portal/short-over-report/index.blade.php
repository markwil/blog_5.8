@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Transaction</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="form-group">
      <label for="" style="color: #fff">&nbsp;-</label>
      <div class="row">
      <div class="col-md-6 text-left">
        {{-- <div class="btn-group">
          <a href="{{ route('portal.transactions.today') }}" class="btn btn-outline-secondary">Today</a>
          <a href="{{ route('portal.transactions.week') }}" class="btn btn-outline-secondary btn-remove">This Week</a>
          <a href="{{ route('portal.transactions.month') }}" class="btn btn-outline-secondary btn-remove">This Month</a>
        </div> --}}
      </div>
      <div class="col-md-6 text-right">
        <form method="post">
          {{ csrf_field() }}
        <button type="submit" class="btn btn-outline-primary btn-remove">Export to Pdf</button>
        </form>
      </div>
    </div>
      

    </div>
  </div>


  <div class="col-md-12">
    <div class="row">
        {{-- <div class="table-responsive"> --}}
@foreach($short_over as $data)

          <div class="col-lg-4">
            <div class="table table-responsive">
              <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
              <thead class="thead-dark">
              <tr>
                <td>Date</td> <td> {{ Helper::date_only($data->start_shift) }}</td>
              </tr>
              <tr>
                <td>Cash Sales</td> <td>{{ $data->total }}</td>
              </tr>
              <tr>
                <td>Representation</td><td>-</td>
              </tr>
              <tr>
                <td>Sales Discount</td><td>{{ $data->discount }}</td>
              </tr>
              <tr>
                <td>Total Sales</td><td>{{ $data->total }}</td>
              </tr>
              <tr>
                <td>Misc. Income</td> <td>-</td>
              </tr>
              <tr>
                <td>Cash in Bank</td><td>-</td>
              </tr>
              <tr>
                <td>Short and Over</td><td>-</td>
              </tr>

            </thead>


          </table>
        </div>
      </div> 
        @endforeach
     @foreach($short_over as $data)

          <div class="col-lg-4">
            <div class="table table-responsive">
              <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
                
              <thead>
                <tr>
                <th colspan="3" style="text-align: center;">AM SHIFT</th>
                </tr>
              <tr>
                <th colspan="3" style="text-align: center;">CASH BREAKDOWN SUMMARY</th>
              </tr>
              <tr>
                <th>DENOMINATION</th>
                <th>No. of Pieces</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tr>
              <td>1000</td> <td>{{ $data->one_thousand }}</td> <td>{{$data->one_thousand * 1000}} </td>
            </tr>
            <tr>
              <td>500</td><td>{{ $data->five_hundred }}</td> <td>{{$data->five_hundred * 500}} </td>
            </tr>
            <tr>
               <td>200</td><td>{{ $data->two_hundred }}</td> <td>{{$data->two_hundred * 200}} </td>
            </tr>
             <tr>
               <td>100</td><td>{{ $data->one_hundred }}</td> <td>{{$data->one_hundred * 100}} </td>
             </tr>
              <tr>
                 <td>50</td><td>{{ $data->fifty_peso }}</td> <td>{{$data->fifty_peso * 50}} </td>
              </tr>
             <tr>
               <td>20</td><td>{{ $data->twenty_peso }}</td> <td>{{$data->twenty_peso * 20}} </td>
             </tr>
              <tr>
                <td>10</td><td>{{ $data->ten_peso }}</td> <td>{{$data->ten_peso * 10}} </td>
              </tr>
              <tr>
                <td>5</td><td>{{ $data->five_peso }}</td> <td> {{$data->five_peso * 5}} </td>
              </tr>
              <tr>
                <td>1</td><td>{{ $data->one_peso }}</td> <td> {{$data->one_peso * 1}} </td>
              </tr>

              <tr>
                <td>Total</td><td></td> <td>{{ number_format($data->denomination_total,2)  }} </td>
              </tr>
              
            </tr>


          </table>
        </div>
      </div> 
        @endforeach
    </div>
</div>
  </div>



  <!-- The Modal -->
  <div class="modal fade" id="marked_up">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             <input type="number" name="marked_up" step="any"  id="marked" class="form-control" >
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" href="#" data-id="" id="removes" > Update </button>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop
@section('page-styles')
<style type="text/css">
  .table tbody tr td {padding: 2px;vertical-align: middle;text-align: center;}
</style>
@stop

@section('page-scripts')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('assets/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/datatables/datatables.script.min.js') }}"></script> --}}
<script>
  $(document).ready(function(){
      $(document).on('click','a[data-role=edit]',function(){
        var marked_up = $(this).data('mark');
        $('#marked').val(marked_up)
      })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
