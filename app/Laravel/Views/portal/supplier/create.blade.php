@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Supplier</h1>
  <ul>
    <li>Add New Supplier</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
      <div class="card-header">
        <h3 class="w-50 float-left card-title m-0">Supplier - Add Record Form</h3>
      </div>
      <div class="card-body">
        @include('portal.components.notifications')
        <form action="" method="POST" id="supplier_form">
          {!!csrf_field()!!}
          <div class="form-group">

            <label for="input_name"><b>Supplier Name</b></label>
            <input type="text" class="form-control" id="name" placeholder="Enter Supplier Name" value="{{old('name')}}" name="name">
            <p class="form-text text-danger" id="names"></p>
            @if($errors->first('name'))
            <p class="form-text text-danger">{{$errors->first('name')}}</p>
            @endif
          </div>
           <div class="form-group">

            <label for="input_name"><b>Email Address</b></label>
            <input type="email" class="form-control" id="email" placeholder="Enter Email Address" value="{{old('email')}}" name="email">
            <p class="form-text text-danger" id="emails"></p>
            @if($errors->first('email'))
            <p class="form-text text-danger">{{$errors->first('email')}}</p>
            @endif
          </div>

          <div class="form-group">
            <label for="contact"><b>Contact Number</b></label>
            <input type="text" class="form-control" id="contact" placeholder="Enter Contact Number" value="{{old('contact')}}" name="contact">
            @if($errors->first('contact'))
            <p class="form-text text-danger">{{$errors->first('contact')}}</p>
            @endif
          </div>

          <div class="form-group">
            <label for="address"><b>Address</b></label>
            {{-- <input type="text" class="form-control" id="address" name="address" placeholder="enter address" value="{{old('address')}}" name="value"> --}}
            <textarea class="form-control" id="address" name="address" value="{{old('address')}}" >
              
            </textarea>
            @if($errors->first('address'))
            <p class="form-text text-danger">{{$errors->first('address')}}</p>
            @endif
          </div>

          <div class="form-group">
            <a  href="{{ route('portal.supplier.index') }}" class="btn  btn-outline-info">Go Back to Supplier List</a>
              <button type="submit" id="submit" class="btn  btn-outline-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop


@section('page-scripts')


<script type="text/javascript">

  $(document).ready(function(){

    
    })
  })
</script>
@stop
