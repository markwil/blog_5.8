@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb d-flex justify-content-between">
  <h1>Supplier List</h1>
   <a href="{{route('portal.supplier.create')}}" class="btn p-2 pr-3 pl-3 btn-outline-primary"><i class="i-Add-User"></i> <span>Add New Supplier</span></a>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0  p-2">Record Data </h3>
                </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">



            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="text-uppercase thead-dark">
                  <tr>
                    <th scope="col">Supplier Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact Number</th>
                    <th scope="col">Address</th>
                    <th scope="col">Status</th>
                    <th scope="col"></th>
                    
                  </tr>
                </thead>
                <tbody>
                  @forelse($suppliers as $data)
                  <tr>
                    <td><span>{{ strtoupper($data->name) }}</span></td>
                    <td><span>{{ strtoupper($data->email) }}</span></td>
                    <td><span>{{ strtoupper($data->contact) }}</span></td>
                    <td><span>{{ strtoupper($data->address) }}</span></td>
                    <td><span>{!! Helper::supplier_status($data->status) !!}</span></td>
                    <td>
                      <div class="btn-group">
                        <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle form-control" aria-expanded="false">ACTIONS <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('portal.supplier.edit',[$data->id])}}">Edit Details</a>
                          <a class="dropdown-item" href="#status" data-role="update" data-toggle="modal" data-status="{{ $data->status == 1 ? 'Deactivate' : 'Activate' }}"  data-url="{{ route('portal.supplier.update-status',[$data->id, $data->status == 1 ? '0' : '1']) }}">{{ $data->status == 1 ? "Deactivate" : "Activate" }}</a>
                          {{-- <a class="dropdown-item" data-role="delete" href="#remove" data-url="{{ route('portal.supplier.destroy',[$data->id]) }}" data-toggle="modal">Deactivate Supplier</a> --}}
                          <a class="dropdown-item" href="{{route('portal.supplier-product.create',[$data->id])}}">View Product</a>

                        </div>
                      </div>
                    </td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="4">no data found</td>
                  </tr>
                  @endforelse()
                </tbody>
              </table>

            </div>

          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}

<!-- The Modal -->
  <div class="modal fade" id="status">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             <span>Are you sure want to </span><span id="message"></span><span>?</span>
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="stat" style="color:#fff" >Update</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })

 $(document).on('click','a[data-role=update]',function(){
    var btn = $(this);
    $('#message').text(btn.data('status'))
    $("#stat").attr({"href" : btn.data('url')});
  })

 })
</script>
@stop

@section('page-styles')
{{-- <style type="text/css">
.table-bordered th, .table-bordered td {
  border: none !important;
}
</style> --}}
@stop
