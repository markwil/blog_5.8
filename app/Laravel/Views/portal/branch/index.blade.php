@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Branch List</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data </h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.branch.index')}}" class="btn btn-primary">Back</a> -->
          </span>
          <span class="float-right mr-1">
            <a href="{{route('portal.branch.create')}}" class="btn btn-outline-primary">Add New Branch</a>
          </span>
        </div>
        <div class="card-body" >
          @include('portal.components.notifications')


          <div class="row">

            <div class="table table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <th>Branch Name</th>
                  <th>Address</th>
                  <th>Contact</th>
                  <th>View</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  @foreach($branches as $data)
                  <tr>
                    <td><span>{{ $data->branch_name }}</span></td>
                    <td><span>{{ $data->branch_location }}</span></td>
                    <td><span>{{ $data->branch_contact }}</span></td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <a class="btn btn-outline-info" href="{{ $data->id == 1 ? route('portal.stock.index') :route('portal.store.index',[$data->id])}}" type="button">Stock(s)</a>
                        <a class="btn btn-outline-info" href="{{ $data->id == 1 ? route('portal.stock.index') :route('portal.transactions.index',[$data->id])}}" type="button">Transactions</a>
                        <a class="btn btn-outline-info" href="{{ $data->id == 1 ? route('portal.stock.index') :route('portal.transactions.index',[$data->id])}}" type="button">Short and Over</a>
                      </div>
                    </td>
                    <td>
                      <a class="btn btn-outline-primary-uppercase" style="font-size:12px" href="{{route('portal.branch.edit',[$data->id])}}">Edit</a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          
      </div>


    </div>



  </div>
</div>

</div>



<!-- The Modal -->
<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit/Update</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group" hidden>

           <input type="text" name="id" id="id" value="" required class="form-control">
         </div>
         <div class="form-group">
          <label for="">Branch Name</label>
          <input type="text" name="branch_name" id="branch_name" value="" required class="form-control">
        </div>
        <div class="form-group">
          <label for="">Branch address/Location</label>
          <input type="text" name="branch_location" id="branch_location" value="" required class="form-control">
        </div>
        <div class="form-group">
         <label for="">Branch Contact</label>
         <input type="text" name="branch_contact" id="branch_contact" value="" required class="form-control">
       </div>
     </div>
   </div>

   <!-- Modal footer -->
   <div class="modal-footer" style="background:#ddd">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
    <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Update</a>
  </div>

</div>
</div>
</div>

<div class="modal fade" id="remove">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group">
           {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
           {!!Helper::confirmation_delete() !!}
         </div>
       </div>
     </div>

     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
    </div>

  </div>
</div>
</div>

{{-- end modal --}}


@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
   $(document).on('click','a[data-role=edit]',function(){
     var id, name,contact,location
     var btn = $(this);
     id = btn.data('id');
     name = btn.data('name')
     location = btn.data('location')
     contact = btn.data('contact')

     $('#id').val(id);
     $('#branch_name').val(name);
     $('#branch_location').val(location);
     $('#branch_contact').val(contact);


   })

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })

   $("#ingredients_container")
   .delegate(".btn-add",'click',function(){
    var item = $(this).parents('.item-ingredients').html();

    var to_append = '<div class="row item-ingredients">'+item+'</div>';

    $("#ingredients_container").append(to_append)

  })
   .delegate(".btn-remove","click",function(){
    var item = $(this).parents('.item-ingredients');
    item.fadeOut(500,function(){
      $(this).remove();
    })
  });
 })
</script>
@stop
