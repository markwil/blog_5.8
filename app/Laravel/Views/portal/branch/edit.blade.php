@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Branch</h1>
  <ul>
      <li>Edit Branch</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Branch - Edit Record Form</h3>
            <span class="float-right">
             
            </span>
        </div>

        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {{ csrf_field() }}
                    <div class="form-group">
                      <label for="branch_name">Branch Name</label>
                      <input type="text" name="branch_name" value="{{old('branch_name',$data->branch_name)}}" class="form-control" >
                      @if($errors->first('branch_name'))
                      <p class="form-text text-danger">{{$errors->first('branch_name')}}</p>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="branch_name">Branch Location/Address</label>
                      <input type="text" name="branch_location" value="{{old('branch_location',$data->branch_location)}}" class="form-control" >
                      @if($errors->first('branch_location'))
                      <p class="form-text text-danger">{{$errors->first('branch_location')}}</p>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="branch_name">Contact</label>
                      <input type="text" name="branch_contact" value="{{old('branch_contact',$data->branch_contact)}}" class="form-control" >
                      @if($errors->first('branch_contact'))
                      <p class="form-text text-danger">{{$errors->first('branch_contact')}}</p>
                      @endif
                    </div>


                    <div class="form-group">
                       <a href="{{route('portal.branch.index')}}" class="btn btn-outline-primary">Back to Branch List</a>
                      <button type="submit" class="btn btn-outline-primary">Update</button>
                    </div>
                  <!-- </div> -->
                </form>
        </div>
    </div>
  </div>
</div>
@stop
