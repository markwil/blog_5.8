@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Stock(s) Inventory</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <div class="row">
            <div class="col-lg-6">
              <h3 class="card-title m-0">Record Data </h3>
            </div> 
            <div class="col-lg-6 text-right">
              <span class="mr-1">


                <a href="{{route('portal.store.index',[Request::segment(2)])}}" class="btn btn-primary">Back</a>

              </span>
            </div>
          </div>
        </div>
        <div class="form-group">

        </div>
        <div class="card-body" >
          @include('portal.components.notifications')
          <table class="table table-bordered text-center">
            <thead class="thead-dark">
              <tr>

                <th scope="col">Quantity</th>
                <th scope="col">UOM</th>
                <th scope="col">Request Date</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pending_request as $data)

              <tr>
                <td>{{ $data->product_qty }}</td>
                <td>{{ $data->product_unit }}</td>
                <td>{{ $data->created_at }}</td>
                <td> <span class="badge badge-danger">{{ $data->product_request_status }}</span></td>

                <td>
                  <div class="btn-group">
                    <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle form-control" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{route('portal.supplier.edit',[$data->id])}}">Recieve Order</a>
                      
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          

        </div>



      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="recieve">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit/Update</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

        </div>

        <!-- Modal footer -->
        <div class="modal-footer" style="background:#ddd">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Update</a>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}


@stop

@section('page-scripts')
<script>
  $(document).ready(function(){


   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })

   $("#ingredients_container")
   .delegate(".btn-add",'click',function(){
    var item = $(this).parents('.item-ingredients').html();

    var to_append = '<div class="row item-ingredients">'+item+'</div>';

    $("#ingredients_container").append(to_append)

  })
   .delegate(".btn-remove","click",function(){
    var item = $(this).parents('.item-ingredients');
    item.fadeOut(500,function(){
      $(this).remove();
    })
  });
 })
</script>
@stop
