@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Stock Inventory Quick Report</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
        {{-- <div class="table-responsive"> --}}
          <table class="table table-bordered">
              <thead>
                <tr>
                <th colspan="5" ><center>Transaction Out</center></th>
            
              </tr>
               </thead>

            <thead class="thead-dark">
              <th>Transaction Type</th>
              <th>Date</th>
              <th>Product Code</th>
              <th>Product Description</th>
              <th>Quantity</th>
            </thead>
            <tbody>
              @forelse($stocks as $data)
              <tr>
                {{-- TRANSACTION IN --}}
                <td><span class="badge badge-success p-2">{{ $data->transaction_type }}</span></td>
                <td><span class="badge">{{ Helper::date_format($data->created_at) }}</span></td>
                <td><span class="badge">{{ $data->product_code}}</span></td>
                <td><span class="badge">{{ $data->description}}</span></td>
                <td><span class="badge">{{ $data->qty}}</span></td>
               
                </tr>
              @empty
              <tr>
                <td colspan="5">NO DATA FOUND</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        {{-- </div> --}}
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="marked_up">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             <input type="number" name="marked_up" step="any"  id="marked" class="form-control" >
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" > Update </button>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
      $(document).on('click','a[data-role=edit]',function(){
        var marked_up = $(this).data('mark');
        $('#marked').val(marked_up)
      })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
