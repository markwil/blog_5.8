@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Raw Material Stock Quick Report</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-6">
        {{-- <div class="table-responsive"> --}}
          <table class="table table-bordered">
              <thead>
                <tr>
                <th colspan="6" ><center>RAW MATERIAL   INVENTORY</center></th>
            
              </tr>
               </thead>

            <thead class="thead-dark">
              <th>Product Code</th>
              <th>Product Description</th>
              <th>Avaialble Stocks</th>
              <th>Remarks</th>
            </thead>
            <tbody>
              @forelse($stocks as $data)
              <tr>
                {{-- TRANSACTION IN --}}
               
                
                <td><span>{{ $data->product_code}}</span></td>
                <td><span>{{ $data->description}}</span></td>
                <td><span>{{ Helper::measurement_unit($data->total_qty - $data->total_out , $data->measurement) }} </span></td>
                <td>
              
                  @if( Helper::measurement_unit($data->total_qty - $data->total_out , $data->measurement) <= 10)
                  <span class="badge btn-danger">LOW</span>
                  @elseif(Helper::measurement_unit($data->total_qty - $data->total_out , $data->measurement) < 20)
                    <span class="badge btn-warning">MEDUIM</span>
                      @else
                       <span class="badge btn-success">HIGH</span>
                  @endif
                  </td>
                </tr>
              @empty
              <tr>
                <td colspan="6"><center>NO DATA FOUND</center></td>
              </tr>
              @endforelse
            </tbody>
          </table>
        {{-- </div> --}}
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="marked_up">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             <input type="number" name="marked_up" step="any"  id="marked" class="form-control" >
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" > Update </button>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
      $(document).on('click','a[data-role=edit]',function(){
        var marked_up = $(this).data('mark');
        $('#marked').val(marked_up)
      })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
