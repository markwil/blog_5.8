@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product Category</h1>
  <ul>
     
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Edit Product Category </h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_name"><b>Product Category</b></label>
                <input type="text" class="form-control" id="" placeholder="" value="{{old('category_name',$categories->category_name)}}" name="category_name">
                @if($errors->first('category_name'))
                <p class="form-text text-danger">{{$errors->first('category_name')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{ route('portal.product_category.index') }}"    class="btn  btn-outline-danger ">Back</a>
              <button type="submit" class="btn  btn-outline-primary">Update</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-scripts')
<script> 
    $('#back').on('click',function(){
      window.history.back()
    })
</script>
@stop