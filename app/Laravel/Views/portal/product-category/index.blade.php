@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product Category</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card o-hidden mb-12">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Product Category- Record Data </h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.branch.index')}}" class="btn btn-primary">Back</a> -->
          </span>
          <span class="float-right mr-1">
            <a href="{{route('portal.product_category.create')}}" class="btn btn-outline-primary">Add New Category</a>
          </span>
        </div>
        <div class="card-body" >
          @include('portal.components.notifications')
          <div class="row">
            

           <div class="col-lg-12">
            <div class="table table-responsive">
              <table class="table table-bordered text-center" width="100%" >
                     <thead class="thead-dark">
              <th>Category Name</th>
              <th></th>
            </thead>

            <tbody>
              @foreach($categories as $category)
              <tr>
                <td><span>{{ $category->category_name }}</span></td>
                
                <td>
                   <div class="btn-group">
                        <button type="button" data-toggle="dropdown" class="btn btn-sm btn-outine-secondary dropdown-toggle form-control" aria-expanded="false">ACTIONS <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{ route('portal.product_category.edit',[$category->id]) }} ">Edit</a>
                          <a class="dropdown-item"  data-url="{{ route('portal.product_category.delete',[$category->id]) }}" href="#delete" data-toggle="modal" data-role="delete">Delete</a>
                        </div>
                      </div>
                </td>
              </tr>
              @endforeach      
            </tbody>
              </table>
</div>
         </div>
          <div class="row">
          
       </div>


     </div>



   </div>
 </div>

</div>



<!-- The Modal -->


<div class="modal fade" id="delete">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group">
           {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
           {!!Helper::confirmation_delete() !!}
         </div>
       </div>
     </div>

     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      <a  class="btn btn-primary" href="#" data-id="" data-url="" id="del" >Delete</a>
    </div>

  </div>
</div>
</div>

{{-- end modal --}}


@stop



@section('page-styles')
<style type="text/css">
  
   .table tbody tr td{ vertical-align: middle; }

</style>
@endsection

@section('page-scripts')
<script>
  $(document).ready(function(){
   
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    // alert(btn.data('url'))
    $("#del").attr({"href" : btn.data('url')});
  })



 })
</script>
@stop
