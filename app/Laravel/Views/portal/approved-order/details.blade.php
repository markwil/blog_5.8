@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>ORDER LIST </h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Item List</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2"></span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {{ csrf_field() }}
          <div class="row justify-content-center">
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Product Code</th>
                    <th scope="col">Description</th>
                    <th scope="col">Quantity</th>
                    {{-- <th scope="col">Stock(s) on Hand</th> --}}
                    <th scope="col">Price</th>
                    <th scope="col">Subtotal</th>
           
                  </tr>
                </thead>
                <tbody>
                  @php
                  $total = 0;
                  @endphp
                  @foreach($cart as $data)
                  
                  <input type="hidden" value="{{ $data->cart_id }}" name="cart_id">
                  <tr>
                    <td>{{ $data->product_code }}</td>
                    <td><span class="badge">{{ $data->description }}</span></td>
                    <td><span class="badge">{{ $data->qty }}</span></td>
                    <td><span class="badge">{{ $data->price }}</span></td>
                    <td><span class="badge">{{ Helper::money_format($data->price * $data->qty)  }}</span></td>
                  </tr>
                  @php
                  $total += $data->price * $data->qty;
                  @endphp
                  @endforeach
                  <tr>
                    <td colspan="5" style="text-align:left;"><span class="badge p-2">TOTAL </span><span class="badge badge-success p-2" style="float: right;">{{ Helper::money_format($total) }}</span></td>
                   
                  </tr>
                </tbody>
              </table>
              <form action="" method="POST">
              <div class="form-group" hidden="">
                <a href="#approve" data-toggle="modal" class="btn btn-primary">Complete Transaction</a>
              </div>
              </form>
            </div>

          </div>
        </form>
        </div>
      </div>
    </div>

  </div>


<!-- The Modal -->
  <div class="modal fade" id="approve">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
 

  <!-- The Modal -->
  <div class="modal fade" id="quantity">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Quantity</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="{{ route('portal.order_list_details.update')}}" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
              <input type="hidden" name="id" id="id"  class="form-control">
              <input type="number" name="qty" id="qtys" value="" class="form-control">
           </div>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Update</button>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
    $(document).on('click','a[data-role=edit]',function(){
      var qtys = $(this).data('qty');
     
      $('#qtys').val(qtys);
      $('#id').val($(this).data('id'));
    })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
