@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>APPROVED ORDER LIST </h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Approved Order</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2">
            @if($supplier > 0)
             {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a> --}}
             {{-- <a href="#supplier" data-toggle="modal" class="btn btn-sm btn-primary">Recieve Order</a> --}}
            @endif
           
          </span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">
            <table class="table table-bordered text-center">
            <thead class="thead-dark">
            
            
             {{--  <th>Item Count</th> --}}
              <th>Order Date</th>
              <th>Total Amount Due</th>
              <th>Status</th>
              <th>Action</th>
            </thead>
            <tbody>
              @forelse($items as $data)
              <tr id="">
                <td data-target="product_name">{{ $data->updated_at == NULL ? Helper::date_format($data->created_at) :  Helper::date_format($data->updated_at) }}</td>

                <td data-target="description">{{ Helper::money_format($data->total_amount) }}</td>
                <td data-target="description">{!! Helper::status($data->status) !!}</td>
               
                <td>
      
                   <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-outline-info" {{ $data->status == 3 ? "hidden" : "" }} href="{{ route('portal.approved-order.details',[$data->cart_id]) }}" type="button">Details</a>
                    <a class="btn btn-outline-danger" href="#cancels" {{ $data->status == 3 ? "hidden" : "" }} data-role="cancel" data-url="{{ route('portal.approved-order.delete',[$data->id]) }}" data-toggle="modal" id="cancel" type="button">Cancel</a>

                    <a class="btn btn-outline-danger" {{ $data->status == 3 ? "" : "hidden" }} href="#remove" data-role="remove" data-url="{{ route('portal.approved-order.delete',[$data->id]) }}" data-toggle="modal" id="cancel" type="button">&times;</a>

                  </div>
                </td>
              </tr>
             
              @empty
              @endforelse
            </tbody>
          </table>
            

          </div>
        </div>
      </div>
    </div>

  </div>


 <!-- The Modal -->
  <div class="modal fade" id="cancels">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {{-- {!!Helper::confirmation_delete() !!} --}}
             Are you sure want to cancel this order ?
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-outline-primary" href="#" data-url="" id="canceled"  >Yes</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}


 <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {{-- {!!Helper::confirmation_delete() !!} --}}
             Are you sure want to remove this order ?
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-outline-primary" href="#" data-url="" id="removes"  >Yes</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
  <!-- The Modal -->
  <div class="modal fade" id="supplier">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Supplier List</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="{{ route('portal.order_list.show')}}" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
              <select name="supplier_id" id="supplier_id" required="" class="form-control">
                <option value="">--Choose Supplier--</option>
                @forelse($suppliers as $data)
                <option value="{{ $data->id }}"> {{ $data->name }}</option>
                @empty
                @endforelse 
              </select>
           </div>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a type="submit" class="btn btn-outline-primary" href="#" data-id="" data-url="" id="removes">Proceed</a>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>

  $(document).ready(function(){
   $(document).on('click','a[data-role=cancel]',function(){
    var btn = $(this);
    // alert(btn.data('url'))
    $("#canceled").attr({"href" : btn.data('url')});
  })

   $(document).on('click','a[data-role=remove]',function(){
    var btn = $(this);
    // alert(btn.data('url'))
    $("#removes").attr({"href" : btn.data('url')});
  })
 })

</script>
@stop
