@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Supplier</h1>
  <ul>
      <li>Edit Supplier</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Supplier</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_name"><b>Supplier Name</b></label>
                <input type="text" class="form-control" id="name" placeholder="" value="{{old('name',$supplier->name)}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="contact"><b>Contact Number</b></label>
                <input type="text" class="form-control" id="contact" placeholder="" value="{{old('contact',$supplier->contact)}}" name="contact">
                @if($errors->first('contact'))
                <p class="form-text text-danger">{{$errors->first('contact')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="address"><b>address</b></label>
                <input type="text" class="form-control" id="address" name="address" placeholder="enter phone/mobile number" value="{{old('address',$supplier->address)}}" name="value">
                @if($errors->first('address'))
                <p class="form-text text-danger">{{$errors->first('address')}}</p>
                @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn  btn-primary">Update</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop
