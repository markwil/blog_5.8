@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Terminal Form</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Terminal Record </h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.branch.index')}}" class="btn btn-primary">Back</a> -->
          </span>
          <span class="float-right mr-1">
            <a href="{{route('portal.terminal.create')}}" class="btn btn-outline-primary">Add New Terminal</a>
          </span>
        </div>
        <div class="card-body" >
          @include('portal.components.notifications')


<div class="row">

  <table class="table table-bordered">
    <thead class="thead-dark">
      <th>Branch</th>
      <th>Serialnumber</th>
      <th>Device Name</th>
    </thead>

  <tbody>
    @forelse($terminal_infos as $data)
    <tr>
      <td><span class="badge">{{ $data->branch_location }}</span></td>
      <td><span class="badge">{{ $data->serialnumber }}</span></td>
      <td><span class="badge">{{ $data->device_name }}</span></td>
    </tr>

    @empty
    @endforelse
       </tbody>
  </table>
         
  </div>

  
    </div>



      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="edit">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit/Update</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group" hidden>

             <input type="text" name="id" id="id" value="" required class="form-control">
           </div>
           <div class="form-group">
                <label for="">Branch Name</label>
             <input type="text" name="branch_name" id="branch_name" value="" required class="form-control">
           </div>
           <div class="form-group">
              <label for="">Branch address/Location</label>
             <input type="text" name="branch_location" id="branch_location" value="" required class="form-control">
           </div>
           <div class="form-group">
             <label for="">Branch Contact</label>
             <input type="text" name="branch_contact" id="branch_contact" value="" required class="form-control">
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Update</a>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="remove">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
         <div class="form-group">
           {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
           {!!Helper::confirmation_delete() !!}
         </div>
       </div>
     </div>

     <!-- Modal footer -->
     <div class="modal-footer" style="background:#ddd">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
    </div>

  </div>
</div>
</div>

{{-- end modal --}}


@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
   $(document).on('click','a[data-role=edit]',function(){
     var id, name,contact,location
     var btn = $(this);
     id = btn.data('id');
     name = btn.data('name')
     location = btn.data('location')
     contact = btn.data('contact')

     $('#id').val(id);
     $('#branch_name').val(name);
     $('#branch_location').val(location);
     $('#branch_contact').val(contact);


   })

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })

  $("#ingredients_container")
      .delegate(".btn-add",'click',function(){
          var item = $(this).parents('.item-ingredients').html();

          var to_append = '<div class="row item-ingredients">'+item+'</div>';

          $("#ingredients_container").append(to_append)

      })
      .delegate(".btn-remove","click",function(){
          var item = $(this).parents('.item-ingredients');
          item.fadeOut(500,function(){
              $(this).remove();
          })
      });
 })
</script>
@stop
