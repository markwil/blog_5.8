@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Administrators</h1>
  <ul>
      <li>Add New Account</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-6">
      <div class="card-header">
        <h3 class="w-50 float-left card-title m-0">Create New Administrator Account Form </h3>
       
      </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group has-error">
                <label for="input_name"><b>Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name')}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_username"><b>Username</b></label>
                <input type="text" class="form-control" id="input_username" placeholder="" value="{{old('username')}}" name="username">
                @if($errors->first('username'))
                <p class="form-text text-danger">{{$errors->first('username')}}</p>
                @endif
            </div>


            <div class="form-group">
                <label for="input_email"><b>Email</b></label>
                <input type="email" class="form-control" id="input_email" placeholder="" value="{{old('email')}}" name="email">
                @if($errors->first('email'))
                <p class="form-text text-danger">{{$errors->first('email')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_password"><b>Password</b></label>
                <input type="password" class="form-control" id="input_password" placeholder="" value="" name="password">
                @if($errors->first('password'))
                <p class="form-text text-danger">{{$errors->first('password')}}</p>
                @endif
            </div>
            

            <div class="form-group">
                <label for="input_password_confirmation"><b>Confirm Password</b></label>
                <input type="password" class="form-control" id="input_password_confirmation" placeholder="" value="" name="password_confirmation">
            </div>
            @if(auth()->user()->type == "super_user")
            <div class="form-group">
                <label for="input_type"><b>Type</b></label>
                {!!Form::select('type',$types,old('type'),['class' => "form-control",'id' => "input_type"])!!}
                @if($errors->first('type'))
                <p class="form-text text-danger">{{$errors->first('type')}}</p>
                @endif
            </div>

            @else
            <div class="form-group">
                <label for="input_type"><b>Type</b></label>
                <select name="type" id="type" class="form-control">
                  <option value="">-Choose Admin Type-</option>
                  <option value="store_admin">Admin</option>
                  <option {{ auth()->user()->type == "super_user" || auth()->user()->type == "accounting" ? "hidden" : ""  }} value="cashier">Cashier</option>
                </select>
                @if($errors->first('type'))
                <p class="form-text text-danger">{{$errors->first('type')}}</p>
                @endif
            </div>
            <input type="hidden" name="branch_id" value="{{ auth()->user()->branch_id }}">
            @endif

            <div class="form-group">
                <label for="input_status"><b>Status</b></label>
                {!!Form::select('status',$statuses,old('status'),['class' => "form-control",'id' => "input_status"])!!}
                @if($errors->first('status'))
                <p class="form-text text-danger">{{$errors->first('status')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_status"><b>Store Branch</b></label>
                    <select class="form-control" id="branch_id" name="branch_id">
                        <option value="">--select branch--</option>
                        @foreach($branches as $data)
                          <option value="{{ $data->id }} : {{ $data->branch_location}}">{{ $data->branch_location }}</option>
                        @endforeach
                    </select>
                  @if($errors->first('status'))
                  <p class="form-text text-danger">{{$errors->first('branch')}}</p>
                  @endif

                  
            </div>

            <div class="form-group">
              <a href="{{route('portal.administrator.index')}}" class="btn btn-secondary">Go back to Administrators List</a>
              <button type="submit" class="btn  btn-outline-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop
@section('page-scripts')

<script>
 
</script>
@endsection
