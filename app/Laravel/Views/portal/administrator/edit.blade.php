@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Administrators</h1>
  <ul>
      <li>Update Account</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Administrator Account Update Form</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group has-error">
                <label for="input_name"><b>Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name',$account->name)}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_username"><b>Username</b></label>
                <input type="text" class="form-control" id="input_username" placeholder="" value="{{old('username',$account->username)}}" name="username">
                @if($errors->first('username'))
                <p class="form-text text-danger">{{$errors->first('username')}}</p>
                @endif
            </div>


            <div class="form-group">
                <label for="input_email"><b>Email</b></label>
                <input type="email" class="form-control" id="input_email" placeholder="" value="{{old('email',$account->email)}}" name="email">
                @if($errors->first('email'))
                <p class="form-text text-danger">{{$errors->first('email')}}</p>
                @endif
            </div>

             <div class="form-group" {{ $account->type = "store_admin"  ? "" : "hidden" }}>
                <label for="pin_code"><b>Pincode</b></label>
                <input type="text" class="form-control" id="pincode" placeholder="" value="{{old('pin_code',$account->pin_code)}}" name="pin_code">
                @if($errors->first('pin_code'))
                <p class="form-text text-danger">{{$errors->first('pin_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="branch_location"><b>Branch Location</b></label>

                <select name="branch_location" id="" class="form-control">
                     <option value="{{ old('branch_location',$account->branch_location ) }}">{{ $account->branch_location }}</option>
                    @foreach($branch as $store)
                     <option value="{{$store->branch_location }}">{{ $store->branch_location }}</option>
                    @endforeach
                </select>
               {{--  <input type="text" class="form-control" id="branch_location" placeholder="" value="{{old('branch_location',$account->branch_location)}}" name="branch_location"> --}}
                @if($errors->first('branch_location'))
                <p class="form-text text-danger">{{$errors->first('branch_location')}}</p>
                @endif
            </div>

              <div class="form-group">
                <label for="input_type"><b>Type</b></label>
                {!!Form::select('type',$types,old('type',$account->type),['class' => "form-control",'id' => "input_type"])!!}
                @if($errors->first('type'))
                <p class="form-text text-danger">{{$errors->first('type')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_status"><b>Status</b></label>
                {!!Form::select('status',$statuses,old('status',$account->status),['class' => "form-control",'id' => "input_status"])!!}
                @if($errors->first('status'))
                <p class="form-text text-danger">{{$errors->first('status')}}</p>
                @endif
            </div>


            <div class="form-group">
              <a href="{{route('portal.administrator.index')}}" class="btn btn-secondary">Go back to Administrators List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop