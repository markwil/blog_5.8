@extends('portal._layouts.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-2"></div>
        <div class="col-md-8 col-lg-6 ml-5">
            <div style="height: 6px;background-color: #663399"></div>
            <div class="card o-hidden p-4" style="border-radius: 0px !important;">
                <div class="row">
                    
                   {{--  <div class="col-md-6">
                        <div class="p-4"> --}}
                            {{-- @if(session()->get('admin_login','no') == "yes")
                            <h4>Admin Portal</h4>
                            @else
                            <h4>Partner Portal</h4>
                            <p>Sales Tracker &amp; Barcode Generator.</p>
                            <h3><b class="text-primary">Work Smarter</b> <br>not <b class="text-danger"><strike>Hard</strike></b>.</h3>
                            @endif  --}}
                            
                            {{-- @if(session()->get('admin_login','no') == "no")
                            <p><a href="{{route('portal.admin_login')}}">[Admin Login]</a></p>
                            @else
                            <p><a href="{{route('portal.login')}}">[Go back to Partner Login]</a></p>
                            @endif --}}
                       {{--  </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div class="p-3">
                            <div class="auth-logo text-center mb-4">
                                <img src="{{asset('assets/images/logo.png')}}" class="h-50" alt="">
                            </div>

                            <h1 class="mb-4 text-center text-uppercase" style="font-size: 17px;">Sign In your Admin account</h1>
                            @include('portal.components.notifications')
                            <form action="" method="POST">
                                {!!csrf_field()!!}
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input id="username" class="form-control form-control" type="text" name="username" value="{{old('username')}}">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" class="form-control form-control" type="password" name="password">
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary mt-2 p-2 w-50 text-uppercase">Sign In</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-lg-3 col-md-2"></div>
    </div>
</div>

@include('sweetalert::alert')
@stop