@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Supplier</h1>
  <ul>
    <li>Add New Supplier Product</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
@include('portal.components.notifications')
<div class="row">

<div class="col-md-6">
  <div class="card o-hidden mb-6">
    <div class="card-header">
      <h3 class="w-50 float-left card-title m-0">Supplier Product</h3>
    </div>
    <div class="card-body">


      {{-- product registration --}}
      <form action="" method="POST" enctype="multipart/form-data" id="supplier_form">
        {!!csrf_field()!!}
        <div class="form-group">

          <input type="hidden" value="{{ Request::segment(3) }}" name="supplier_id">
          <label for="input_name"><b>Product Code</b></label>
          <input type="text" class="form-control" autocomplete="off" id="product_code" placeholder="Enter Product Code" value="{{old('product_code')}}" name="product_code">
          <p class="form-text text-danger" id="product_code"></p>
          @if($errors->first('product_code'))
          <p class="form-text text-danger">{{$errors->first('product_code')}}</p>
          @endif
        </div>

        <div class="form-group">
          <label for="description"><b> Item Description</b></label>
          <input type="text" class="form-control" id="description" placeholder="Enter Description" value="{{old('description')}}" name="description" autocomplete="off">
          @if($errors->first('description'))
          <p class="form-text text-danger">{{$errors->first('description')}}</p>
          @endif
        </div>
        <div class="form-group">
          <label for="description"><b>Measurement</b></label>
          <select name="measurment" id=""  class="form-control">
           <option value="">Choose Measurement</option>
           <option value="Kg">Kilogram</option>
           <option value="g">grams</option>
           <option value="L">Litre</option>
           <option value="oz">Oz</option>
           <option value="ml">ml</option>
           <option value="pcs">pieces</option>
           <option value="other">Other</option>
         </select>
         @if($errors->first('measurment'))
         <p class="form-text text-danger">{{$errors->first('measurment')}}</p>
         @endif
       </div>
       <div class="form-group">
        <label for="qty"><b>Qty</b></label>
        <input type="number" class="form-control" step="any" id="qty" placeholder="Enter qty" value="{{old('qty')}}" name="qty" >
        @if($errors->first('qty'))
        <p class="form-text text-danger">{{$errors->first('qty')}}</p>
        @endif
      </div>
         {{--  <div class="form-group">
            <label for="address"><b>Category</b></label>
              <select name="category_id" id="" class="form-control">
                <option value="">Choose Category</option>
                @foreach($categories as $data)
                <option value="{{ $data->id }}"> {{ $data->category_name  }}</option>
                @endforeach
              </select>
            @if($errors->first('price'))
            <p class="form-text text-danger">{{$errors->first('price')}}</p>
            @endif
          </div> --}}
          <div class="form-group">
            <label for="address"><b>Price</b></label>
            <input type="number" class="form-control" autocomplete="off" id="price" name="price" placeholder="Enter Price" value="{{old('price')}}" name="value">
            @if($errors->first('price'))
            <p class="form-text text-danger">{{$errors->first('price')}}</p>
            @endif
          </div>

          <div class="form-group">
            <label for="address"><b>Remarks</b></label>
            <input type="text" class="form-control" autocomplete="off" id="remarks" name="remarks" placeholder="Enter remarks" value="{{old('remarks')}}" name="value">
            @if($errors->first('remarks'))
            <p class="form-text text-danger">{{$errors->first('remarks')}}</p>
            @endif
          </div>

          <div class="form-group">
            <a   href="{{ route('portal.supplier.index') }}" id="back" class="btn  btn-primary">Back</a>
            <button type="submit" id="submit"  class="btn  btn-primary">Submit</button>
          </div>
        </form>

        {{-- end product registration --}}
      </div>
    </div>
  </div>

  {{-- Product table --}}




  <div class="col-md-6">
    <div class="card o-hidden mb-4">
      <div class="card-header">
        <h3 class="w-50 float-left card-title m-0">Supplier Product List</h3>
      </div>
      <div class="card-body">


        <div class="table-responsive">
          <table class="table table-bordered text-center">
            <thead class="text-uppercase">
              <tr>
                {{-- <th>Image</th> --}}
                <th scope="col">Product Code</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col"></th>

              </tr>
            </thead>
            <tbody>
              @forelse($products as $data)
              <tr>
                {{-- <td><img src="{{$data->directory}}/{{$data->filename}} " alt=""></td> --}}
                <td>{{ $data->product_code  }}</td>
                <td><span class="badge">{{ $data->description   }} {{ Helper::qty_format($data->qty) }} {{ $data->measurment }}</span></td>
                <td>{{ $data->price  }}</td>
                <td><a  class="btn btn-outline-danger action-delete" href="#" data-url="{{route('portal.supplier-product.destroy',[$data->id])}}" data-toggle="modal" data-target="#confirm-delete" title="Remove Record">&times;</a></td>
              </tr>
              @empty
              @endforelse
            </tbody>
          </table>

        </div>

      </div>
    </div>
  </div>
  {{--End of  Product table --}}
  <!-- The Modal -->
  <div class="modal fade" id="product_list">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Product List</h4>
          {{-- <button type="button" class="close" data
          -dismiss="modal">&times;</button> --}}
        </div>
        <form action="" method="POST">
          {{ csrf_field() }}
          <div class="modal-body">
            <div class="col-md-12">
             <div class="form-group">
               <input type="text" name="category_name" id="category" placeholder="Enter Category Name" class="form-control">
             </div>
           </div>
         </div>


         <!-- Modal footer -->
       {{-- <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete" data-url="">Delete Record</a>
      </div> --}}
    </form>
  </div>
</div>
</div>

{{-- end modal --}}
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop


@section('page-scripts')


<script type="text/javascript">

  $(document).ready(function(){

    $('#back').on('click',function(){
      history.back(-1)
    })
  })


  $(".action-delete").on("click",function(){
    var btn = $(this);
    $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    $("#btn-confirm-delete").attr({"data-url" : btn.data('url')});
    $('#reason').change();
  });

  $('#btn-confirm-delete').on('click', function() {
    $('.btn-link').hide();
    $('.btn-loading').button('loading');
    $('#target').submit();
  });

</script>
@stop
