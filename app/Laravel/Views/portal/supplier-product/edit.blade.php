@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Supplier</h1>
  <ul>
    <li>Add New Supplier</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
      <div class="card-header">
        <h3 class="w-50 float-left card-title m-0">Supplier Product</h3>
      </div>
      <div class="card-body">
        @include('portal.components.notifications')
        <form action="" method="POST" id="supplier_form">
          {!!csrf_field()!!}
          <div class="form-group">

            <label for="input_name"><b>Product Name</b></label>
            <input type="text" class="form-control" id="product_name" placeholder="enter supplier" value="{{old('product_name')}}" product_name="product_name">
            <p class="form-text text-danger" id="product_name"></p>
            @if($errors->first('product_name'))
            <p class="form-text text-danger">{{$errors->first('product_name')}}</p>
            @endif
          </div>

          <div class="form-group">
            <label for="description"><b>Descriptio</b></label>
            <input type="text" class="form-control" id="description" placeholder="enter description" value="{{old('description')}}" name="description">
            @if($errors->first('description'))
            <p class="form-text text-danger">{{$errors->first('description')}}</p>
            @endif
          </div>

          <div class="form-group">
            <label for="address"><b>Price</b></label>
            <input type="text" class="form-control" id="price" name="price" placeholder="enter price" value="{{old('price')}}" name="value">
            @if($errors->first('price'))
            <p class="form-text text-danger">{{$errors->first('price')}}</p>
            @endif
          </div>

          <div class="form-group">
            <button type="submit" id="submit" class="btn  btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop


@section('page-scripts')


<script type="text/javascript">

  $(document).ready(function(){

    
    })
  })
</script>
@stop
