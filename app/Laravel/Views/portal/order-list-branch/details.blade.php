@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>ORDER LIST</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-10">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Item List</h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2">

            {{-- @if($supplier > 0) --}}
             {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a> --}}
            @php
            $total = 0;
            @endphp
            @foreach($status as $data)
          
              @if($data->status == '0' || $data->status == '1' )
             @else
             <a href="{{ route( 'portal.order_list_branch.recieve' )}}"  class="btn btn-sm btn-primary">Recieve Deliver</a>
            @endif   
            @endforeach
           
            {{-- @endif --}}
           
          </span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')
  

          <div class="row">
          
          
                      @php 
                    $total = 0;
                  @endphp
                  @forelse($cart as $data)
                
                    <div class="col-lg-12 mb-2">
                      <div class="p-3" style="border:2px solid #f4f4f4">
                        <div class="row">
                          <div class="col-lg-2">
                             <img src="{{asset('assets/images/ziilogo.png')}}" style="height: 60px;width: auto" alt="">
                          </div>
                          <div class="col-lg-6">
                            <h6><strong>Product Name</strong>: {{ strtoupper($data->product_code) }}</h6>
                            <h6><strong>Description</strong>: {{ $data->description }}</h6>
                            <h6><strong>Item Price</strong>: ₱ {{ Helper::money_format($data->price) }}</h6>
                          </div>
                          <div class="col-lg-4 text-lg-right">
                            <h4> ₱ {{ Helper::money_format($data->price * $data->qty ) }}</h4>
                             <p>for {{ $data->qty }} {{ $data->qty == 1 ? 'piece' :'pieces' }} </p>
                             @foreach($status as $datas)
                              @if($datas->status == '0')
                             <a href="#quantity" class="p-1 btn btn-primary" data-toggle="modal" id="qty" data-role="edit" data-id="{{ $data->id }}" data-qty="{{ $data->qty }}">Edit Quantity</a>
                             @else
                             @endif   
                          </div>
                        </div>
                      </div>
                    </div>
                  
                    @php
                    $total += $data->price * $data->qty ;
                    @endphp
         
                    @endforeach
                    
                 
                  @php
                  // $total += $data->total_price;
                  @endphp
                  @empty
                
                  <h5 class="text-center">No Data Found</h5>
                  @endforelse
                  <div class="col-lg-12">
                    {{ $cart->links()  }}
                  </div>
                  <div class="col-lg-12">
                    <hr>
                  </div>
                      <div class="col-lg-6">
                        <h4>TOTAL</h4>
                      </div>
                      <div class="col-lg-6 text-right">
                        <h3>₱ {{ Helper::money_format($total) }}</h3>
                      </div>
                    </div>
                  </div>
          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="supplier">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Supplier List</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
              <select name="supplier_id" id="supplier_id" required="" class="form-control">
                <option value="">--Choose Supplier--</option>
                @forelse($suppliers as $data)
                <option value="{{ $data->id }}"> {{ $data->name }}</option>
                @empty
                @endforelse 
              </select>
           </div>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Proceed</button>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}

  <!-- The Modal -->
  <div class="modal fade" id="quantity">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Quantity</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="{{ route('portal.order_list_details.update')}}" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
              <input type="hidden" name="id" id="id"  class="form-control">
              <input type="number" name="qty" id="qtys" value="" class="form-control">
           </div>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Update</button>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
    $(document).on('click','a[data-role=edit]',function(){
      var qtys = $(this).data('qty');
     
      $('#qtys').val(qtys);
      $('#id').val($(this).data('id'));
    })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
