    @extends('portal._layouts.main')

  @section('breadcrumb')
  <div class="breadcrumb">
    {{-- <h1>SUPPLIER : {{ strtoupper($supplier_name->name) }}</h1> --}}
    {{-- <ul>
        <li><a href="">Dashboard</a></li>
        <li>Version 1</li>
      </ul> --}}
    </div>

    <div class="separator-breadcrumb border-top"></div>
    @stop

    @section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="card o-hidden mb-4">
          <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Transaction In</h3>
            <span class="float-right">
              <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
            </span>
            <span class="float-right mr-2">
              @if($supplier > 0)
               {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a> --}}
               {{-- <a href="#confirm"  data-toggle="modal" class="btn btn-sm btn-primary">Confirm Order</a> --}}
               {{-- <a href="{{ route('portal.order-list-details.index',[Request::segment(3)]) }}"  class="btn btn-sm btn-primary">Back To Order List</a> --}}
              @endif
             
            </span>

          </div>
          <div class="card-body">
            @include('portal.components.notifications')
  <form action="" method="POST">
            <div class="row">
         <div class="col-lg-12">
              <div class="table-responsive">
               
          
                  <hr>
                  <br>
                   <table class="table table-bordered">
                     <thead class="thead-dark">
                        {{-- <th>Order ID</th> --}}
                        <th><span>Order ID</span></th>
                        <th><span>Item Code</span></th>
                        <th><span>Description</span></th>
                        <th><span>Quantity</span></th>
                        <th><span>Price</span></th>
                        <th><span>Total</span></th>
                     </thead>
                     <tbody>
                      @php  $amount = 0; @endphp
                       @forelse($cart as $data)
                       
                       <tr>
                       {{-- <td>{{ $data->cart_id}}</td> --}}
                       <td hidden=""><input type="hidden"  value="{{ $data->measurement }}" name="measurement[]"></td>
                       <td><span>{{  str_pad($data->cart_id, 4, "0", STR_PAD_LEFT ) }}</span></td>
                       <td hidden="" =""><input type="hidden"  value="{{ $data->measurement_qty }}" name="measurement_qty[]"></td>
                       <td><span>{{ strtoupper($data->product_code) }}</span> <input type="hidden" name="product_code[]" value="{{ $data->product_code }}"></td>
                       <td><span>{{ $data->description }}</span><input type="hidden" name="description[]" value="{{ $data->description }}"></td>
                       <td><span>{{ $data->qty }}</span> <input type="hidden" name="qty[]" value="{{ $data->qty }}"></td>
                       <td><span>{{ Helper::money_format($data->price) }}</span><input type="hidden" name="price[]" value="{{ $data->price }}"></td>
                       <td><span>{{Helper::money_format($data->total_amount) }}</span></td>
                        
                       </tr>
                     @php
                      $amount += $data->qty * $data->price;
                      @endphp
                       @empty
                       @endforelse
                      <tr>
                          <td colspan="5">TOTAL</td>
                          <td>{{ Helper::money_format($amount)  }}</td>
                        </tr>
                      <input type="hidden" name="total_due" value="{{ $amount }}">
                     </tbody>
                   </table>  
              </div>
              </div>
        
         <div class="col-lg-12 text-right">
            <div class="row">
              <div class="offset-lg-7 col-lg-5">
                  <div class="table-responsive">
      
                        {{ csrf_field() }}

                        <input type="text" hidden="" value="{{ $data->supplier_id }}" name="supplier_id">
                       <div class="form-group" hidden="">
                          <label for="">Terms</label>
                          <input type="number" name="terms" id="" value="0" class="form-control">
                          <input type="text" name="cart_id" id="" value="{{ $data->cart_id }}" class="form-control">
                        </div>
                        
                    
                        <div class="form-group" hidden="">
                          <label for="">Date of Deliver</label>
                          <input type="text" name="date" readonly="" id="" value="{{ date('Y-m-d')}}" class="form-control">
                        </div>
                       

                        <div class="form-group" >
                        </div>
                        <input type="text" name="transaction_id" hidden="" value="{{ uniqid() }}">
                        <div class="form-group">
                          <a href="#confirm" data-toggle="modal" class="btn btn-outline-primary" {{ $count->tatus == 1 ? "hidden": "" }}>Recieved Deliver</a>
                        </div>
                   
                  </div>
                </div>
              </div>
              </div>

              {{-- LIST DETAILS --}}

             
            </div>
           {{-- </form> --}}
          </div>


        </div>
      </div>

    </div>



    <!-- The Modal -->
    <div class="modal fade" id="confirm">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Confirm</h4>
            {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
          </div>
           <form action="" method="POST">
          <div class="modal-body">
           
            <span>Are You Sure Want to Proceed ?</span>

         <!-- Modal footer -->
        </div>
         <div class="modal-footer" style="background:#ddd">
         {{--  <form action="" method="post">
            {{ csrf_field() }} --}}
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-primary" href="#" data-id="" id="removes" >Proceed</button>
        </form>
        </div>
   
   
      </form>
    </div>
  </div>

  {{-- end modal --}}
  @stop

  @section('page-scripts')
  <script>
    $(document).ready(function(){

      // $('#terms').on('keyup',function(){
      //   var test = $(this).val()
      //   alert(test)
      //   $('#due_date').val('{{ Carbon::now()->addDays(10) }}'
      // })
     
      
     $(document).on('click','a[data-role=delete]',function(){
      var btn = $(this);
      $("#removes").attr({"href" : btn.data('url')});
    })
   })
  </script>
  @stop
