@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">


  <h1>Add Stock</h1>
  <ul>
      <li>Add Stock</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Stock</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="product_supplier"><b>Supplier</b></label>
              {!!Form::select('product_supplier',$supplier,old('type'),['class' => "form-control",'id' => "input_type"])!!}
                @if($errors->first('supplier'))
                <p class="form-text text-danger">{{$errors->first('product_supplier')}}</p>
                @endif

            </div>
            <div class="form-group">
                <label for="product_name"><b>Product Name</b></label>
                <input type="text" class="form-control" id="product_name" placeholder="" value="{{old('name')}}" name="product_name">
                @if($errors->first('product_name'))
                <p class="form-text text-danger">{{$errors->first('product_name')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="product_name"><b>Product Code</b></label>
                <input type="text" class="form-control" id="product_code" placeholder="" value="{{old('product_code')}}" name="product_code">
                @if($errors->first('product_code'))
                <p class="form-text text-danger">{{$errors->first('product_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="qty"><b>Quantity</b></label>
                <input type="number" min="0" value="0" step="any" class="form-control" id="qty" placeholder="" value="{{old('product_qty')}}" name="product_qty">
                @if($errors->first('product_qty'))
                <p class="form-text text-danger">{{$errors->first('product_qty')}}</p>
                @endif


            </div>



            <div class="form-group">
                <label for="input_type"><b>Measurement/Unit</b></label>
                <input type="text" class="form-control" id="product_unit" placeholder="" value="{{old('product_unit')}}" name="product_unit">
                @if($errors->first('product_unit'))
                <p class="form-text text-danger">{{$errors->first('product_unit')}}</p>
                @endif
            </div>



            <div class="form-group">
              <a href="{{ route('portal.stock.index') }}" class="btn btn-danger mr-2">Back</a>
               <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop
