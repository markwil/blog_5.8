@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product(s)</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-12">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data </h3>
          <span class="float-right">
            <a href="{{route('portal.product.index')}}" title="back" class="btn btn-outline-primary">Back</a>
          </span>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">



            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Material Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Unit Cost</th>
                    <th scope="col">Measurement Unit</th>
                    <th scope="col"></th>

                  </tr>
                </thead>
                <tbody>
                  @forelse($ingredients as $data)
                    <td ><span>{{ Str::upper($data->description) }}</span></td>
                    <td ><span>{{ Helper::qty_format($data->qty)}}</span></td>
                    <td ><span>{{ $data->cost_unit  }}</span></td>
                    <td ><span>{{ Helper::measurement($data->mou)  }}</span></td>
                    <td>
                     
                      <a href="#edit" data-toggle="modal" class="btn btn-outline-primary" data-role="edit" data-id="{{ $data->id }}" data-qty="{{ $data->qty }}" data-url="{{ route('portal.product.edit-qty',[$data->id]) }}">Edit</a> 
                       <a href="#remove" class="btn btn-outline-danger" title="Remove Ingrendient" data-toggle="modal" data-role="remove" data-url="{{ route('portal.product.remove-ingredient',[$data->id]) }}" data-id="{{ $data->id }}" class="text text-danger">Remove</a>
                    </td>
                  </tr>
                  @empty
                  @endforelse
                </tbody>
              </table>

            </div>

          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-outline-primary" href="#" data-id="" id="removes">Yes</a>
      </div>

    </div>
  </div>
</div>

{{-- edit modal --}}
 <div class="modal fade" id="edit">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Ingredient</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <form action="{{ route('portal.product-ingredient.update') }}" method="POST">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
            <label for="">Quantity</label>
            <input type="number" class="form-control" class="form-control" id="qty" name="qty">
            <input type="hidden" class="form-control" class="form-control" id="id" name="id">
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-outline-success" id="updates">Update</button>
       {{--  <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Update</a> --}}
      </div>
      </form>
    </div>
  </div>
</div>

@stop
@section('page-styles')
<style type="text/css">
  .table tbody tr td{  padding: 2px;vertical-align: middle; }
</style>
@stop
@section('page-scripts')
<script>
  $(document).ready(function(){

    $(document).on('click','a[data-role=generate]',function(){
      var name = $(this).data('name')
      var price = $(this).data('price')
    
      $('#name').val(name);
      $('#price').val(price);

      $('#name').val(name).css('display','none');
      $('#price').val(price).css('display','none');
      // $('#category').val(category).css('display','none');

    })

    $(document).on('click','a[data-role=remove]',function(){
      var btn = $(this);
        
      $("#removes").attr({"href" : btn.data('url')});
    })

    $(document).on('click','a[data-role=edit]',function(){
      $('#qty').val($(this).data('qty'))
      $('#id').val($(this).data('id'))
      var btn = $(this);
      $('#updates').attr({"href" : btn.data('url')});
    })


  })
</script>
@stop
