@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product</h1>
  <ul>
      <li>Add New Product</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-7">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Product</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="category">Product Category</label>
                <select name="category" id="category" class="form-control" >
                    <option value="{{ $data->category }}">{{ $data->category }}</option>
                    @foreach ($category as $data)
                    <option value="{{ $data->name }}">{{ $data->name }}</option>
                     @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="input_name"><b>Product Name</b></label>
                <input type="text" class="form-control" id="name" placeholder="" value="{{old('name',$data->name)}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="price"><b>Price</b></label>
                <input type="text" class="form-control" id="price" placeholder="" value="{{old('price',$data->price)}}" name="price">
                @if($errors->first('price'))
                <p class="form-text text-danger">{{$errors->first('price')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_description"><b>Description</b></label>
                <input type="text" class="form-control" id="description" placeholder="" value="{{old('description')}}" name="description">
                @if($errors->first('description'))
                <p class="form-text text-danger">{{$errors->first('description')}}</p>
                @endif
            </div>

            <div id="ingredients_container">
                <div class="row item-ingredients">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ingredients">Raw Materials</label>
                            <select name="ingredients[]"  class="form-control">
                                <option value="">--Choose Ingredients--</option>
                                @foreach ($ingredients as $data)
                                <option value="{{ $data->name }}">{{ $data->name }}</option>
                                 @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="qty"><b>Qty</b></label>
                            <input type="text" class="form-control" id="qty" placeholder="" value="{{old('qty')}}" name="quantity[]">
                            @if($errors->first('qty'))
                            <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                            @endif
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="" style="color: #fff">&nbsp;-</label>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btn-add">Add New</button>
                                <button type="button" class="btn btn-danger btn-remove">Remove</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            <div class="form-group">
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
        
        </div>
    </div>
  </div>


  {{-- <div class="col-md-8">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Ingredients</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications') --}}
        
        
            {{-- <div id="ingredients_container">
                <div class="row item-ingredients">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ingredients">Raw Materials</label>
                            <select name="ingredients[]"  class="form-control">
                                <option value="">--Choose Ingredients--</option>
                                @foreach ($ingredients as $data)
                                <option value="{{ $data->name }}">{{ $data->name }}</option>
                                 @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="qty"><b>Qty</b></label>
                            <input type="text" class="form-control" id="qty" placeholder="" value="{{old('qty')}}" name="qty">
                            @if($errors->first('qty'))
                            <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                            @endif
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-add">Add New</button>
                                <button type="button" class="btn btn-danger btn-remove">Remove</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            
            <div class="form-group">
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div> --}}
           
          </form>
      {{--   </div>
    </div>
  </div>


</div> --}}
@stop

@section('page-scripts')
<script type="text/javascript">
    $(function(){

        $("#ingredients_container")
            .delegate(".btn-add",'click',function(){
                var item = $(this).parents('.item-ingredients').html();

                var to_append = '<div class="row item-ingredients">'+item+'</div>';
                
                $("#ingredients_container").append(to_append)
                
            })
            .delegate(".btn-remove","click",function(){
                var item = $(this).parents('.item-ingredients');

                item.fadeOut(500,function(){
                    $(this).remove();
                })
            });


        
    })
</script>
@stop