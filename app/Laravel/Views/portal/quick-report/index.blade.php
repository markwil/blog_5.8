@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Stock Inventory Quick Report</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      @include('portal.components.notifications')
        {{-- <div class="table-responsive"> --}}
         <div class="col-md-12 text-right">
          <form method="post">
            {{ csrf_field() }}
            <a href="{{ route('portal.quick_report.export_pdf_in') }}"  class="btn btn-outline-primary btn-remove">Export to Pdf</a>
          </form>
        </div>
        <br>
          <table class="table table-bordered">
              <thead>
                <tr>
                <th colspan="7" ><center>Transaction IN</center></th>
            
              </tr>
               </thead>

            <thead class="thead-dark">
              <th>Date</th>
              <th>Particulars</th>
              <th>Transaction ID</th>
              <th>Item Code</th>
              <th>Quantity</th>
              <th>Unit Price</th>
              <th>Total Amount</th>
            </thead>
            <tbody>
              @forelse($stocks as $data)
              <tr>
                {{-- TRANSACTION IN --}}
                <td><span>{{ Helper::date_format($data->created_at) }}</span></td>
                <td><span>{{ $data->description}}</span></td>
                <td>{{ $data->transaction_id }}</td>
                <td><span>{{ $data->product_code}}</span></td>
                
                <td><span>{{ $data->qty}}</span></td>
                <td><span>{{ $data->price}}</span></td>
                <td><span>{{ number_format($data->qty * $data->price,2)}}</span></td>
               
                </tr>
              @empty
              <tr>
                <td colspan="5">NO DATA FOUND</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        {{-- </div> --}}
        
        <div class="pagination">
        {{ $stocks->links() }}
        </div>

      </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="marked_up">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             <input type="number" name="marked_up" step="any"  id="marked" class="form-control" >
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" > Update </button>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){
      $(document).on('click','a[data-role=edit]',function(){
        var marked_up = $(this).data('mark');
        $('#marked').val(marked_up)
      })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
