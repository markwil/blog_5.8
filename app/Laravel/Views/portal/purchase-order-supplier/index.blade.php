@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>ORDER</h1>
  <ul>
      <li>Purchse Order</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">

            <h3 class="w-50 float-left card-title m-0">Supplier List</h3>
            {{-- <a href="{{ route('portal.stock.index') }}" class="w-50 d-flex justify-content-end card-title m-0">Back</a> --}}
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
          <div class="row">
          <table class="table table-bordered text-center">
            <tr class="text-center">
              <th>Choose Supplier</th>
            </tr>
            @forelse($suppliers as $data)
            <tr>
              <td><a href="{{ route('portal.purchase-order-supplier.create',[$data->id])}}">{{  $data->supplier }}</a></td>
            </tr>
            @empty
            @endforelse
            
          </table>
            </div>

   
          </form>
        </div>
    </div>
  </div>
</div>
@stop

