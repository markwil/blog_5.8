@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>ORDER</h1>
  <ul>
      <li>Purchse Order</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-12">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Purchase Order  - Record Data</h3>

            
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
    
          {{-- /////////////////////////////////////////// --}}
          {{-- <input type="text" placeholder="Seach Product Here..." class="form-control"> --}}
          <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
            <thead class="thead-dark">
              <th>Product Code</th>
              <th>Descripition</th>
              <th>Price</th>
              <th style="width: 20px">Action</th>
            </thead>
            <tbody>
              @forelse($products as $data)
              <tr id="{{ $data->id }}">
                <td data-target="product_code">{{ $data->product_code }}</td>
                <td data-target="description">{{ $data->description}} {{ $data->qty - floor($data->qty) == 0 ? floor($data->qty) : $data->qty }} {{ $data->measurment }}</td>
                <td data-target="price">{{ $data->price}}</td>
                <td><a style="padding: 3px 7px 3px 7px!important;" href="#myModal" data-toggle="modal" data-role="add" data-id="{{ $data->id }}" data-measure="{{ $data->measurment }}" data-qty="{{ $data->qty }}" data-image="{{$data->directory}}/{{$data->filename}}" class="btn btn-outline-primary">Add to cart</a></td>
              </tr>
              @empty
              @endforelse
            </tbody>
          </table>
        {{--   <div class="form-group">
            {{ $products->links() }}
          </div> --}}
        </div>
    </div>
  </div>
</div>



<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Product Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
       <form action="{{ route('portal.purchase-order-supplier.add-to-cart',[Request::segment(3)]) }}" method="POST">
        {{ csrf_field() }}
      <div class="modal-body">
        <input type="hidden"  name="branch_id" value="{{ auth()->user()->branch_id }}">
        <input type="hidden"  name="measurement" value="" id="measurement">
        <input type="hidden" value="{{ Request::segment(3) }}" name="supplier_id">
        <input type="hidden" value="{{ Request::segment(3)  }}" id="cart_id" name="cart_id">
        <input type="hidden" value="" id="product_id" name="product_id">
        <input type="hidden" value="" id="measurement_qty" name="measurement_qty">
        {{-- <input type="text" value="" id="total_amount" name="total_amount"> --}}
        <table class="table table-bordered">
          <tr><td>Product Code:</td><td><span id="p_name"></span></td><input type="hidden" name="product_code" id="product_code"></tr>
          <tr><td>Product Description:</td><td><span id="descriptions"></span></td><input type="hidden" name="description" id="description"></tr>
          <tr><td>Product Price:</td><td><span id="prc"></span></td><input type="hidden" name="price" id="price"></tr>
          <tr><td>Qty:</td><td><input type="number" value="1" step="any"  min="1"  name="qty"></td></tr>
        </table>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-outline-primary">Add To Order List</button>
        {{-- <a href="{{ route('portal.purchase-order-supplier.add-to-cart') }}" class="btn btn-primary">Add To Order List</a> --}}
      </div>
</form>
    </div>
  </div>
</div>
@stop

@section('page-scripts')

<script src="{{ asset('assets/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/datatables/datatables.script.min.js') }}"></script>
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=add]',function(){
    var id = $(this).data('id');

    var p_name = $('#'+id).children('td[data-target=product_code]').text();
    var description = $('#'+id).children('td[data-target=description]').text();
    var price = $('#'+id).children('td[data-target=price]').text();
    // var measure = $('#'+id).children('td[data-target=measure]').text();
    
 
   
    $('#p_name').text(p_name)
    $('#product_id').val(id)
    $('#descriptions').text(description)
    $('#prc').text(price)
    $('#branch_id').val($(this).data('branch'))
    $('#product_code').val(p_name);
    $('#description').val(description);
    $('#price').val(price);
    $('#measurement').val($(this).data('measure'));
    $('#measurement_qty').val($(this).data('qty'));
    
    // $('#image').val($(this).data('image'));
   })
 })
</script>
@stop