@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Delivery Form</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data </h3>
          <span class="float-right">
            <a href="{{route('portal.stock.index')}}" class="btn btn-primary">Back</a>
          </span>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">


    <div class="col-md-7 mt-5 mb-5">
      <form action="" method="POST">
                <div class="form-group">

                  <label for="branch">Store Branch</label>
                  <select class="form-control" name="branch">
                    <option value="">--select branch--</option>
                    <option value="branch 1">branch 1</option>
                    <option value="branch 1">branch 2</option>
                    <option value="branch 1">branch 3</option>
                    <option value="branch 1">branch 4</option>
                  </select>
                </div>


                <div id="ingredients_container">
                    <div class="row item-ingredients">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ingredients">Raw Materials</label>
                                <select name="product[]"  class="form-control">
                                    <option value="">--Choose Ingredients--</option>
                                    @foreach ($products as $data)
                                    <option value="{{ $data->slug }}">{{ Str::upper($data->name) }}</option>
                                     @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="qty"><b>Qty</b></label>
                                <input type="text" class="form-control" id="qty" placeholder="" value="{{old('qty')}}" name="quantity[]">
                                @if($errors->first('qty'))
                                <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="" style="color: #fff">&nbsp;-</label>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success btn-add">Add New</button>
                                    <button type="button" class="btn btn-danger btn-remove">Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary">Proceed</button>
                </div>
              </div>
            </form>



          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Yes,Delete it!</a>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })

  $("#ingredients_container")
      .delegate(".btn-add",'click',function(){
          var item = $(this).parents('.item-ingredients').html();

          var to_append = '<div class="row item-ingredients">'+item+'</div>';

          $("#ingredients_container").append(to_append)

      })
      .delegate(".btn-remove","click",function(){
          var item = $(this).parents('.item-ingredients');
          item.fadeOut(500,function(){
              $(this).remove();
          })
      });
 })
</script>
@stop
