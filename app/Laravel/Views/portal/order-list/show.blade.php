@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>SUPPLIER : {{ strtoupper($supplier_name->name) }}</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data </h3>
          <span class="float-right">
            <!-- <a href="{{route('portal.stock-out.create')}}" class="btn btn-primary">Delivery</a> -->
          </span>
          <span class="float-right mr-2">
            @if($supplier > 0)
             {{-- <a href="{{route('portal.purchase-order-supplier.index')}}"  class="btn btn-sm btn-primary">Purchase Order</a> --}}
             {{-- <a href="#confirm"  data-toggle="modal" class="btn btn-sm btn-primary">Confirm Order</a> --}}
             <a href="{{ route('portal.order_list.index') }}"  class="btn btn-sm btn-primary">Back To Order List</a>
            @endif
           
          </span>

        </div>
        <div class="card-body">
          @include('portal.components.notifications')

          <div class="row justify-content-center">
       
            <div class="table-responsive">
                   <form action="{{ route('portal.order-list.stock-in') }}" method="POST">
                    {{ csrf_field() }}
                <div class="d-flex flex-row">
                    <div class="form-group col-md-3">
                      <label for="terms">Terms</label>
                      <input type="text" value="" name="terms" id="terms" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                       <label for="reference_number">Reference Number</label>
                      <input type="text" value="" name="reference_number" id="reference_number" class="form-control">
                    </div>
                    <div class="form-group col-md-3">
                       <label for="due_date">Date</label>
                      <input type="text" value="{{ Carbon::now()->addDays(10)->format('y-m-d') }}" name="date" id="date" class="form-control">
                    </div>
                     <div class="form-group col-md-3">
                       <label for="terms" style="color: #fff">date</label><br>
                      <button type="submit" class="btn btn-success" style="width: 250px">Save</button>
                    </div>
                  </div>

                     
            
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Item Code</th>
                    <th scope="col">Item</th>
                    <th scope="col">Description</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Subtotal</th>
                              </tr>
                </thead>
                <tbody>
                      @php 
                    $total = 0;
                    $x = 0;
                  @endphp
                  @forelse($suppliers as $data)
                  <tr>
        <td>{{  str_pad($data->id, 4, "0", STR_PAD_LEFT)}} <input type="hidden" value="{{ str_pad($data->id, 4, "0", STR_PAD_LEFT)}}" name="item_code[]"></td>
                    <td>{{ strtoupper($data->product_name) }} <input type="hidden" value="{{ $data->product_name }}" name="product_name[]"></td>

                    <td>{{ $data->description }}  <input type="hidden" value="{{ $data->description }}" name="description[]"></td>
                    <td>{{ $data->qty }} <input type="hidden" value="{{ $data->qty }}" name="qty[]"></td>
                    <td>{{ $data->price }} <input type="hidden" value="{{ $data->price }}" name="price[]"></td>
                    <td>{{ Helper::money_format($data->total_price)}}</td>
                  </tr>
                   
                  @php
                  $total += $data->total_price;
                  @endphp
                  @empty
                  
                  <tr colspan="5" class="text-center">No Data Found</tr>
                  @endforelse
                   <tr>
                  <td>VAT</td>
                  <td>VAT</td>
                  <td>VAT</td>
                  <td>1</td>
              <td>{{ $count > 0 ? Helper::money_format($data->vat_price) : ''  }} <input type="hidden" value="{{ $count > 0 ? $data->vat_price : '' }}" name="total_price" ></td>
                  <td>{{ $count > 0 ? Helper::money_format($data->vat_price) :''  }}</td>
                  </tr>
                  <tr>
                    <td></td>
                      <td colspan="4" class="text-right font-weight-bold">TOTAL ( VAT EXCLUDED)</td>
                      <td class="font-weight-bold">{{ $count > 0 ?  Helper::money_format($total) : '' }}</td>  
                  </tr>
                  <tr>
                    <td></td>
                      <td colspan="4" class="text-right font-weight-bold">GRAND TOTAL (VAT INCLUDED)</td>
                      <td class="font-weight-bold">{{ $count > 0 ? Helper::money_format($data->vat_price + $total):'' }} <input type="hidden" name="grand_total" value="{{ $count > 0 ? $data->vat_price + $total :'' }}"></td>  
                  </tr>
                  {{-- <tr>
                    <td colspan="5"></td>
                    <td><button type="submit" class="btn btn-primary form-control">Save</button></td>  
                  </tr> --}}
                </tbody>
              </table>
 </form>
            </div>
           
          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="confirm">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        </div>
         <form action="" method="POST">
        <div class="modal-body">
         
          <div class="col-md-12">
           <div class="form-group">
             <label for="terms">Terms</label>
             <input type="number" value="0" min="1" class="form-control" name="terms" id="terms" >
           </div>
           
           <div class="form-group">
             <label for="reference_number">Reference Number</label>
             <input type="text" value="" class="form-control" name="reference_number"   >
           </div>
           <div class="form-group">
             <label for="reference_number">Due Date</label>
             <input type="text" value="" class="form-control" name="due_date" id="due_date"   >
           </div>
           <div class="form-group">
             <label for="reference_number">Amount Due</label>
             <input type="number" disabled="" value="{{ $count > 0 ?  $data->vat_price + $total :''  }}" class="form-control" name="amount_due" >
           </div>
         </div>
        
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" href="#" data-id="" id="removes" style="color:#fff" >Proceed</button>
      </div>
 
    </div>
    </form>
  </div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

    // $('#terms').on('keyup',function(){
    //   var test = $(this).val()
    //   alert(test)
    //   $('#due_date').val('{{ Carbon::now()->addDays(10) }}'
    // })
   
    
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
