@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Material</h1>
  <ul>
      <li>Add New Raw material</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Material</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_name"><b>Material Name</b></label>
                <input type="text" class="form-control" id="name" placeholder="" value="{{old('name',$data->name)}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

      

            <div class="form-group">
                <label for="input_value"><b>Value</b></label>
                <input type="text" class="form-control" id="value" placeholder="" value="{{old('value',$data->value)}}" name="value">
                @if($errors->first('value'))
                <p class="form-text text-danger">{{$errors->first('value')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_type"><b>Type</b></label>
                <input type="text" class="form-control" id="description" placeholder="" value="{{old('type',$data->type)}}" name="type">
                @if($errors->first('type'))
                <p class="form-text text-danger">{{$errors->first('type')}}</p>
                @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn  btn-primary">Update</button>
            </div>

           
          </form>
        </div>
    </div>
  </div>
</div>

@stop
