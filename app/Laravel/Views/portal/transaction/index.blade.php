@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Transaction</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="form-group">
      <label for="" style="color: #fff">&nbsp;-</label>
      <div class="row">
      <div class="col-md-6 text-left">
        <div class="btn-group">
          <a href="{{ route('portal.transactions.today') }}" class="btn btn-outline-secondary">Today</a>
          <a href="{{ route('portal.transactions.week') }}" class="btn btn-outline-secondary btn-remove">This Week</a>
          <a href="{{ route('portal.transactions.month') }}" class="btn btn-outline-secondary btn-remove">This Month</a>

        </div>
      </div>
      <div class="col-md-6 text-right">
        <form method="post">
          {{ csrf_field() }}
        <button type="submit" class="btn btn-outline-primary btn-remove">Export to Pdf</button>
        </form>
      </div>
    </div>
      

    </div>
  </div>


  <div class="col-md-12">
        {{-- <div class="table-responsive"> --}}
        <div class="table table-responsive">
          <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
              {{-- <thead>
                <tr>
                <th colspan="9" ><center>Sales Journal</center></th>
              </tr>
               </thead> --}}

            <thead class="thead-dark">
              <th>Date</th>
              <th>Client/Buyer</th>
              <th>Particulars</th>
              <th>Item Code</th>
              <th>Month</th>
              <th>Trans#</th>
              <th>Quantity</th>
              <th>Unit Price</th>
              <th>Amount</th>
              <th>Revenue</th>
              <th>Cash</th>
              <th>Account Recivable</th>
              <th>Representation</th>
              <th>Sales Discount</th>
              <th>Category</th>
            
            
            </thead>
             @php
                $total = 0;
               @endphp
                @foreach($transactions as $transaction)
                <tr>
                  <td> <span>{{ date("Y M d",strtotime($transaction->created_at))  }}</span></td>
                  <td> <span>{{$transaction->buyer_type}}</span></td>
                  <td> <span>{{ $transaction->product_name }}</span></td>
                  <td> <span>{{ $transaction->product_code }}</span></td>
                  <td><span>{{ date('M',strtotime($transaction->created_at)) }}</span></td>
                  <td> <span> 00000{{ $transaction->id }}</span></td>
                  <td> <span>{{ $transaction->qty }}</span></td>
                  <td> <span>{{ $transaction->price }}</span></td>
                  <td> <span>{{  $transaction->amount}}</span></td>
                  <td> <span>{{ $transaction->amount }}</span></td>
                  <td> <span>{{ $transaction->qty }}</span></td>
                  <td> <span>{{ $transaction->qty }}</span></td>
                  <td> <span>{{ $transaction->qty }}</span></td>
                {{--   <td> <span>{{ $transaction->produt_name = "Senior's Citizen/PWD (20%)" ?   $transaction->price +  $transaction->amount :  $transaction->price }}</span></td>
                  <td> <span>{{ $transaction->produt_name = "Senior's Citizen/PWD (20%)" ?   $transaction->price +  $transaction->amount :  $transaction->amount }}</span></td> --}}
                   <td> <span>{{  $transaction->price  }}</span></td>
                  <td> <span>{{  $transaction->amount  }}</span></td>
                </tr>

                @php 
                $total += $transaction->amount ;
                @endphp
                @endforeach
                {{-- <tr>
                  <td colspan="8"> <span >Total</span></td>
                  <td><span btn-success">{{ number_format($total,2) }}</span></td>
                </tr> --}}
          </table>
</div>
          <div class="paginate">
            {{ $transactions->links() }}
          </div>
        {{-- </div> --}}
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="marked_up">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
          <button type="button" class="close" data
          -dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             <input type="number" name="marked_up" step="any"  id="marked" class="form-control" >
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" href="#" data-id="" id="removes" > Update </button>
      </div>

    </div>
  </div>
</div>

{{-- end modal --}}
@stop
@section('page-styles')
<style type="text/css">
  .table tbody tr td {padding: 2px;vertical-align: middle;text-align: center;}
</style>
@stop

@section('page-scripts')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('assets/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/datatables/datatables.script.min.js') }}"></script> --}}
<script>
  $(document).ready(function(){
      $(document).on('click','a[data-role=edit]',function(){
        var marked_up = $(this).data('mark');
        $('#marked').val(marked_up)
      })
   $(document).on('click','a[data-role=delete]',function(){
    var btn = $(this);
    $("#removes").attr({"href" : btn.data('url')});
  })
 })
</script>
@stop
