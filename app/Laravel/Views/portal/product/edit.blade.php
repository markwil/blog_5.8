@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product</h1>
  <ul>
      <li>View Ingredendiet(S)</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-7">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Product</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

         {{--    <div class="form-group">
                <label for="category">Product Code</label>
                <select name="category" id="category" class="form-control" >
                    <option value="{{ $data->category }}">{{ $data->category }}</option>
                    @foreach ($category as $data)
                    <option value="{{ $data->name }}">{{ $data->name }}</option>
                     @endforeach
                </select>
            </div>

            --}}     
              {{-- <div class="form-group">
                <label for="input_name"><b>Categroy</b></label>
                <input type="text" class="form-control" id="Product_code" placeholder="" value="{{old('product_code',$data->product_code)}}" name="product_code">
                @if($errors->first('product_code'))
                <p class="form-text text-danger">{{$errors->first('product_code')}}</p>
                @endif
            </div> --}} 
            <div class="form-group">
                <label for="input_name"><b>Product Code</b></label>
                <input type="text" class="form-control" id="Product_code" placeholder="" value="{{old('product_code',$data->product_code)}}" name="product_code">
                @if($errors->first('product_code'))
                <p class="form-text text-danger">{{$errors->first('product_code')}}</p>
                @endif
            </div>

         

            <div class="form-group">
                <label for="input_description"><b>Discription</b></label>
                <input type="text" class="form-control" id="description" placeholder="" value="{{old('description',$data->description)}}" name="description">
                @if($errors->first('description'))
                <p class="form-text text-danger">{{$errors->first('description')}}</p>
                @endif
            </div>
               <div class="form-group">
                <label for="price"><b>Price</b></label>
                <input type="text" class="form-control" id="price" placeholder="" value="{{old('price',$data->price)}}" name="price">
                @if($errors->first('price'))
                <p class="form-text text-danger">{{$errors->first('price')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="file"><b>Image</b></label>
                <input type="file" class="form-control"  id="file" placeholder="" value="" name="file">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('file')}}</p>
                @endif
            </div>           
            <div class="form-group">
              <button type="submit" class="btn  btn-primary">Submit</button>
          </div>

      </div>
  </div>
</div>


  {{-- <div class="col-md-8">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Ingredients</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications') --}}


            {{-- <div id="ingredients_container">
                <div class="row item-ingredients">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ingredients">Raw Materials</label>
                            <select name="ingredients[]"  class="form-control">
                                <option value="">--Choose Ingredients--</option>
                                @foreach ($ingredients as $data)
                                <option value="{{ $data->name }}">{{ $data->name }}</option>
                                 @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="qty"><b>Qty</b></label>
                            <input type="text" class="form-control" id="qty" placeholder="" value="{{old('qty')}}" name="qty">
                            @if($errors->first('qty'))
                            <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                            @endif
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-add">Add New</button>
                                <button type="button" class="btn btn-danger btn-remove">Remove</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            
            <div class="form-group">
              <button type="submit" class="btn  btn-primary">Submit</button>
          </div> --}}

      </form>
      {{--   </div>
    </div>
  </div>


</div> --}}
@stop

@section('page-scripts')
<script type="text/javascript">
    $(function(){

        $("#ingredients_container")
        .delegate(".btn-add",'click',function(){
            var item = $(this).parents('.item-ingredients').html();

            var to_append = '<div class="row item-ingredients">'+item+'</div>';

            $("#ingredients_container").append(to_append)

        })
        .delegate(".btn-remove","click",function(){
            var item = $(this).parents('.item-ingredients');

            item.fadeOut(500,function(){
                $(this).remove();
            })
        });


        
    })
</script>
@stop