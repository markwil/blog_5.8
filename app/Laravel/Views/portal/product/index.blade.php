@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product(s)</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
    </ul> --}}
  </div>

  <div class="separator-breadcrumb border-top"></div>
  @stop

  @section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="card o-hidden mb-4">
        <div class="card-header">
          <h3 class="w-50 float-left card-title m-0">Record Data </h3>
          <span class="float-right">
            {{-- <a href="{{route('portal.product.create')}}" class="btn btn-primary" title="Add New Product">+</a> --}}
          </span>
        </div>
        <div class="card-body">

          @include('portal.components.notifications')
       <form action="{{ route('portal.barcode.store') }}" method="POST" >
          <div class="row justify-content-end">

        {{ csrf_field() }}
            <div class="form-group">
              <button class="btn btn-outline-primary" type="submit">Generate Barcode</button>
               <a href="{{route('portal.product.create')}}" class="btn btn-outline-info" title="Add New Product">Add new Product</a>
            </div>
            <div class="table-responsive">

              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Product Code</th>
                    <th scope="col">Product Description</th>
                    {{-- <th scope="col">Ingredients</th> --}}
                    <th scope="col">Price</th>
                    <th scope="col">Unit Cost</th>
                    <th scope="col"></th>

                  </tr>
                </thead>
                <tbody>
                  @forelse($products as $data)
                  {{-- <tr id="{{ $data->id }}"> --}}
                    <input type="hidden" name="code[]" id="" value="{{ $data->product_code }}">
                    <input type="hidden" name="description[]" id="" value="{{ $data->description }}">
                    <input type="hidden" name="price[]" value="{{$data->price}}" id="">
                    <td ><span>{{ Str::upper($data->product_code) }}</span></td>
                    <td><span>{{ Str::upper($data->description) }}</span></td>
                    {{-- <td >{{ Str::upper($data->category) }}</td> --}}
                    <td><span></span>{{ number_format($data->price,2) }}</td>
                    <td><span>{{ $data->total_cost_unit }}</span></td>
                    <td>
                      <div class="dropdown show">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Action
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                          <a class="dropdown-item" href="{{ route('portal.product.edit',[$data->id]) }}" >Edit</a>
                          <a class="dropdown-item" href="{{ route('portal.product-ingredient.index',[$data->product_code]) }}">View Ingredient</a>
                          <a class="dropdown-item"class="dropdown-item" href="#remove_product" data-url="{{ route('portal.product.remove-product',[$data->id]) }}" data-toggle="modal" data-role="deleteProduct" >Remove</a>

                        </div>
                      </div>
                    </td>
                  </tr>
                  @empty
                  @endforelse
                </tbody>
              </table>
              </form>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>



  <!-- The Modal -->
  <div class="modal fade" id="remove_product">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Confirm</h4>
         {{--  <button type="button" class="close"  data
          -dismiss="modal">&times;</button> --}}
        </div>
        <div class="modal-body">
          <div class="col-md-12">
           <div class="form-group">
             {{-- <center><span>{{ Str::title('are you sure want to delete this data?') }}</span></center> --}}
             {!!Helper::confirmation_delete() !!}
           </div>
         </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        <a  class="btn btn-outline-primary" data-url="" href="#" data-id="" id="delete_product">Yes</a>
      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="barcode">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Confirm</h4>
        <button type="button" class="close" data
        -dismiss="modal">&times;</button>
      </div>
      <form action="{{ route('portal.barcode.store') }}" method="POST">

        {{ csrf_field() }}
        <div class="modal-body">


          <div class="col-md-12">
           <div class="form-group">

            <label for="input_code"><b>Barcode/SKU</b></label>
            <input type="text" class="form-control" id="input_code" placeholder="" value="{{old('code',(Str::upper(session()->get('is_admin','no') == "no" ? Auth::guard('partner')->user()->code: "")))}}" name="code">

            <p class="form-text">Qty will be automatically adjust to divisible by 3. eg. 5 -> 6 qty will be stored. <b> ~21 barcodes per page</b></p>
            @if($errors->first('code'))
            <p class="form-text text-danger">{{$errors->first('code')}}</p>
            @endif
          </div>
          <div class="form-group">
            <label for="qty">No. of Copy</label>
            <input type="number" name="qty" id="qty" class="form-control" autocomplete="off">
          </div>
        </div>

         
        <input type="text" name="price" id="price">
         <input type="text" name="name" id="name" class="form-control">
        {{-- <input type="text" name="product_category" id="category"> --}}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer" style="background:#ddd">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="submit"  class="btn btn-primary" >Yes, Generate!</button>
      </div>
    </form>
  </div>
</div>
</div>

{{-- end modal --}}
@stop

@section('page-scripts')
<script>
  $(document).ready(function(){

    $(document).on('click','a[data-role=generate]',function(){
      var name = $(this).data('name')
      var price = $(this).data('price')
    
      $('#name').val(name);
      $('#price').val(price);


      $('#name').val(name).css('display','none');
      $('#price').val(price).css('display','none');
      // $('#category').val(category).css('display','none');

    })

    $(document).on('click','a[data-role=delete]',function(){
      var btn = $(this);
      $("#removes").attr({"href" : btn.data('url')});
    })

    $(document).on('click','a[data-role=deleteProduct]',function(){
      
      var btn = $(this);
      $("#delete_product").attr({"href" : btn.data('url')});
    })


  })
</script>
@stop
