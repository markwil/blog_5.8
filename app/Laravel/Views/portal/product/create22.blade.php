@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product</h1>
  <ul>
      <li>Add New Product</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Product</h3>
        </div>
        <div class="card-body">



          @include('portal.components.notifications')
            
             <form  method="post" action="">  
            {!!csrf_field()!!}
            <div class="col-md-12">
                <div class="form-group">
                <label for="category">Product Category</label>
                <select name="category" id="category" class="form-control">
                    <option value="">--Choose Product Category</option>
                    @foreach ($category as $data)
                    <option value="{{ $data->name }}">{{ $data->name }}</option>
                     @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="input_name"><b>Product Name</b></label>
                <input type="text" class="form-control" id="name" placeholder="" value="{{old('name')}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="price"><b>Price</b></label>
                <input type="text" class="form-control" id="price" placeholder="" value="{{old('code')}}" name="price">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_margin"><b>Slug</b></label>
                <input type="text" class="form-control" id="slug" placeholder="" value="{{old('margin')}}" name="slug">
                @if($errors->first('margin'))
                <p class="form-text text-danger">{{$errors->first('margin')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_owner_name"><b>Discription</b></label>
                <input type="text" class="form-control" id="description" placeholder="" value="{{old('owner_name')}}" name="description">
                @if($errors->first('owner_name'))
                <p class="form-text text-danger">{{$errors->first('owner_name')}}</p>
                @endif
            </div>
            <div class="form-group">  
                  <div class="table-responsive">  
                     <table class="table table-bordered" id="dynamic_field">  
                        <tr>  
                           <td><input type="text" name="ingredients[]" placeholder="Enter your Name" class="form-control name_list" /></td>  
                           <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                       </tr>  
                   </table>  
                   <input type="submit"  id="submit" class="btn btn-info" value="Submit" />  
               </div>  
               </div>
            </div>
            
       </div>  
    </div> 

</div>
<div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Product</h3>
        </div>
        <div class="card-body">
            
             
            <div class="col-md-12">
                
                 <div class="form-group">  
                  {{-- <div class="table-responsive">  
                     <table class="table table-bordered" id="dynamic_field">  
                        <tr>  
                           <td><input type="text" name="ingredients[]" placeholder="Enter your Name" class="form-control name_list" /></td>  
                           <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                       </tr>  
                   </table>  
                   <input type="submit"  id="submit" class="btn btn-info" value="Submit" />  
               </div>  --}} 
               </div>
               </div>

           </form>  
       </div>  
    </div> 

</div>
</div>
</div>
</div>

@stop

@section('page-scripts')
<script type="text/javascript">
    $(function(){

      var i=1;  
      $('#add').click(function(){  
         i++;  
         $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="ingredients[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
     });  
      $(document).on('click', '.btn_remove', function(){  
         var button_id = $(this).attr("id");   
         $('#row'+button_id+'').remove();  
     });  
      

  })
</script>
@stop