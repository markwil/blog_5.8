@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('portal.product.create')}}" class="btn btn-primary">Add New Product</a>
            </span>
        </div>
        <div class="card-body">
            @include('portal.components.notifications')
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                  <thead class="thead-dark">
                      <tr>
                          <th scope="col">Product Name</th>
                          <th scope="col">Stock(s)</th>
                          
                      </tr>
                  </thead>
                  <tbody>
                    @forelse($products as $product)
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    @empty
                    @endforelse
                  </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop