@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Product</h1>
  <ul>
      <li>Add New Product</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<form action="" enctype="multipart/form-data" method="POST">
    {{ csrf_field() }}
<div class="row">
  <div class="col-md-4">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Product</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
         

        
            <div class="form-group">
                <label for="category"><b>Product Type</b></label>
                <select name="category" id="category" required="" class="form-control">
                    <option value="{{ old('category') }}">-Choose Product Type-</option>
                    <option value="juice">Juice</option>
                    <option value="tea">Milk Tea</option>
                    <option value="tea">Coffee</option>
                    {{-- <option value="food">Food</option> --}}
                </select>
                @if($errors->first('category'))
                <p class="form-text text-danger">{{$errors->first('category')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="tea-category"><b>Product Category</b></label>
                <select name="tea_category" id="tea_category" required="" class="form-control">
                    <option value="{{ old('tea_category') }}">-Choose Tea Category-</option>
                    @foreach($categories as $data)
                        <option value="{{ $data->category_name }}">{{ $data->category_name }}</option> 
                    @endforeach
                </select>
                @if($errors->first('tea_category'))
                <p class="form-text text-danger">{{$errors->first('tea_category')}}</p>
                @endif
            </div>


            <div class="form-group">
                <label for="input_name"><b>Product Name</b></label>
                <input type="text" class="form-control" required="" id="name" placeholder="" value="{{old('name')}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>
            <div class="form-group" id="tea" hidden="">
                <label for="size"><b>Size</b></label>
                    <input type="text" class="form-control" name="size" id="size" readonly="">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>
            <div class="form-group" id="tea" hidden="">
                <label for="size"><b>Measurement Unit</b></label>
                    <input type="text" class="form-control" value="{{ old('size') }}" name="measurement" id="measurement" readonly="">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>
            
          
              <div class="form-group">
                <label for="input_name"><b>Product Code</b></label>
                <input type="text" class="form-control" required="" id="product_code" placeholder="" value="{{old('product_code')}}" name="product_code">
                @if($errors->first('product_code'))
                <p class="form-text text-danger">{{$errors->first('product_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="price"><b>Price</b></label>
                <input type="text" class="form-control" required="" id="price" placeholder="" value="{{old('price')}}" name="price" step="any">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('code')}}</p>
                @endif
            </div>


            <div class="form-group">
                <label for="image"><b>image</b></label>
                <input type="file" class="form-control" required="" id="image" placeholder="" value="{{old('file')}}" name="file">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('image')}}</p>
                @endif
            </div>

          

        </div>
    </div>
  </div>
  <div class="col-md-7">
            <div id="ingredients_container">
                <div class="row item-ingredients">
                    <div class="col-md-4">
                        {{-- <div class="form-group">
                            <label for="ingredients">Raw Materials</label>
                            <select name="ingredients[]"  required="" class="form-control">
                                <option value="{{ old('ingredients[]') }}">--Choose Ingredients--</option>
                                @foreach ($raw_material as $data)
                                @php
                                 $a = explode(' ',trim($data->description));
                                 @endphp

                                <option value="{{ $data->product_code }}"> {{ $a[0]}} </option>
                                 @endforeach
                            </select>
                        </div> --}}

                         <div class="form-group">
                            <label for="ingredients">Raw Materials</label>
                            <select name="ingredients[]"  id="ingredients" required="" class="form-control">
                                <option value="{{ old('ingredients[]') }}">--Choose Ingredients--</option>
                                @foreach ($raw_material as $data)
                                @php
                                 $a = explode(' ',trim($data->description));
                                 @endphp
                                <option value="{{$data->product_code}}:{{$data->price}}:{{$data->measurement_qty}}"> {{ $a[0]}} </option>
                                 @endforeach
                            </select>
                        </div>


                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="qty"><b>Qty</b></label>
                            <input type="text" class="form-control" id="qty" value="{{ old('qty',1)}}" placeholder="" value="{{old('qty')}}" name="qty[]">
                            @if($errors->first('qty'))
                            <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                            @endif
                        </div>
                    </div>
                     <div class="col-md-2">
                        <div class="form-group">
                            <label for="mou"><b>MU</b></label>
                           <select name="mou[]" id="mou" class="form-control" required="">
                               <option value="">-Choose MU-</option>
                               <option value="g">grams</option>
                               <option value="oz">Oz</option>
                               <option value="pcs">pcs</option>
                               <option value="ml">ml</option>
                           </select>
                            @if($errors->first('mou'))
                            <p class="form-text text-danger">{{$errors->first('mou')}}</p>
                            @endif
                        </div>
                    </div>
                     <div class="col-md-2">
                    <div class="form-group">
                        <label for="unit_cost">Cost Unit</label><br>
                        <input type="text" class="form-control" readonly="" id="unit_cost" value="0">
                    </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="" style="color: #fff">&nbsp;-</label>
                            <div class="btn-group">
                                <button type="button" class="btn btn-outline-success btn-add">Add New</button>
                                <button type="button" class="btn btn-outline-danger btn-remove">Remove</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn  btn-outline-primary">Submit</button>
            </div>

        </div>
    </div>
</form>

  </div>
  </div>

@stop

@section('page-scripts')
<script type="text/javascript">
    $(function(){


        // $('#mou').on('change',function(){
        //     var price = $('#ingredients').val()
        //     var qty = $('#qty').val()
        //     var amount = price.split(":");
        //     var unit_cost = (amount[1] / amount[2]) * qty;
        //     $('#unit_cost').val(unit_cost)

            
        // })

        $('#category').on('change',function(){
          switch ($(this).val())
          {
            case "juice":
            $('#size').val(16)
            $('#measurement').val('oz')
            break;
            case "tea":
            $('#size').val(22)
            $('#measurement').val('oz')
            break;
            default:
            $('#size').val("")
            $('#measurement').val("")
          }
        })

        $("#ingredients_container")
            .delegate(".btn-add",'click',function(){
                var item = $(this).parents('.item-ingredients').html();
                var to_append = '<div class="row item-ingredients">'+item+'</div>';

                $("#ingredients_container").append(to_append)
                $("#unit_cost").attr("id",+1)
            })
            .delegate(".btn-remove","click",function(){
                var item = $(this).parents('.item-ingredients');
                item.fadeOut(500,function(){
                    $(this).remove();
                })
            });

    })
</script>
@stop
