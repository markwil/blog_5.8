<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Zii Tea</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<title>BILL</title>
	<style>
		body{
			font-family: 'Helvetica';
			font-size: 12px;
		}
	</style>
</head>
<body>
	<div><b></b></div>
	<center><div><b style="color: #dd0000;"></b>Transmital Form</div></center>
	<br>
	<table width="100%" style="border:1px solid;border-collapse: collapse;">
	<thead  style="border:1px solid;border-collapse: collapse;">
		<tr>
		<th   style="border:1px solid;border-collapse: collapse;">Description</th>
		<th  style="border:1px solid;border-collapse: collapse;">Quantity</th>
		<th  style="border:1px solid;border-collapse: collapse;">Price</th>
		<th  style="border:1px solid;border-collapse: collapse;">Subtotal</th>
		</thead>
		</tr>
		</thead>
		@php 
		$total = 0;
		@endphp
		@foreach($items as $data)
		<tbody>
		<tr>
			<td  style="border:1px solid;border-collapse: collapse;"> {{  $data->description }} </td> 
			<td  style="border:1px solid;border-collapse: collapse;"> {{  $data->qty }} </td> 
			<td  style="border:1px solid;border-collapse: collapse;"> {{  $data->price }} </td> 
			<td  style="border:1px solid;border-collapse: collapse;"> {{  number_format($data->total_amount,2) }} </td> 
		</tr>	
		@php
		$total += $data->price * $data->qty;
		@endphp
		@endforeach
		<tr>
			<td></td>
			<td></td>
			<td>Total Amount</td>
			<td>{{ Helper::money_format($total) }}</td>
		</tr>

			</tbody>
	</table>

	

</body>
</html>