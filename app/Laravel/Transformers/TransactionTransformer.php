<?php 

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Laravel\Models\User;
use App\Laravel\Models\TransactionDetail as Detail;
use App\Laravel\Models\EorBundle as Bundle;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class TransactionTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'detail'
    ];


	public function transform(User $header) {

	    return [
	     	'id' => $header->id ?: 0,
	     	'dsfasfsaf'=>  $header->username,
	     	'password'=>  $header->password,

	     	'date_created' => [
				'date_db' => $header->date_db($header->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $header->month_year($header->created_at),
				'time_passed' => $header->time_passed($header->created_at),
				'timestamp' => $header->created_at
			]
	     ];
	}

	public function includeDetail(Header $header){
		return $this->collection($header->detail ?: new Detail, new TransactionDetailTransformer);
	}
}