<?php 

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\Product;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class ProductTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];

	public function transform(Product $product) {
	    return [
	     	'id' => $product->id ?: 0,
	     	'code' => $product->product_code,
	     	'description' => $product->description,
	     	'price' => $product->price,
	     	'display_price' => Helper::money_format($product->price),
	     	'date_created' => [
				'date_db' => $product->date_db($product->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $product->month_year($product->created_at),
				'time_passed' => $product->time_passed($product->created_at),
				'timestamp' => $product->created_at
			],
			'image' => [
				'path' => $product->directory ?: '',
	 			'filename' => $product->filename ?: '',
	 			'path' => $product->path ?: '',
	 			'directory' => $product->directory ?: '',
	 			'full_path' => "{$product->directory}/resized/{$product->filename}",
	 			'thumb_path' => "{$product->directory}/thumbnails/{$product->filename}",
			]
	     ];
	}
}