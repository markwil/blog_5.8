<?php 

namespace App\Laravel\Transformers;

use Input;
use JWTAuth, Carbon, Helper;

use App\Laravel\Models\User;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

use Str;

class UserTransformer extends TransformerAbstract{

	protected $user,$auth;

	protected $availableIncludes = [
    ];

    public function __construct() {
    	$this->auth = Auth::user();
    }

	public function transform(User $user) {
		$today = Carbon::now()->format("Y-m-d");
		
	    return [
	     	'id' => $user->id ?:0,
	     	'name' => $user->name,
	     	'username' => $user->username,
	     	'type' => $user->type,
			'email' => $user->email,
			'member_since' => [
				'date_db' => $user->date_db($user->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $user->month_year($user->created_at),
				'time_passed' => $user->time_passed($user->created_at),
				'timestamp' => $user->created_at
			]
	    ];
	}
}


