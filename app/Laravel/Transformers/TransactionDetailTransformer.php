<?php 

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Laravel\Models\TransactionHeader as Header;
use App\Laravel\Models\TransactionDetail as Detail;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class TransactionDetailTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];


	public function transform(Detail $detail) {
	    return [
	     	'id' => $detail->id ?: 0,
	     	'transaction_id' => $detail->transaction_id,
	     	'cashier_user_id' => $detail->cashier_user_id ?: '',
	     	'branch_id' => $detail->branch_id ?: '',
	     	'product_id' => $detail->product_id ?: '',
	     	'product_code' => $detail->product_code ?: '',
	     	'product_name' => $detail->product_name ?: '',
	     	'price' => $detail->price ?: '',
	     	'qty' => $detail->qty ?: '',
	     	'amount' => $detail->amount ?: '',
	     	'type' => $detail->type ?: '',
	     	'date_created' => [
				'date_db' => $detail->date_db($detail->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $detail->month_year($detail->created_at),
				'time_passed' => $detail->time_passed($detail->created_at),
				'timestamp' => $detail->created_at
			],
	     ];
	}

}