<?php

namespace App\Laravel\Middleware\Portal;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check() && Auth::user()->type != "accounting" && Auth::user()->type != "super_user" && Auth::user()->type != "it" )
        {
            
             return redirect()->back();
        }
        return $next($request);
    }
}
