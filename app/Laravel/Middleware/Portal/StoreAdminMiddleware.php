<?php

namespace App\Laravel\Middleware\Portal;

use Closure;
use Illuminate\Support\Facades\Auth;

class StoreAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check() && Auth::user()->type != "store_admin" && Auth::user()->type != "accounting" )
        {
            session()->flash('notification-status',"warning");
            session()->flash('notification-msg',"You don't have permision to view the page");
             return redirect()->back();
        }
        return $next($request);
    }
}
