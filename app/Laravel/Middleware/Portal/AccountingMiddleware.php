<?php

namespace App\Laravel\Middleware\Portal;

use Closure;
use Illuminate\Support\Facades\Auth;

class AccountingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check() && Auth::user()->type != "accounting" )
        {
            
           return redirect()->back();
        }
        return $next($request);
    }
}
