<?php

namespace App\Laravel\Middleware\Api;

use Closure, Helper;

use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\Product;

class ExistRecord
{

    protected $format;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $record
     * @return mixed
     */
    public function handle($request, Closure $next, $record)
    {
        $this->format = $request->format;
        $response = array();

        switch (strtolower($record)) {

            case 'transaction':
                if(! $this->_exist_transaction($request)) {
                    $response = [
                        'msg' => "Transaction not found",
                        'status' => FALSE,
                        'status_code' => "TRANSACTION_NOT_FOUND",
                        'hint' => "Make sure the 'transaction_id' from your request parameter exists and valid."
                    ];
                }
            break;

            case 'pending_transaction':
                if(! $this->_exist_pending_transaction($request)) {
                    $response = [
                        'msg' => "Transaction is already completed.",
                        'status' => FALSE,
                        'status_code' => "COMPLETED_TRANSACTION",
                        'hint' => "Transaction modification is only allowed to transactions with pending status."
                    ];
                }
            break;

            case 'product':
                if(! $this->_exist_product($request)) {
                    $response = [
                        'msg' => "Product not found",
                        'status' => FALSE,
                        'status_code' => "PRODUCT_NOT_FOUND",
                        'hint' => "Make sure the 'product_id' from your request parameter exists and valid."
                    ];
                }
            break;
        }

        if(empty($response)) {
            return $next($request);
        }

        switch ($this->format) {
            case 'json':
                return response()->json($response, 404);
            break;
            case 'xml':
                return response()->xml($response, 404);
            break;
        }
    }

    private function _exist_transaction($request) {
        $transaction = TransactionHeader::find($request->get('transaction_id'));
        
        if($transaction) {
            $request->merge(['transaction_data' => $transaction]);
            return TRUE;
        }

        return FALSE;
    }

    private function _exist_pending_transaction($request) {
        $transaction = TransactionHeader::find($request->get('transaction_id'));

        return in_array($transaction->status, ['pending']) ? TRUE : FALSE;
    }

    private function _exist_product($request) {
        $product = Product::where('product_code', $request->get('product_code'))->first();
        
        if($product) {
            $request->merge(['product_data' => $product]);
            return TRUE;
        }

        return FALSE;
    }
}