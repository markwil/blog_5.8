<?php

namespace App\Laravel\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;
use OwenIt\Auditing\Contracts\Auditable;
use Yadahan\AuthenticationLog\AuthenticationLogable;
// use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject,Auditable
{
    use Notifiable,SoftDeletes, DateFormatterTrait,AuthenticationLogable;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $auditInclude = [
        'username',
        'email',
        'branch_id',
        'branch_location',
        'pincode',
      
    ];
protected $auditExclude = [
        'remember_token',
    ];

    protected $auditEvents = [
        'deleted',
        'restored',
        'created',
    ];
    public function getJWTIdentifier(){
        return $this->getKey();
    }
    public function getJWTCustomClaims(){
        return [];
    }

    
}
