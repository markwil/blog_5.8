<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->datetime('start_shift')->nullable();
            $table->datetime('end_shift')->nullable();
            $table->decimal('cash_on_float', 25, 2)->nullable();
            $table->decimal('cash_to_deposit', 25, 2)->nullable();
            $table->integer('one_thousand')->nullable();
            $table->integer('five_hundred')->nullable();
            $table->integer('two_hundred')->nullable();
            $table->integer('one_hundred')->nullable();
            $table->integer('fifty_peso')->nullable();
            $table->integer('twenty_peso')->nullable();
            $table->integer('ten_peso')->nullable();
            $table->integer('five_peso')->nullable();
            $table->integer('one_peso')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance');
    }
}
