<?php

use App\Laravel\Models\User;
use Illuminate\Database\Seeder;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrNew([
            'name' => "Super User", 
            'status' => 'active',
            'branch_id' => '1',
            'email' => "admin@gmail.com", 
            'username' => "master_admin",
            'type' => "super_user",
        ]);

        $user->password = bcrypt("admin");
        $user->save();
 $store = User::create([
            'name' => "Juan Dela Cruz", 
            'status' => 'active',
            'branch_id' => '2',
            'email' => "admi@gmail.com", 
            'username' => "store_admin",
            'type' => "store_admin",
        ]);

        $store->password = bcrypt("admin");
        $store->save();
    }
}
